/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation

import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import com.google.android.material.card.MaterialCardView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.model.chart.DisplaySettings
import de.namnodorel.chatstat.model.chart.bar.BarChartDisplaySettings
import de.namnodorel.chatstat.model.chart.line.LineChartDisplaySettings
import de.namnodorel.chatstat.model.chart.pie.PieChartDisplaySettings
import de.namnodorel.chatstat.model.chart.radar.RadarChartDisplaySettings
import de.namnodorel.chatstat.model.chart.scatter.ScatterChartDisplaySettings
import de.namnodorel.chatstat.model.chart.text.TextDisplaySettings
import de.namnodorel.chatstat.view.implementation.utils.resolveColor


class ChartListItem(internal val title: String, internal val description: String, internal val chartType: DisplaySettings, internal val valid: Boolean) : AbstractItem<ChartListItem.ViewHolder>() {

    override val type = 0

    override val layoutRes = R.layout.chart_list_item

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v as MaterialCardView)
    }


    class ViewHolder(val view: MaterialCardView) : FastAdapter.ViewHolder<ChartListItem>(view) {

        //AndroidAnnotations has sadly no support for using @ViewById in a ViewHolder,
        //and I am not adding Butterknife to the project for three lines of code.

        private val titleTv: TextView = view.findViewById(R.id.chart_title)

        private val descriptionTv: TextView = view.findViewById(R.id.chart_description)

        private val iconIv: AppCompatImageView = view.findViewById(R.id.chart_icon)

        override fun bindView(item: ChartListItem, payloads: MutableList<Any>) {
            titleTv.text = item.title
            descriptionTv.text = item.description


            if (!item.valid) {
                view.strokeColor = view.context.resolveColor(R.attr.colorError)

            }else if(item.isSelected){
                //Use a custom way to signal that a card is selected because for some reason
                //view.isSelected = true
                //makes the card resize in RecyclerViews. No idea why.
                view.strokeColor = view.context.resolveColor(R.attr.colorPrimary)

            }else{
                view.strokeColor = view.context.resolveColor(R.attr.colorSurface)
            }

            val chartIconRes = when(item.chartType){
                is BarChartDisplaySettings -> R.drawable.ic_bar_chart
                is LineChartDisplaySettings -> R.drawable.ic_line_chart
                is RadarChartDisplaySettings -> R.drawable.ic_radar_chart
                is ScatterChartDisplaySettings -> R.drawable.ic_scatter_chart
                is PieChartDisplaySettings -> R.drawable.ic_pie_chart
                is TextDisplaySettings -> R.drawable.ic_text_chart
                else -> R.drawable.ic_baseline_error
            }

            iconIv.setImageDrawable(ContextCompat.getDrawable(view.context, chartIconRes))

        }

        override fun unbindView(item: ChartListItem) {

        }


    }
}
