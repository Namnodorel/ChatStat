/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation.formelements.kotlin

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Switch
import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.view.implementation.utils.getString
import de.namnodorel.ofv.ValueChangeCallback
import de.namnodorel.ofv.annotations.ObjectFormElement

@ObjectFormElement("boolean")
class KotlinBooleanFormElement {

    fun attachView(prefill: Any?, fieldID: String, changeCallback: ValueChangeCallback, parent: ViewGroup) {

        val context = parent.context

        val vi = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val root = vi.inflate(R.layout.userinput_boolean, parent) as ViewGroup
        val view = root.getChildAt(root.childCount - 1)

        val sw = view.findViewById<Switch>(R.id.userinput_boolean_switch)

        if (prefill != null) {
            sw.isChecked = (prefill as Boolean)
        }

        sw.setOnCheckedChangeListener { _, isChecked -> changeCallback.updateVariable(isChecked) }

        sw.text = context.getString(fieldID)

    }

}
