/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation.formelements

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.MaterialCalendarView
import de.namnodorel.chatstat.R
import de.namnodorel.ofv.ValueChangeCallback
import de.namnodorel.ofv.annotations.ObjectFormElement
import org.joda.time.Interval
import org.joda.time.LocalDateTime
import java.util.*

@ObjectFormElement("org.joda.time.Interval")
class IntervalFormElement {

    fun attachView(prefill: Any?, fieldID: String, changeCallback: ValueChangeCallback, parent: ViewGroup) {

        val context = parent.context

        val vi = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val root = vi.inflate(R.layout.userinput_interval, parent) as ViewGroup
        val view = root.getChildAt(root.childCount - 1)

        val calView = view.findViewById<MaterialCalendarView>(R.id.interval_calendar_view)

        val selectedDates = LinkedList<CalendarDay>()
        if (prefill != null) {
            selectedDates.add(CalendarDay.from((prefill as Interval).start.year, prefill.start.monthOfYear, prefill.start.dayOfMonth))
            selectedDates.add(CalendarDay.from(prefill.end.year, prefill.end.monthOfYear, prefill.end.dayOfMonth))

            calView.selectRange(selectedDates[0], selectedDates[selectedDates.size - 1])
        }

        calView.setOnRangeSelectedListener { materialCalendarView, updatedRange ->
            val firstDate = updatedRange[0]
            val secondDate = updatedRange[updatedRange.size - 1]

            if (firstDate == null || secondDate == null) {
                changeCallback.updateVariable(null)

            } else {
                changeCallback.updateVariable(
                        Interval(
                                LocalDateTime(
                                        firstDate.year,
                                        firstDate.month,
                                        firstDate.day,
                                        0,
                                        0)
                                        .toDateTime(),
                                LocalDateTime(
                                        secondDate.year,
                                        secondDate.month,
                                        secondDate.day,
                                        0,
                                        0)
                                        .toDateTime()
                        )
                )
            }
        }

    }

}
