/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation.chart

import android.content.Context
import android.view.View

import com.github.mikephil.charting.charts.RadarChart
import com.github.mikephil.charting.data.RadarData
import com.github.mikephil.charting.data.RadarDataSet
import com.github.mikephil.charting.formatter.ValueFormatter

import de.namnodorel.chatstat.model.ColorRepository
import de.namnodorel.chatstat.model.chart.radar.RadarChartDisplaySettings
import de.namnodorel.chatstat.model.data.ChartSettings
import de.namnodorel.chatstat.model.groupselector.LooseTimeIntervalGroupSelector
import de.namnodorel.chatstat.model.groupselector.StandardTimeIntervalGroupSelector
import de.namnodorel.chatstat.view.ViewStyler
import de.namnodorel.chatstat.view.implementation.utils.resolveColor


class RadarChartStyler : ViewStyler<RadarData, RadarChartDisplaySettings> {
    override fun makeView(chartData: RadarData, chartSettings: ChartSettings<out RadarChartDisplaySettings>, colorRepo: ColorRepository, context: Context): View {

        for (irds in chartData.dataSets) {
            val dataSet = irds as RadarDataSet

            dataSet.color = colorRepo.getColor(dataSet.label)
        }

        val settings = chartSettings.displaySettings

        val chart = RadarChart(context)
        chart.apply {

            data = chartData
            data.apply {

                setDrawValues(settings.drawValuesEnabled)
                setValueTextColor(context.resolveColor(android.R.attr.textColorPrimary))

            }

            xAxis.apply {

                setDrawLabels(settings.drawXLabelsEnabled)
                textColor = context.resolveColor(android.R.attr.textColorPrimary)
                valueFormatter = object : ValueFormatter() {
                    override fun getFormattedValue(value: Float): String {
                        //MPAndroidChart always wants to display the 0th day of the week, for example - instead, we take the highest
                        //X value, because they are in the same spot
                        return if (value == 0f && (chartSettings.dataGroupSelector is StandardTimeIntervalGroupSelector || chartSettings.dataGroupSelector is LooseTimeIntervalGroupSelector)) {
                            chartSettings.dataGroupSelector.getXLabel(chartData.maxEntryCountSet.entryCount)
                        } else {
                            chartSettings.dataGroupSelector.getXLabel(value.toInt())
                        }
                    }
                }
            }

            yAxis.apply {

                val yTitle = chartSettings.dataTransformersObservable
                        .reduce(chartSettings.collectorType.name,
                                { title, transformer ->
                                    transformer
                                            .updatedYUnit
                                            .replace("{original}", title)
                                })
                        .blockingGet()

                title = yTitle

                setDrawLabels(settings.drawYLabelsEnabled)
                textColor = context.resolveColor(android.R.attr.textColorPrimary)
            }

            webColor = context.resolveColor(android.R.attr.textColorSecondary)
            webColorInner = context.resolveColor(android.R.attr.textColorSecondary)

            isHighlightPerTapEnabled = false
            legend.isEnabled = false
            description.isEnabled = false

        }

        return chart
    }
}
