/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view

import net.grandcentrix.thirtyinch.TiView
import net.grandcentrix.thirtyinch.callonmainthread.CallOnMainThread

import java.io.Serializable

import de.namnodorel.chatstat.model.persist.ComponentType


interface ChartCreatorView : TiView {

    @CallOnMainThread
    fun <T : Serializable> goToListComponent(componentType: ComponentType, args: List<T>)

    @CallOnMainThread
    fun <T : Serializable> goToSelectionComponent(type: ComponentType, preFill: T)

    @CallOnMainThread
    fun <T : Serializable> goToTypedListComponent(type: Class<out T>, preFill: List<T>)

    fun setResultAndFinish(chartID: Int?, result: Serializable)

    fun setChartTitle(text: String)

    fun setChartDescription(text: String)

    fun setPreTransformerSub(sub: String)
    fun setWarnPreTransformer(warn: Boolean)

    fun setCollectorTypeSub(sub: String)

    fun setDataGroupSelectorSub(sub: String)
    fun setWarnDataGroupSelector(warn: Boolean)

    fun setDataCollectorSub(sub: String)
    fun setWarnDataCollector(warn: Boolean)

    fun setTransformerSub(sub: String)
    fun setWarnTransformer(warn: Boolean)

    fun setDisplaySettingsSub(sub: String)
    fun setWarnDisplaySettings(warn: Boolean)
    fun setWarnTitle(warn: Boolean)

    fun getString(id: String): String
}
