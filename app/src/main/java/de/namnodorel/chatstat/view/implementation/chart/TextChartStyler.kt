/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation.chart

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView

import java.text.DecimalFormat

import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.model.ColorRepository
import de.namnodorel.chatstat.model.chart.text.TextDisplaySettings
import de.namnodorel.chatstat.model.data.ChartSettings
import de.namnodorel.chatstat.view.ViewStyler


class TextChartStyler : ViewStyler<Map<String, Any>, TextDisplaySettings> {
    override fun makeView(data: Map<String, Any>, chart: ChartSettings<out TextDisplaySettings>, colorRepo: ColorRepository, context: Context): View {

        val settings = chart.displaySettings

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.text_chart, null)

        var dataSetLabel = data["dataSetLabel"] as String?
        val key = data["key"] as Int?
        val value = data["value"] as Float?
        var valueLabel = data["valueLabel"] as String?

        val xLabel = chart.dataGroupSelector.getXLabel(key!!)

        val formattedValue: String
        if (value!! % 1 == 0f) {
            val smallerFormat = DecimalFormat("###,###,###")
            formattedValue = smallerFormat.format(value)
        } else {
            val defaultFormat = DecimalFormat("###,###,###.##")
            formattedValue = defaultFormat.format(value)
        }

        if (valueLabel == null) {
            valueLabel = ""
        }
        if (dataSetLabel == null) {
            dataSetLabel = ""
        }

        val preTextTv = view.findViewById<TextView>(R.id.pre_text)
        val mainTextTv = view.findViewById<TextView>(R.id.main_text)
        val postTextTv = view.findViewById<TextView>(R.id.post_text)

        preTextTv.text = settings.preText
                .replace("[dataset]", dataSetLabel)
                .replace("[x]", xLabel)
                .replace("[val]", formattedValue)
                .replace("[val-label]", valueLabel)

        mainTextTv.text = settings.mainText
                .replace("[dataset]", dataSetLabel)
                .replace("[x]", xLabel)
                .replace("[val]", formattedValue)
                .replace("[val-label]", valueLabel)

        postTextTv.text = settings.postText
                .replace("[dataset]", dataSetLabel)
                .replace("[x]", xLabel)
                .replace("[val]", formattedValue)
                .replace("[val-label]", valueLabel)

        return view
    }
}
