/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation.formelements.kotlin

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.Switch
import android.widget.TextView
import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.view.implementation.utils.getString
import de.namnodorel.ofv.ValueChangeCallback
import de.namnodorel.ofv.annotations.ObjectFormElement
import kotlin.math.abs

@ObjectFormElement("int")
class KotlinIntFormElement {

    val min: Int = 0

    val max: Int = 0

    var allowAuto: Boolean = false

    fun attachView(prefill: Any?, fieldID: String, changeCallback: ValueChangeCallback, parent: ViewGroup) {
        var prefill = prefill

        val context = parent.context

        val vi = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val root = vi.inflate(R.layout.userinput_int, parent) as ViewGroup
        val view = root.getChildAt(root.childCount - 1)

        val labelTv = view.findViewById<TextView>(R.id.userinput_int_label)

        val autoSwitch = view.findViewById<Switch>(R.id.auto_switch)
        autoSwitch.visibility = if (allowAuto) View.VISIBLE else View.GONE

        val seekBar = view.findViewById<SeekBar>(R.id.userinput_int_seekbar)

        val valueView = view.findViewById<TextView>(R.id.userinput_int_value)

        if (!allowAuto && prefill == null) {
            prefill = min
        } else if (allowAuto && prefill == null) {
            seekBar.isEnabled = false
            valueView.text = context.getString("auto")
        }

        autoSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                changeCallback.updateVariable(null)
                seekBar.isEnabled = false
                valueView.text = context.getString("auto")
            } else {
                changeCallback.updateVariable(min)
                seekBar.progress = 0
                seekBar.isEnabled = true
                valueView.text = min.toString()
            }
        }
        autoSwitch.isChecked = prefill == null

        seekBar.max = max + abs(min)


        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                changeCallback.updateVariable(progress - abs(min))
                valueView.text = (progress - abs(min)).toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

            }
        })

        labelTv.text = context.getString(fieldID)

        if (prefill != null) {
            seekBar.progress = prefill as Int + abs(min)
            valueView.text = prefill.toString()
        }
    }

}
