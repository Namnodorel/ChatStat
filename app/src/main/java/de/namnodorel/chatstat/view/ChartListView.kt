/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view

import net.grandcentrix.thirtyinch.TiView
import net.grandcentrix.thirtyinch.callonmainthread.CallOnMainThread

import java.io.Serializable

import de.namnodorel.chatstat.model.chart.DisplaySettings
import de.namnodorel.chatstat.model.data.ChartSettings


interface ChartListView : TiView {

    fun getSelection(): Set<Int>

    @CallOnMainThread
    fun checkAndReturnSaveFileInput()

    @CallOnMainThread
    fun returnSaveFileOutput()

    @CallOnMainThread
    fun addEntry(title: String, description: String, chartType: DisplaySettings, valid: Boolean)

    @CallOnMainThread
    fun replaceEntry(id: Int, title: String, description: String, editable: Boolean, chartType: DisplaySettings, valid: Boolean)

    @CallOnMainThread
    fun deleteEntry(id: Int)

    @CallOnMainThread
    fun clearEntries()

    @CallOnMainThread
    fun selectEntry(id: Int)

    @CallOnMainThread
    fun deselectEntry(id: Int)

    @CallOnMainThread
    fun clearSelection()

    @CallOnMainThread
    fun goToChartEditor(chartID: Int, r: ChartSettings<out DisplaySettings>)

    @CallOnMainThread
    fun goToChartCreator(r: ChartSettings<out DisplaySettings>)

    @CallOnMainThread
    fun showSaveSuccessful()

    fun setResultAndFinish(result: Serializable)

}
