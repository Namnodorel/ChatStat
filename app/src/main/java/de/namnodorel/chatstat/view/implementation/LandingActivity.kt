/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation

import android.content.Intent
import android.os.Bundle
import com.afollestad.materialdialogs.MaterialDialog
import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.presenter.LandingPresenter
import de.namnodorel.chatstat.presenter.ChartListPresenter
import de.namnodorel.chatstat.view.LandingView
import de.namnodorel.chatstat.view.Theme
import de.namnodorel.creditz.views.AboutActivity
import de.namnodorel.creditz.views.CreditzConfig
import de.namnodorel.creditz.views.InfoButton
import net.grandcentrix.thirtyinch.TiActivity
import org.androidannotations.annotations.Click
import org.androidannotations.annotations.EActivity
import java.util.*

@EActivity(R.layout.activity_landing)
open class LandingActivity : TiActivity<LandingPresenter, LandingView>(), LandingView {

    private lateinit var dialog: MaterialDialog

    override fun providePresenter(): LandingPresenter {
        val prefs = getSharedPreferences("de.namnodorel.chatstat", MODE_PRIVATE)
        val themeString = prefs.getString(Theme.SETTINGS_KEY, Theme.DARK_BLUE.name)!!

        return LandingPresenter(Theme.valueOf(themeString))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTheme(getThemeIdFor(presenter.theme))

        dialog = MaterialDialog(this)
                .title(R.string.import_dialog_title)
                .message(R.string.import_tutorial_text)
                .positiveButton(R.string.understood) {
                    confirmBtnClicked()
                }
    }

    override fun openImportDialog() {
        dialog.show()
    }

    override fun closeImportDialog() {
        dialog.dismiss()
    }

    override fun goToChartManagement() {
        val intent = Intent(this, ChartListActivity_::class.java)
        intent.putExtra(ChartListPresenter.PURPOSE_KEY, ChartListPresenter.PURPOSE_BROWSE)
        startActivity(intent)
    }

    override fun goToAboutScreen() {

        val intent = Intent(this, AboutActivity::class.java)
        val config = CreditzConfig()
        config.theme = getThemeIdFor(presenter.theme)
        config.aboutTitle = getString(R.string.app_name)
        config.aboutDescription = getString(R.string.app_description)

        val infoButtons = LinkedList<InfoButton>()
        infoButtons.add(InfoButton(getString(R.string.project_page), "https://gitlab.com/Namnodorel/ChatStat"))
        infoButtons.add(InfoButton(getString(R.string.changelog), "https://gitlab.com/Namnodorel/ChatStat/releases"))
        //TODO Change this to the actual help desk mail when the feature is available
        infoButtons.add(InfoButton(getString(R.string.bug_report), "e@mail.com"))
        config.infoButtons = infoButtons

        intent.putExtra(AboutActivity.CREDITZ_CONFIG, config)
        startActivity(intent)

    }

    override fun changeThemeTo(theme: Theme) {
        setTheme(getThemeIdFor(theme))

        val prefs = getSharedPreferences("de.namnodorel.chatstat", MODE_PRIVATE)
        prefs.edit().putString(Theme.SETTINGS_KEY, theme.name).apply()

        recreate()
    }

    private fun getThemeIdFor(theme: Theme): Int{
        return when(theme){
            Theme.DARK_BLUE -> R.style.DarkBlue_NoActionBar
            Theme.LIGHT_BLUE -> R.style.LightBlue_NoActionBar
        }
    }

    @Click(R.id.import_btn)
    fun importBtnClicked() {
        presenter.importBtnClicked()
    }

    @Click(R.id.manage_charts_btn)
    fun chartBtnClicked() {
        presenter.chartListBtnClicked()
    }

    @Click(R.id.change_theme_btn)
    fun chaneThemeClicked(){
        presenter.changeThemeClicked()
    }

    @Click(R.id.about_btn)
    fun aboutClicked(){
        presenter.aboutClicked()
    }

    private fun confirmBtnClicked() {
        presenter.confirmationBtnClicked()
    }
}
