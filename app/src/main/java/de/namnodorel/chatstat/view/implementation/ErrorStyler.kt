/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.model.ColorRepository
import de.namnodorel.chatstat.model.chart.DisplaySettings
import de.namnodorel.chatstat.model.chart.ErrorData
import de.namnodorel.chatstat.model.data.ChartSettings
import de.namnodorel.chatstat.view.ViewStyler


class ErrorStyler : ViewStyler<ErrorData, DisplaySettings> {
    override fun makeView(data: ErrorData, settings: ChartSettings<out DisplaySettings>, colorRepo: ColorRepository, context: Context): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.error, null)

        val userDescriptionTv = view.findViewById<TextView>(R.id.error_subtext)
        val explanationTv = view.findViewById<TextView>(R.id.error_explanation)

        val copyErrorMsgBtn = view.findViewById<Button>(R.id.copy_error_button)

        userDescriptionTv.text = data.error.message
        if(data.explanation != null){
            explanationTv.text = data.explanation
        }

        copyErrorMsgBtn.setOnClickListener {
            val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("error", data.error.localizedMessage)
            clipboard.primaryClip = clip

            Toast.makeText(context, context.resources.getString(R.string.copied_error), Toast.LENGTH_LONG).show()
        }

        return view
    }
}
