/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Spinner

import net.grandcentrix.thirtyinch.TiActivity

import org.androidannotations.annotations.AfterViews
import org.androidannotations.annotations.EActivity
import org.androidannotations.annotations.Extra
import org.androidannotations.annotations.ViewById

import java.io.Serializable
import java.util.ArrayList
import java.util.LinkedList

import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.model.persist.ComponentType
import de.namnodorel.chatstat.presenter.SingleSelectionWithSettingsPresenter
import de.namnodorel.chatstat.view.SingleSelectionWithSettingsView
import de.namnodorel.chatstat.view.Theme
import de.namnodorel.chatstat.view.implementation.utils.getString
import de.namnodorel.ofv.FormViewGenerator

@EActivity(R.layout.activity_single_selection)
open class SingleSelectionWithSettingsActivity : TiActivity<SingleSelectionWithSettingsPresenter, SingleSelectionWithSettingsView>(), SingleSelectionWithSettingsView {

    companion object {
        const val COMPONENT_TYPE = "class_options"
        const val PREFILL_VALUE = "prefill_value"
        const val RESULT = "single_selection_with_settings_result"
    }

    //No generics here 'cause AA doesn't support it
    @Extra(COMPONENT_TYPE)
    internal lateinit var type: ComponentType

    @Extra(PREFILL_VALUE)
    internal lateinit var prefillValue: Serializable

    @ViewById(R.id.chart_selection_spinner)
    internal lateinit var spinner: Spinner

    @ViewById(R.id.chart_selection_settings_space)
    internal lateinit var settingsSpace: LinearLayout

    private lateinit var adapter: ArrayAdapter<String>

    override fun providePresenter(): SingleSelectionWithSettingsPresenter {
        return SingleSelectionWithSettingsPresenter(type, prefillValue)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val prefs = getSharedPreferences("de.namnodorel.chatstat", MODE_PRIVATE)
        val themeString = prefs.getString(Theme.SETTINGS_KEY, Theme.DARK_BLUE.name)!!
        setTheme(getThemeIdFor(Theme.valueOf(themeString)))
    }

    @AfterViews
    fun viewInit() {

        title = this.getString(type.name.toLowerCase())
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        adapter = ArrayAdapter(this, R.layout.spinner_item, ArrayList())
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item)
        spinner.adapter = adapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                presenter.optionSelected(id.toInt())
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                presenter.optionSelected(-1)
            }
        }
    }

    private fun getThemeIdFor(theme: Theme): Int{
        return when(theme){
            Theme.DARK_BLUE -> R.style.DarkBlue
            Theme.LIGHT_BLUE -> R.style.LightBlue
        }
    }

    override fun setSelectableOptions(options: List<String>) {

        val translatedOptions = LinkedList<String>()
        for (s in options) {
            translatedOptions.add(this.getString(s + "_simple_name"))
        }

        adapter.clear()
        adapter.addAll(translatedOptions)
        adapter.notifyDataSetChanged()
    }

    override fun selectOption(index: Int) {
        spinner.setSelection(index)
    }

    override fun generateSettingsFor(prefill: Any) {
        val generator = FormViewGenerator.find(prefill)
        settingsSpace.addView(generator.generateFormView(baseContext))
    }

    override fun clearSettings() {
        settingsSpace.removeAllViews()
    }

    //Since this is not a regular start for result but just a submenu in reality, pressing back should actually deliver the result
    override fun onBackPressed() {
        presenter.backPressed()
    }

    override fun onSupportNavigateUp(): Boolean {
        presenter.backPressed()
        return true
    }

    override fun setResultAndFinish(result: Serializable) {
        val intent = Intent()
        intent.putExtra(RESULT, result)
        setResult(RESULT_OK, intent)
        finish()
        overridePendingTransition(R.transition.right_in, R.transition.right_out)
    }
}
