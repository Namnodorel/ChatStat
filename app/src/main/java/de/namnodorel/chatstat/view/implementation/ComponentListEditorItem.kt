/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation

import android.view.View
import android.widget.TextView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.drag.IDraggable
import com.mikepenz.fastadapter.items.AbstractItem
import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.view.implementation.utils.resolveColor


class ComponentListEditorItem(
        private val title: String,
        private val subTitle: String,
        private val valid: Boolean)
    : AbstractItem<ComponentListEditorItem.ViewHolder>(),
        IDraggable {

    override fun getViewHolder(v: View): ViewHolder {
        return ViewHolder(v)
    }

    override val type = 0

    override val layoutRes = R.layout.component_list_editor_item

    override val isDraggable = true

    class ViewHolder(itemView: View) : FastAdapter.ViewHolder<ComponentListEditorItem>(itemView) {

        private val titleTv: TextView = itemView.findViewById(R.id.chart_creator_list_item_title)
        private val subTitleTv: TextView = itemView.findViewById(R.id.chart_creator_list_item_subtitle)

        override fun bindView(item: ComponentListEditorItem, payloads: MutableList<Any>) {
            titleTv.text = item.title
            if(item.subTitle.isEmpty()){
                subTitleTv.visibility = View.GONE
            }else{
                subTitleTv.visibility = View.VISIBLE
                subTitleTv.text = item.subTitle
            }

            if(item.valid){
                titleTv.setTextColor(titleTv.context.resolveColor(android.R.attr.textColorPrimary))
            }else{
                titleTv.setTextColor(titleTv.context.resolveColor(R.attr.colorError))
            }
        }

        override fun unbindView(item: ComponentListEditorItem) {
            titleTv.text = ""
            subTitleTv.text = ""
        }
    }
}
