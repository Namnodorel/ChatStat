/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view

import net.grandcentrix.thirtyinch.TiView
import net.grandcentrix.thirtyinch.callonmainthread.CallOnMainThread
import java.io.Serializable


interface ChartCreatorListComponentView : TiView {

    @CallOnMainThread
    fun addItem(title: String, subTitle: String = "", valid: Boolean)

    @CallOnMainThread
    fun removeItem(index: Int)

    @CallOnMainThread
    fun clearItems()

    @CallOnMainThread
    fun swapItems(oldPosition: Int, newPosition: Int)

    fun setResultAndFinish(result: Serializable)

    @CallOnMainThread
    fun chooseFromDialog(options: List<String>)

    @CallOnMainThread
    fun goToDetailsView(preFill: Serializable)

    fun getString(id: String): String
}
