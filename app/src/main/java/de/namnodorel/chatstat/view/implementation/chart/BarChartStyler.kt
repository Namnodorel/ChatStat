/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation.chart

import android.content.Context
import android.view.View
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.HorizontalBarChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import de.namnodorel.chatstat.model.ColorRepository
import de.namnodorel.chatstat.model.chart.bar.BarChartDisplaySettings
import de.namnodorel.chatstat.model.data.ChartSettings
import de.namnodorel.chatstat.view.ViewStyler
import de.namnodorel.chatstat.view.implementation.utils.resolveColor
import java.text.DecimalFormat
import java.util.*
import kotlin.math.ceil
import kotlin.math.floor


class BarChartStyler : ViewStyler<BarData, BarChartDisplaySettings> {
    override fun makeView(chartData: BarData, chartSettings: ChartSettings<out BarChartDisplaySettings>, colorRepo: ColorRepository, context: Context): View {

        //Find all labels, retrieve their color from the color repository,
        //and assign the colors to the datasets. If there are no stacked values/they have no label,
        //the dataset label is used.
        for (ids in chartData.dataSets) {
            val dataSet = ids as BarDataSet

            val colors = LinkedList<Int>()

            if (dataSet.isStacked) {

                for (entry in dataSet.values) {
                    if (entry.yVals != null) {
                        for (i in entry.yVals.indices) {

                            val labels = entry.data as List<String>
                            if (labels.size > i) {
                                colors.add(colorRepo.getColor(labels[i]))
                            } else {
                                colors.add(colorRepo.getColor(dataSet.label))
                            }

                        }
                    } else {
                        colors.add(colorRepo.getColor(dataSet.label))
                    }
                }

            } else {
                colors.add(colorRepo.getColor(dataSet.label))
            }

            dataSet.colors = colors
        }

        val settings = chartSettings.displaySettings

        val chart: BarChart

        if (settings.isVertical) {
            chart = BarChart(context)
        } else {
            chart = HorizontalBarChart(context)
        }

        chart.apply {

            data = chartData
            data.apply {

                setDrawValues(settings.drawBarValuesEnabled)
                setValueTextColor(context.resolveColor(android.R.attr.textColorPrimary))
                setValueFormatter(object : ValueFormatter() {
                    private val defaultFormat = DecimalFormat("###,###,###.##")

                    override fun getBarStackedLabel(value: Float, stackIndex: Int, entry: BarEntry): String {
                        return if (value != 0f) {
                            if ((entry.data as List<String>).size > 0 && (entry.data as List<String>).size > stackIndex) {
                                defaultFormat.format(value.toDouble()) + (entry.data as List<String>)[stackIndex]
                            } else {
                                defaultFormat.format(value.toDouble())
                            }
                        } else {
                            ""
                        }
                    }

                    override fun getFormattedValue(value: Float): String {
                        return if (value == 0f) {
                            ""
                        } else defaultFormat.format(value.toDouble())

                    }
                })

            }

            axisLeft.apply {

                val yTitle = chartSettings.dataTransformersObservable
                        .reduce(chartSettings.collectorType.name,
                                { title, transformer ->
                                    transformer
                                            .updatedYUnit
                                            .replace("{original}", title)
                                })
                        .blockingGet()

                title = yTitle
                textColor = context.resolveColor(android.R.attr.textColorPrimary)

                axisMinimum = 0f
                labelCount = 5
                setDrawLabels(settings.drawYLabelsEnabled)

                gridColor = context.resolveColor(android.R.attr.textColorSecondary)
                setDrawGridLines(settings.drawYGrid)
                axisLineColor = context.resolveColor(android.R.attr.textColorSecondary)
                setDrawAxisLine(settings.drawAxisLine)
            }

            xAxis.apply {

                position = XAxis.XAxisPosition.BOTTOM
                granularity = 1f

                textColor = context.resolveColor(android.R.attr.textColorPrimary)
                gridColor = context.resolveColor(android.R.attr.textColorSecondary)
                setDrawGridLines(settings.drawXGrid)
                axisLineColor = context.resolveColor(android.R.attr.textColorSecondary)
                setDrawAxisLine(settings.drawAxisLine)


                labelCount = floor(settings.initialZoom.toFloat() / 2).toInt()
                setDrawLabels(settings.drawXLabelsEnabled)
                valueFormatter = object : ValueFormatter() {
                    override fun getFormattedValue(value: Float): String {
                        return if (value != ceil(value.toDouble()).toFloat()) {
                            ""
                        } else {
                            return chartSettings.dataGroupSelector.getXLabel(value.toInt())
                        }

                    }
                }

            }

            isDragEnabled = true
            isDragDecelerationEnabled = true
            isScaleYEnabled = false
            isScaleXEnabled = settings.zoomEnabled
            setPinchZoom(settings.zoomEnabled)
            isDoubleTapToZoomEnabled = settings.zoomEnabled

            setBorderColor(context.resolveColor(android.R.attr.textColorPrimary))

            setVisibleXRangeMinimum(0f)
            setVisibleXRangeMaximum(settings.initialZoom.toFloat())

            isHighlightPerTapEnabled = false
            isHighlightPerDragEnabled = false
            isHighlightFullBarEnabled = false
            legend.isEnabled = false
            description.isEnabled = false
            axisRight.isEnabled = false

            if (chartData.dataSetCount > 1) {

                //The space required for one group is calculated as follows:
                //mDataSets.size() * (mBarWidth + barSpace) = 1
                //when a barSpace is defined to be 1/4 of the width of a bar
                //mDataSets.size() * (mBarWidth + mBarWidth/4) = 1
                //Solving for mBarWidth:
                //mBarWidth = 4/(5*mDataSets.size())

                val newWidth = 4f / (5f * chartData.dataSetCount)

                data.barWidth = newWidth

                groupBars(newWidth / 4)
            }

        }

        return chart
    }
}
