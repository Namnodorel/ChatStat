/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.content.res.Resources
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageButton
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.children
import com.github.mikephil.charting.data.*
import com.google.android.material.card.MaterialCardView
import com.jaredrummler.android.colorpicker.ColorPickerDialog
import com.jaredrummler.android.colorpicker.ColorPickerDialogListener
import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.model.ColorRepository
import de.namnodorel.chatstat.model.chart.DisplaySettings
import de.namnodorel.chatstat.model.chart.ErrorData
import de.namnodorel.chatstat.model.chart.bar.BarChartDisplaySettings
import de.namnodorel.chatstat.model.chart.line.LineChartDisplaySettings
import de.namnodorel.chatstat.model.chart.pie.PieChartDisplaySettings
import de.namnodorel.chatstat.model.chart.radar.RadarChartDisplaySettings
import de.namnodorel.chatstat.model.chart.scatter.ScatterChartDisplaySettings
import de.namnodorel.chatstat.model.chart.text.TextDisplaySettings
import de.namnodorel.chatstat.model.data.ChartSettings
import de.namnodorel.chatstat.presenter.ChartPresenter
import de.namnodorel.chatstat.presenter.ChartListPresenter
import de.namnodorel.chatstat.view.ChartView
import de.namnodorel.chatstat.view.Theme
import de.namnodorel.chatstat.view.implementation.chart.*
import de.namnodorel.chatstat.view.implementation.utils.StupidWorkaround
import de.namnodorel.chatstat.view.implementation.utils.getString
import net.grandcentrix.thirtyinch.TiActivity
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EActivity
import org.androidannotations.annotations.Extra
import org.androidannotations.annotations.ViewById
import java.io.FileNotFoundException
import java.text.SimpleDateFormat
import java.util.*

@EActivity(R.layout.activity_chart)
open class ChartActivity : TiActivity<ChartPresenter, ChartView>(), ChartView, ColorPickerDialogListener {

    companion object {
        private const val SELECT_CHART = 0
    }

    @ViewById(R.id.chart_content_layout)
    internal lateinit var chartLayout: LinearLayoutCompat

    @Extra(Intent.EXTRA_STREAM)
    lateinit var intentArgs: ArrayList<*>

    @Bean
    internal lateinit var presenter: ChartPresenter

    override fun providePresenter(): ChartPresenter {

        //Getting these arguments needs to happen here, because they require a Context (or Android specific components)
        //and the Presenter should be kept POJ

        presenter.setAdditionalArgs(getCurrentLocale(), getUserCustomTimeFormat())

        if (intentArgs[0] != null) {
            try {

                if(intentArgs[0] !is Uri){
                    return presenter
                }

                val uri = intentArgs[0] as Uri
                val type = MimeTypeMap.getSingleton().getExtensionFromMimeType(contentResolver.getType(uri))

                presenter.setSource(type!!, contentResolver.openInputStream(uri)!!)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }

        }
        return presenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val prefs = getSharedPreferences("de.namnodorel.chatstat", MODE_PRIVATE)
        val themeString = prefs.getString(Theme.SETTINGS_KEY, Theme.DARK_BLUE.name)!!
        setTheme(getThemeIdFor(Theme.valueOf(themeString)))
    }

    private fun getThemeIdFor(theme: Theme): Int{
        return when(theme){
            Theme.DARK_BLUE -> R.style.DarkBlue_NoActionBar
            Theme.LIGHT_BLUE -> R.style.LightBlue_NoActionBar
        }
    }

    override fun addChart(chart: ChartSettings<*>, data: Any?, colorRepository: ColorRepository): Int? {

        val view = prepareView(chart, data, colorRepository)

        chartLayout.addView(view)

        chartLayout.invalidate()

        return chartLayout.indexOfChild(view)
    }

    override fun removeChart(chartId: Int) {

        chartLayout.removeViewAt(chartId)

        chartLayout.invalidate()
    }

    override fun updateChart(chartId: Int, chart: ChartSettings<*>, data: Any?, colorRepository: ColorRepository) {

        val view = prepareView(chart, data, colorRepository)

        chartLayout.removeViewAt(chartId)
        chartLayout.addView(view, chartId)
        chartLayout.invalidate()
    }

    override fun hasCharts(): Boolean {
        return chartLayout.childCount > 0
    }

    override fun promptUserToSelectChart() {
        val intent = Intent(this, ChartListActivity_::class.java)
        intent.putExtra(ChartListPresenter.PURPOSE_KEY, ChartListPresenter.PURPOSE_SELECT)
        startActivityForResult(intent, SELECT_CHART)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == SELECT_CHART) {
            when (resultCode) {
                RESULT_OK -> {
                    val result = data!!.getSerializableExtra(ChartListActivity.SELECTION_RESULT) as Set<ChartSettings<out DisplaySettings>>
                    getPresenter().receivedChartSelection(result)
                }
                RESULT_CANCELED -> //User pressed back or sth
                    finish()

                else -> throw IllegalStateException("Received code $requestCode from ListComponent; this should not be possible!")
            }
        } else {
            Log.e("ChatStat", "Illegal onActivityResult(), finishing.")
            finish()
        }
    }

    override fun goToLandingPage() {
        val intent = Intent(this, LandingActivity_::class.java)
        startActivity(intent)
        finish()
    }

    private fun prepareView(chart: ChartSettings<out DisplaySettings>, data: Any?, colorRepository: ColorRepository): View {

        val settings = chart.displaySettings
        val largeView = settings.size == null && data != null

        val containerResId =
                if(!largeView){
                    R.layout.basic_chart_container
                }else{
                    R.layout.large_chart_container
                }

        val vi = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val cardContainer = vi.inflate(containerResId, null) as MaterialCardView

        val containerLp = LinearLayoutCompat.LayoutParams(
                LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.WRAP_CONTENT
        )
        containerLp.setMargins(dpToPixel(16), dpToPixel(16), dpToPixel(16), dpToPixel(0))
        cardContainer.layoutParams = containerLp


        val chartTitleTv = cardContainer.findViewById<TextView>(R.id.chart_title)
        chartTitleTv.text = chart.title

        if(!largeView){
            val contentView = cardContainer.findViewById<FrameLayout>(R.id.chart_content)

            val contentLp = contentView.layoutParams
            contentLp.width = ConstraintLayout.LayoutParams.MATCH_PARENT

            if(settings.size == null){
                contentLp.height = 300
            }else{
                contentLp.height = settings.size
            }

            contentView.layoutParams = contentLp

            val view = makeGraphView(chart, data, colorRepository)
            contentView.addView(view)

        }else{

            val containerLayout = cardContainer.findViewById<ConstraintLayout>(R.id.chart_container_layout)
            containerLayout.setOnClickListener {

                presenter.largeGraphRequested(chart, data)

            }

        }

        return cardContainer
    }

    private fun makeGraphView(chart: ChartSettings<out DisplaySettings>, data: Any?, colorRepository: ColorRepository): View {

        return when (data) {
            null -> LoadingStyler()
                    .makeView(null, chart, colorRepository, this)

            is ErrorData -> ErrorStyler()
                    .makeView(data, chart, colorRepository, this)

            is BarData -> BarChartStyler()
                    .makeView(data, chart as ChartSettings<out BarChartDisplaySettings>, colorRepository, this)

            is RadarData -> RadarChartStyler()
                    .makeView(data, chart as ChartSettings<out RadarChartDisplaySettings>, colorRepository, this)

            is ScatterData -> ScatterChartStyler()
                    .makeView(data, chart as ChartSettings<out ScatterChartDisplaySettings>, colorRepository, this)

            is PieData -> PieChartStyler()
                    .makeView(data, chart as ChartSettings<out PieChartDisplaySettings>, colorRepository, this)

            is LineData -> LineChartStyler()
                    .makeView(data, chart as ChartSettings<out LineChartDisplaySettings>, colorRepository, this)

            is Map<*, *> -> TextChartStyler()
                    .makeView(data as MutableMap<String, Any>, chart as ChartSettings<out TextDisplaySettings>, colorRepository, this)

            else -> throw IllegalArgumentException("Data type not recognized: ${data.javaClass}")
        }
    }

    override fun goToFullscreenChartView(chart: ChartSettings<out DisplaySettings>, data: Any, colorRepository: ColorRepository) {
        val intent = Intent(this, FullscreenChartActivity_::class.java)
        intent.putExtra(FullscreenChartActivity.CHART_CHART, chart)
        intent.putExtra(FullscreenChartActivity.COLOR_REPO, colorRepository)

        StupidWorkaround.staticReferenceToDataObject = data

        startActivity(intent)
    }

    override fun addLegendEntry(label: String, color: Int) {

        val contentView = findViewById<LinearLayoutCompat>(R.id.drawer_content)

        val vi = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val root = vi.inflate(R.layout.legend_entry, contentView) as LinearLayoutCompat
        val legendEntryView = root.getChildAt(root.childCount - 1)

        val labelTv = legendEntryView.findViewById<TextView>(R.id.legend_label)
        val colorBtn = legendEntryView.findViewById<AppCompatImageButton>(R.id.legend_color)

        labelTv.text = label

        colorBtn.supportImageTintList = ColorStateList.valueOf(color)

        val clickListener: View.OnClickListener = View.OnClickListener{
            presenter.colorChangeClicked(label)
        }

        colorBtn.setOnClickListener(clickListener)
        legendEntryView.setOnClickListener(clickListener)
    }

    override fun setLegendColorForLabel(label: String, newColor: Int) {
        val contentView = findViewById<LinearLayoutCompat>(R.id.drawer_content)

        for(legendEntryView in contentView.children){
            val labelTv = legendEntryView.findViewById<TextView>(R.id.legend_label)

            if(labelTv.text == label){
                val colorBtn = legendEntryView.findViewById<AppCompatImageButton>(R.id.legend_color)
                colorBtn.supportImageTintList = ColorStateList.valueOf(newColor)
                return
            }
        }
    }

    override fun showColorChangeDialog(oldColor: Int) {
        ColorPickerDialog.newBuilder()
                .setColor(oldColor)
                .show(this)
    }

    override fun onColorSelected(dialogId: Int, color: Int) {
        presenter.colorChanged(color)
    }

    override fun onDialogDismissed(dialogId: Int) {
        presenter.colorUnchanged()
    }

    private fun getCurrentLocale(): Locale {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            resources.configuration.locales.get(0)
        } else {
            resources.configuration.locale
        }
    }

    //The user can have custom time formatting enabled, which parsing needs to respect
    private fun getUserCustomTimeFormat(): SimpleDateFormat {
        return android.text.format.DateFormat.getTimeFormat(this) as SimpleDateFormat
    }

    private fun dpToPixel(dp: Int): Int{
        return dp * (resources.displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT)
    }

    override fun getString(id: String): String {
        return baseContext.getString(id)
    }
}
