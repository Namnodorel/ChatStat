/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation.utils


object StupidWorkaround {

    /* This is here to pass non-serializable non-parcelable objects to another activity.
        Why, you ask? Well, in order to display charts alone in a separate activity, we
        need to send their data over to that newly created activity. Unfortunately,
        MPAndroidChart doesn't feel like treating its data classes like, you know, data
        classes, and thus does not have them implement Serializable, Parcelable, or anything
        similar. Even worse, it uses objects from the Android Framework which can't be
        serialized without making significant changes to the MPAndroidChart implementation.
        And I'm not willing to do that for something that should be this simple.

        So, as a stupid, dirty workaround, the chart data will be passed between activities
        using this global static value. If MPAndroidChart is even worse with their data class
        implementation than expected, this might leak context, but I really don't see a
        way around it.

        This has apparently been an issue for a while now, but nobody has really done
        anything about it: https://github.com/PhilJay/MPAndroidChart/issues/2592

        (Yes, I just made a 18 line comment to explain one line of code)
     */

    var staticReferenceToDataObject: Any? = null

}