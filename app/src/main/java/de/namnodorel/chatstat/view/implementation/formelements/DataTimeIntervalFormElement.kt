/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation.formelements

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.android.material.chip.ChipGroup
import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.model.data.DataTimeInterval
import de.namnodorel.ofv.ValueChangeCallback
import de.namnodorel.ofv.annotations.ObjectFormElement
import java.util.*

@ObjectFormElement("de.namnodorel.chatstat.model.data.DataTimeInterval")
class DataTimeIntervalFormElement {

    fun attachView(prefill: Any?, fieldID: String, changeCallback: ValueChangeCallback, parent: ViewGroup) {

        val context = parent.context

        val vi = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val root = vi.inflate(R.layout.userinput_datatimeinterval, parent) as ViewGroup
        val view = root.getChildAt(root.childCount - 1)

        val viewIds = HashMap<DataTimeInterval, Int>()
        viewIds[DataTimeInterval.HOUR] = R.id.chip_hour
        viewIds[DataTimeInterval.DAY] = R.id.chip_day
        viewIds[DataTimeInterval.WEEK] = R.id.chip_week
        viewIds[DataTimeInterval.MONTH] = R.id.chip_month
        viewIds[DataTimeInterval.YEAR] = R.id.chip_year

        val groupedChips = view as ChipGroup

        if (prefill != null) {
            groupedChips.check(viewIds[prefill]!!)
        }

        groupedChips.setOnCheckedChangeListener { chipGroup, chipId ->
            for (dti in viewIds.keys) {
                if (viewIds[dti] == chipId) {
                    changeCallback.updateVariable(dti)
                    break
                }
            }
        }

    }

}
