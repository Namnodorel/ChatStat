/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.select.SelectExtension
import com.mikepenz.fastadapter.select.getSelectExtension
import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.model.chart.DisplaySettings
import de.namnodorel.chatstat.model.data.ChartSettings
import de.namnodorel.chatstat.presenter.ChartListPresenter
import de.namnodorel.chatstat.view.ChartListView
import de.namnodorel.chatstat.view.Theme
import de.namnodorel.chatstat.view.implementation.ChartCreatorActivity.Companion.CHART_ID
import de.namnodorel.chatstat.view.implementation.ChartCreatorActivity.Companion.CHART_RESULT
import net.grandcentrix.thirtyinch.TiActivity
import org.androidannotations.annotations.*
import java.io.FileNotFoundException
import java.io.Serializable

@EActivity(R.layout.activity_chart_list)
open class ChartListActivity : TiActivity<ChartListPresenter, ChartListView>(), ChartListView {

    companion object {
        private const val CREATE_CHART_REQUEST = 0
        private const val EDIT_CHART_REQUEST = 1

        const val SELECTION_RESULT = "selection_result"

        private const val SAVE_FILE_NAME = "saved_charts"
    }

    @ViewById(R.id.chart_list)
    internal lateinit var contentView: RecyclerView

    @ViewById(R.id.add_chart)
    internal lateinit var addChartBtn: FloatingActionButton

    @ViewById(R.id.chart_selection_done)
    internal lateinit var selectionDoneBtn: FloatingActionButton

    @Bean
    internal lateinit var presenter: ChartListPresenter

    private lateinit var itemAdapter: ItemAdapter<ChartListItem>
    private lateinit var selectionExt: SelectExtension<ChartListItem>

    override fun providePresenter(): ChartListPresenter {
        return presenter
    }

    //Extras can only be received by @EActivity classes, so we receive it here and promptly redirect it to the presenter
    @Extra(ChartListPresenter.PURPOSE_KEY)
    internal fun setPurpose(purpose: String) {
        presenter.purpose = purpose
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val prefs = getSharedPreferences("de.namnodorel.chatstat", MODE_PRIVATE)
        val themeString = prefs.getString(Theme.SETTINGS_KEY, Theme.DARK_BLUE.name)!!
        setTheme(getThemeIdFor(Theme.valueOf(themeString)))
    }

    @AfterViews
    fun initViews() {

        itemAdapter = ItemAdapter()

        val fastAdapter = FastAdapter.with(itemAdapter)

        if (presenter.isInBrowsingMode) {
            setTitle(R.string.chart_list_title)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)

            selectionDoneBtn.hide()
            addChartBtn.show()

            fastAdapter.onClickListener = { _, _, _, position ->
                presenter.editChartClicked(position)
                false
            }

            fastAdapter.onLongClickListener = {_, _, _, position ->
                MaterialDialog(this).show {
                    title(R.string.delete_chart_doubt)
                    positiveButton(R.string.confirm_delete, click={
                        presenter.deleteChartClicked(position)
                    })
                    negativeButton(R.string.dismiss_dialog)
                }
                false
            }

        } else if (presenter.isInSelectionMode) {

            setTitle(R.string.ask_for_selection)

            addChartBtn.hide()
            selectionDoneBtn.show()


            selectionExt = fastAdapter.getSelectExtension()
            selectionExt.isSelectable = true
            selectionExt.multiSelect = true
            selectionExt.allowDeselection = true
            selectionExt.selectWithItemUpdate = true
        }

        contentView.layoutManager = LinearLayoutManager(this)
        contentView.adapter = fastAdapter
    }

    private fun getThemeIdFor(theme: Theme): Int{
        return when(theme){
            Theme.DARK_BLUE -> R.style.DarkBlue
            Theme.LIGHT_BLUE -> R.style.LightBlue
        }
    }

    override fun checkAndReturnSaveFileInput() {
        try {
            val inputStream = this.openFileInput(SAVE_FILE_NAME)
            getPresenter().chartFileInputFound(inputStream)
        } catch (e: FileNotFoundException) {
            getPresenter().chartFileMissing()
        }

    }

    override fun returnSaveFileOutput() {
        try {
            val outputStream = this.openFileOutput(SAVE_FILE_NAME, MODE_PRIVATE)
            getPresenter().chartFileOutputFound(outputStream)

            //This doesn't really happen, if a file doesn't exist it will be created when it is written to
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

    }

    override fun addEntry(title: String, description: String, chartType: DisplaySettings, valid: Boolean) {
        itemAdapter.add(ChartListItem(title, description, chartType, valid))
    }

    override fun replaceEntry(id: Int, title: String, description: String, editable: Boolean, chartType: DisplaySettings, valid: Boolean) {
        itemAdapter[id] = ChartListItem(title, description, chartType, valid)
    }

    override fun deleteEntry(id: Int) {
        itemAdapter.remove(id)
    }

    override fun clearEntries() {
        itemAdapter.clear()
    }

    override fun selectEntry(id: Int) {
        selectionExt.select(id, fireEvent = true, considerSelectableFlag = true)
    }

    override fun deselectEntry(id: Int) {
        selectionExt.deselect(id)
    }

    override fun getSelection(): Set<Int> {
        return selectionExt.selections
    }

    override fun clearSelection() {
        selectionExt.deselect()
    }

    @Click(R.id.add_chart)
    fun addChartBtnClicked() {
        getPresenter().addChartBtnClicked()
    }

    @Click(R.id.chart_selection_done)
    fun chartSelectionDoneBtnClicked() {
        getPresenter().chartSelectionDoneBtnClicked()
    }

    override fun goToChartCreator(newChart: ChartSettings<out DisplaySettings>) {
        val intent = Intent(this, ChartCreatorActivity_::class.java)
        intent.putExtra(CHART_ID, -1)
        intent.putExtra(ChartCreatorActivity.CHART_PREFILL, newChart)
        startActivityForResult(intent, CREATE_CHART_REQUEST)
    }

    override fun goToChartEditor(chartID: Int, r: ChartSettings<out DisplaySettings>) {
        val intent = Intent(this, ChartCreatorActivity_::class.java)
        intent.putExtra(CHART_ID, chartID)
        intent.putExtra(ChartCreatorActivity.CHART_PREFILL, r)
        startActivityForResult(intent, EDIT_CHART_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CREATE_CHART_REQUEST) {
            if (resultCode == RESULT_OK) {
                val result = data!!.getSerializableExtra(CHART_RESULT) as ChartSettings<out DisplaySettings>
                getPresenter().chartAdded(result)
            }
        } else if (requestCode == EDIT_CHART_REQUEST) {
            if (resultCode == RESULT_OK) {
                val result = data!!.getSerializableExtra(CHART_RESULT) as ChartSettings<out DisplaySettings>
                val id = data.getIntExtra(CHART_ID, -1)
                getPresenter().chartEdited(id, result)
            }
        }
    }

    override fun showSaveSuccessful() {
        Snackbar.make(findViewById(R.id.chart_list_coordinator), "Charts saved", Snackbar.LENGTH_SHORT).show()
    }

    override fun setResultAndFinish(result: Serializable) {
        val intent = Intent()
        intent.putExtra(SELECTION_RESULT, result)
        setResult(RESULT_OK, intent)
        finish()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}
