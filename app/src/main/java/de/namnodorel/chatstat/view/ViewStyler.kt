/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view

import android.content.Context
import android.view.View
import de.namnodorel.chatstat.model.ColorRepository

import de.namnodorel.chatstat.model.chart.DisplaySettings
import de.namnodorel.chatstat.model.data.ChartSettings

interface ViewStyler<T, F : DisplaySettings> {
    fun makeView(data: T, chartSettings: ChartSettings<out F>, colorRepo: ColorRepository, context: Context): View

}
