/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation.formelements

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.view.implementation.utils.getString
import de.namnodorel.ofv.ValueChangeCallback
import de.namnodorel.ofv.annotations.ObjectFormElement
import java.util.*

@ObjectFormElement("java.util.List<java.lang.String>")
class StringListFormElement {

    fun attachView(prefill: Any?, fieldID: String, changeCallback: ValueChangeCallback, parent: ViewGroup) {

        val context = parent.context

        val vi = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val root = vi.inflate(R.layout.userinput_stringlist, parent) as ViewGroup
        val view = root.getChildAt(root.childCount - 1)

        val stringSpace = view.findViewById<LinearLayout>(R.id.userinput_stringlist_linearlayout)

        val titleTv = view.findViewById<TextView>(R.id.userinput_stringlist_title)
        val emptyTv = view.findViewById<TextView>(R.id.userinput_stringlist_empty)

        val stringList: MutableList<String>
        if (prefill != null) {
            stringList = (prefill as List<String>).toMutableList()
        } else {
            stringList = ArrayList()
        }

        if (stringList.isEmpty()) {
            emptyTv.visibility = VISIBLE
        } else {
            emptyTv.visibility = GONE

            for (s in stringList) {
                addSingleStringTo(stringSpace, s, stringList, changeCallback)
            }
        }

        val addStringBtn = view.findViewById<ImageButton>(R.id.userinput_stringlist_add_string)
        addStringBtn.setOnClickListener {
            stringList.add("")
            addSingleStringTo(stringSpace, "", stringList, changeCallback)
            changeCallback.updateVariable(stringList)
        }

        titleTv.text = context.getString(fieldID)
        emptyTv.text = context.getString(fieldID + "_empty")
    }

    private fun addSingleStringTo(view: LinearLayout, preFill: String, stringList: MutableList<String>, changeCallback: ValueChangeCallback) {

        val emptyTv = view.findViewById<TextView>(R.id.userinput_stringlist_empty)

        val inflater = view.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.userinput_stringlist_item, view)

        val viewIndex = view.childCount - 1

        //We subtract one more than usual because the LL has an additional child that is not a result, the "empty state" TextView
        val listIndex = viewIndex - 1

        val singleStringUI = view.getChildAt(viewIndex)

        val textInput = singleStringUI.findViewById<EditText>(R.id.userinput_stringlist_string_input)
        textInput.setText(preFill)
        textInput.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                stringList[listIndex] = textInput.text.toString()
                changeCallback.updateVariable(stringList)
            }

            override fun afterTextChanged(s: Editable) {

            }
        })

        val deleteBtn = singleStringUI.findViewById<ImageButton>(R.id.userinput_stringlist_delete)
        deleteBtn.setOnClickListener {
            val currentViewIndex = view.indexOfChild(singleStringUI)
            view.removeViewAt(currentViewIndex)

            stringList.removeAt(listIndex)
            changeCallback.updateVariable(stringList)

            if (stringList.isEmpty()) {
                emptyTv.visibility = VISIBLE
            } else {
                emptyTv.visibility = GONE
            }
        }

        emptyTv.visibility = GONE
    }


}
