/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation.chart

import android.content.Context
import android.view.View

import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.ValueFormatter

import java.text.DecimalFormat
import java.util.LinkedList

import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.model.ColorRepository
import de.namnodorel.chatstat.model.chart.pie.PieChartDisplaySettings
import de.namnodorel.chatstat.model.data.ChartSettings
import de.namnodorel.chatstat.view.ViewStyler
import de.namnodorel.chatstat.view.implementation.utils.resolveColor


class PieChartStyler : ViewStyler<PieData, PieChartDisplaySettings> {
    override fun makeView(chartData: PieData, chartSettings: ChartSettings<out PieChartDisplaySettings>, colorRepo: ColorRepository, context: Context): View {

        val dataSet = chartData.dataSet as PieDataSet
        val colors = LinkedList<Int>()
        for (entry in dataSet.values) {
            val label = entry.label
            if (label != null) {
                colors.add(colorRepo.getColor(label))
            } else {
                colors.add(colorRepo.getColor(dataSet.label))
            }
        }
        dataSet.colors = colors

        val settings = chartSettings.displaySettings

        val chart = PieChart(context)
        chart.apply {

            data = chartData
            data.apply {

                setDrawValues(settings.drawValuesEnabled)
                setValueTextColor(context.resolveColor(android.R.attr.textColorPrimary))
                setValueFormatter(object : ValueFormatter() {
                    private val defaultFormat = DecimalFormat("###,###,###.##")
                    override fun getPieLabel(value: Float, pieEntry: PieEntry?): String {
                        return defaultFormat.format(value.toDouble())
                    }
                })
            }

            isDrawHoleEnabled = settings.holeEnabled
            setHoleColor(context.resources.getColor(R.color.transparent))
            setTransparentCircleAlpha(0)

            setCenterTextColor(context.resolveColor(android.R.attr.textColorPrimary))
            setEntryLabelColor(context.resolveColor(android.R.attr.textColorSecondary))

            maxAngle = if (settings.halfPieEnabled) 180f else 360f

            val chartTitle = chartSettings.dataTransformersObservable
                    .reduce(chartSettings.collectorType.name,
                            { title, transformer ->
                                transformer
                                        .updatedYUnit
                                        .replace("{original}", title)
                            })
                    .blockingGet()
            centerText = chartTitle

            setDrawEntryLabels(settings.drawLabelsEnabled)

            isHighlightPerTapEnabled = false
            description.isEnabled = false
            legend.isEnabled = false
        }

        return chart
    }
}
