/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation

import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.model.data.ChartSettings
import de.namnodorel.chatstat.model.persist.ComponentType
import de.namnodorel.chatstat.presenter.ChartCreatorPresenter
import de.namnodorel.chatstat.view.ChartCreatorView
import de.namnodorel.chatstat.view.Theme
import de.namnodorel.chatstat.view.implementation.utils.getString
import de.namnodorel.chatstat.view.implementation.utils.resolveColor
import net.grandcentrix.thirtyinch.TiActivity
import org.androidannotations.annotations.*
import java.io.Serializable

@EActivity(R.layout.activity_chart_creator)
open class ChartCreatorActivity : TiActivity<ChartCreatorPresenter, ChartCreatorView>(), ChartCreatorView {

    companion object {

        const val LIST_REQUEST = 0
        const val SELECTION_REQUEST = 1

        const val CHART_ID = "chart_id"
        const val CHART_RESULT = "chart_result"

        const val CHART_PREFILL = "chart_prefill"
    }

    @ViewById(R.id.chart_title_input)
    internal lateinit var chartTitleET: TextInputEditText

    @ViewById(R.id.chart_title_layout)
    internal lateinit var chartTitleTIL: TextInputLayout

    @ViewById(R.id.chart_description_input)
    internal lateinit var chartDescriptionET: TextInputEditText

    @Extra(CHART_ID)
    @JvmField
    internal var chartID: Int = -1

    @Extra(CHART_PREFILL)
    internal lateinit var preFillChart: ChartSettings<*>

    override fun providePresenter(): ChartCreatorPresenter {
        return ChartCreatorPresenter(chartID, preFillChart)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val prefs = getSharedPreferences("de.namnodorel.chatstat", MODE_PRIVATE)
        val themeString = prefs.getString(Theme.SETTINGS_KEY, Theme.DARK_BLUE.name)!!
        setTheme(getThemeIdFor(Theme.valueOf(themeString)))

        setTitle(R.string.chart_creator_title)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun getThemeIdFor(theme: Theme): Int{
        return when(theme){
            Theme.DARK_BLUE -> R.style.DarkBlue
            Theme.LIGHT_BLUE -> R.style.LightBlue
        }
    }

    override fun setChartTitle(text: String) {
        chartTitleET.setText(text)
    }

    override fun setWarnTitle(warn: Boolean){
        chartTitleTIL.error =
                if(warn){
                    getString(R.string.title_missing)
                }else{
                    ""
                }
    }

    override fun setChartDescription(text: String) {
        chartDescriptionET.setText(text)
    }

    override fun setPreTransformerSub(sub: String) {
        val subTitleTv = findViewById<TextView>(R.id.creator_pretransformers_subtitle)
        subTitleTv.text = sub
    }

    override fun setWarnPreTransformer(warn: Boolean) {
        val preTransformerLayout = findViewById<TextView>(R.id.creator_pretransformer_text)
        applyWarning(preTransformerLayout, warn)
    }

    override fun setDataGroupSelectorSub(sub: String) {
        val subTitleTv = findViewById<TextView>(R.id.creator_datagroupselector_subtitle)
        subTitleTv.text = sub
    }

    override fun setWarnDataGroupSelector(warn: Boolean) {
        val groupSelectorLayout = findViewById<TextView>(R.id.creator_datagroupselector_text)
        applyWarning(groupSelectorLayout, warn)
    }

    override fun setCollectorTypeSub(sub: String) {
        val subTitleTv = findViewById<TextView>(R.id.creator_collector_type_subtitle)
        subTitleTv.text = sub
    }

    override fun setDataCollectorSub(sub: String) {
        val subTitleTv = findViewById<TextView>(R.id.creator_datacollector_subtitle)
        subTitleTv.text = sub
    }

    override fun setWarnDataCollector(warn: Boolean) {
        val dataCollectorLayout = findViewById<TextView>(R.id.creator_datacollector_text)
        applyWarning(dataCollectorLayout, warn)
    }

    override fun setTransformerSub(sub: String) {
        val subTitleTv = findViewById<TextView>(R.id.creator_transformer_subtitle)
        subTitleTv.text = sub
    }

    override fun setWarnTransformer(warn: Boolean) {
        val transformerLayout = findViewById<TextView>(R.id.creator_transformer_text)
        applyWarning(transformerLayout, warn)
    }

    override fun setDisplaySettingsSub(sub: String) {
        val subTitleTv = findViewById<TextView>(R.id.creator_displaysettings_subtitle)
        subTitleTv.text = sub
    }

    override fun setWarnDisplaySettings(warn: Boolean) {
        val displaySettingsLayout = findViewById<TextView>(R.id.creator_displaysettings_text)
        applyWarning(displaySettingsLayout, warn)
    }

    private fun applyWarning(view: TextView, warn: Boolean){
        if(warn){
            view.setTextColor(resolveColor(R.attr.colorError))
        }else{
            view.setTextColor(resolveColor(android.R.attr.textColorPrimary))
        }
    }

    override fun <T : Serializable> goToListComponent(componentType: ComponentType, args: List<T>) {
        val intent = Intent(this, ComponentListEditorActivity_::class.java)
        intent.putExtra(ComponentListEditorActivity.HANDED_TYPE, ComponentListEditorActivity.LIST)
        intent.putExtra(ComponentListEditorActivity.COMPONENT_TYPE, componentType)
        intent.putExtra(ComponentListEditorActivity.LIST_COMPONENT_ARGS, args as Serializable)
        intent.putExtra(ComponentListEditorActivity.EMPTY_STATE_TEXT, getString("none"))

        startActivityForResult(intent, LIST_REQUEST)
        overridePendingTransition(R.transition.left_in, R.transition.left_out)
    }

    override fun <T : Serializable> goToSelectionComponent(type: ComponentType, preFill: T) {
        val intent = Intent(this, SingleSelectionWithSettingsActivity_::class.java)
        intent.putExtra(SingleSelectionWithSettingsActivity.COMPONENT_TYPE, type as Serializable)
        intent.putExtra(SingleSelectionWithSettingsActivity.PREFILL_VALUE, preFill)

        startActivityForResult(intent, SELECTION_REQUEST)
        overridePendingTransition(R.transition.left_in, R.transition.left_out)
    }

    override fun <T : Serializable> goToTypedListComponent(type: Class<out T>, preFill: List<T>) {
        val intent = Intent(this, ComponentListEditorActivity_::class.java)
        intent.putExtra(ComponentListEditorActivity.HANDED_TYPE, ComponentListEditorActivity.PRESELECTED)
        intent.putExtra(ComponentListEditorActivity.TYPE, type)
        intent.putExtra(ComponentListEditorActivity.LIST_COMPONENT_ARGS, preFill as Serializable)
        intent.putExtra(ComponentListEditorActivity.EMPTY_STATE_TEXT, getString("no_collectors_long"))


        startActivityForResult(intent, LIST_REQUEST)
        overridePendingTransition(R.transition.left_in, R.transition.left_out)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == LIST_REQUEST) {
            if (resultCode == RESULT_OK) {
                val result = data!!.getSerializableExtra(ComponentListEditorActivity.LIST_COMPONENT_RESULT) as Collection<Nothing>
                presenter.receivedResultFromListComponent(result)
            } else {
                throw IllegalStateException("Received code $requestCode from ListComponent; this should not be possible!")
            }
        } else if (requestCode == SELECTION_REQUEST) {
            if (resultCode == RESULT_OK) {
                val result = data!!.getSerializableExtra(SingleSelectionWithSettingsActivity.RESULT)
                presenter.receivedResultFromSelection(result)
            } else {
                throw IllegalStateException("Received code $requestCode from Selection; this should not be possible!")
            }
        }
    }

    @Click(R.id.creator_pretransformers)
    fun preTransformersClicked() {
        presenter.preTransformersViewClicked()
    }

    @Click(R.id.creator_groupselector)
    fun groupSelectorClicked() {
        presenter.groupSelectorViewClicked()
    }

    @Click(R.id.creator_collector_type)
    fun collectorTypeClicked(){
        presenter.collectorTypeViewClicked()
    }

    @Click(R.id.creator_datacollectors)
    fun dataCollectorsClicked() {
        presenter.dataCollectorsViewClicked()
    }

    @Click(R.id.creator_transformers)
    fun transformersViewClicked() {
        presenter.transformersViewClicked()
    }

    @Click(R.id.creator_displaysettings)
    fun displaySettingsClicked() {
        presenter.displaySettingsViewClicked()
    }

    @TextChange(R.id.chart_title_input)
    fun titleChanged(editTextView: TextView) {
        presenter.titleChanged(editTextView.text.toString())
    }

    @TextChange(R.id.chart_description_input)
    fun descriptionChanged(editTextView: TextView) {
        presenter.descriptionChanged(editTextView.text.toString())
    }

    override fun onBackPressed() {
        presenter.backPressed()
    }

    override fun onSupportNavigateUp(): Boolean {
        presenter.backPressed()
        return true
    }

    override fun setResultAndFinish(chartID: Int?, result: Serializable) {
        val intent = Intent()
        intent.putExtra(CHART_ID, chartID)
        intent.putExtra(CHART_RESULT, result)
        setResult(RESULT_OK, intent)
        finish()
    }

    override fun getString(id: String): String {
        return baseContext.getString(id)
    }
}
