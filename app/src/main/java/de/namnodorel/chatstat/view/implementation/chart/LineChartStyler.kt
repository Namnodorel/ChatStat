/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation.chart

import android.content.Context
import android.view.View
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import de.namnodorel.chatstat.model.ColorRepository
import de.namnodorel.chatstat.model.chart.line.LineChartDisplaySettings
import de.namnodorel.chatstat.model.data.ChartSettings
import de.namnodorel.chatstat.view.ViewStyler
import de.namnodorel.chatstat.view.implementation.utils.resolveColor
import java.text.DecimalFormat
import kotlin.math.ceil


class LineChartStyler : ViewStyler<LineData, LineChartDisplaySettings> {

    override fun makeView(chartData: LineData, chartSettings: ChartSettings<out LineChartDisplaySettings>, colorRepo: ColorRepository, context: Context): View {

        val settings = chartSettings.displaySettings

        for (ilds in chartData.dataSets) {
            val dataSet = ilds as LineDataSet

            dataSet.color = colorRepo.getColor(dataSet.label)

            dataSet.setCircleColor(dataSet.color)

            if (settings.fillEnabled) {
                dataSet.setDrawFilled(true)
                dataSet.fillColor = dataSet.color
            }
        }


        val chart = LineChart(context)
        chart.apply {

            data = chartData
            data.apply {

                setDrawValues(settings.drawValuesEnabled)
                setValueTextColor(context.resolveColor(android.R.attr.textColorPrimary))
                setValueFormatter(object : ValueFormatter() {
                    private val defaultFormat = DecimalFormat("###,###,###.##")

                    override fun getFormattedValue(value: Float): String {
                        return if (value == 0f) {
                            ""
                        } else defaultFormat.format(value.toDouble())

                    }
                })

            }

            axisLeft.apply {

                axisMinimum = 0f

                val yTitle = chartSettings.dataTransformersObservable
                        .reduce(chartSettings.collectorType.name,
                                { title, transformer ->
                                    transformer
                                            .updatedYUnit
                                            .replace("{original}", title)
                                })
                        .blockingGet()

                title = yTitle

                textColor = context.resolveColor(android.R.attr.textColorPrimary)
                gridColor = context.resolveColor(android.R.attr.textColorSecondary)
                axisLineColor = context.resolveColor(android.R.attr.textColorSecondary)

                setDrawLabels(settings.drawYLabelsEnabled)
            }

            xAxis.apply {

                valueFormatter = object : ValueFormatter() {
                    override fun getFormattedValue(value: Float): String {
                        return if (value != ceil(value.toDouble()).toFloat()) {
                            ""
                        } else chartSettings.dataGroupSelector.getXLabel(value.toInt())

                    }
                }

                position = XAxis.XAxisPosition.BOTTOM
                granularity = 1f
                textColor = context.resolveColor(android.R.attr.textColorPrimary)
                gridColor = context.resolveColor(android.R.attr.textColorSecondary)
                axisLineColor = context.resolveColor(android.R.attr.textColorSecondary)

                setDrawLabels(settings.drawXLabelsEnabled)
            }

            setVisibleXRangeMinimum(0f)
            setVisibleXRangeMaximum(settings.initialZoom.toFloat())

            isDragEnabled = true
            isDragDecelerationEnabled = true
            isScaleYEnabled = false
            isScaleXEnabled = settings.zoomEnabled
            setPinchZoom(settings.zoomEnabled)
            isDoubleTapToZoomEnabled = settings.zoomEnabled

            setBorderColor(context.resolveColor(android.R.attr.textColorPrimary))

            isHighlightPerTapEnabled = false
            isHighlightPerDragEnabled = false
            legend.isEnabled = false
            description.isEnabled = false
            axisRight.isEnabled = false

        }

        return chart
    }
}
