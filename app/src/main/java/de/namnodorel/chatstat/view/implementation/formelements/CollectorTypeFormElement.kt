/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation.formelements

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Spinner
import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.model.collector.CollectorType
import de.namnodorel.chatstat.model.persist.ComponentType
import de.namnodorel.chatstat.view.implementation.utils.getString
import de.namnodorel.ofv.ValueChangeCallback
import de.namnodorel.ofv.annotations.ObjectFormElement
import java.util.*

@ObjectFormElement("de.namnodorel.chatstat.model.collector.CollectorType")
class CollectorTypeFormElement {

    fun attachView(prefill: Any?, fieldID: String, changeCallback: ValueChangeCallback, parent: ViewGroup) {

        val context = parent.context

        val vi = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val root = vi.inflate(R.layout.activity_single_selection, parent) as ViewGroup
        val view = root.getChildAt(root.childCount - 1)

        val classOptions = LinkedList<Class<out CollectorType>>()
        for (option in ComponentType.COLLECTOR_TYPES.subTypes) {
            if (option.simpleName.equals("XPerYCollectorType", ignoreCase = true)) {
                continue // Skip this one to avoid the risk of an infinite loop
            }
            classOptions.add(option as Class<out CollectorType>)
        }

        var selection = 0
        if (prefill != null) {
            selection = classOptions.indexOf(prefill.javaClass as Class<out CollectorType>)
        }

        val spinner = view.findViewById<Spinner>(R.id.chart_selection_spinner)
        val resultComponentSpace = view.findViewById<LinearLayout>(R.id.chart_selection_settings_space)

        val stringOptions = LinkedList<String>()
        for (clazz in classOptions) {
            stringOptions.add(context.getString(clazz.simpleName.toLowerCase() + "_simple_name"))
        }

        val adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, stringOptions)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                resultComponentSpace.removeAllViews()

                try {
                    val obj = classOptions[position].newInstance()
                    changeCallback.updateVariable(obj)

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }


        spinner.setSelection(selection)

    }

}
