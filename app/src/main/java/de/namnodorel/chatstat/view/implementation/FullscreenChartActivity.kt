/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation

import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import com.github.mikephil.charting.data.*
import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.model.ColorRepository
import de.namnodorel.chatstat.model.chart.DisplaySettings
import de.namnodorel.chatstat.model.chart.ErrorData
import de.namnodorel.chatstat.model.chart.bar.BarChartDisplaySettings
import de.namnodorel.chatstat.model.chart.line.LineChartDisplaySettings
import de.namnodorel.chatstat.model.chart.pie.PieChartDisplaySettings
import de.namnodorel.chatstat.model.chart.radar.RadarChartDisplaySettings
import de.namnodorel.chatstat.model.chart.scatter.ScatterChartDisplaySettings
import de.namnodorel.chatstat.model.chart.text.TextDisplaySettings
import de.namnodorel.chatstat.model.data.ChartSettings
import de.namnodorel.chatstat.view.Theme
import de.namnodorel.chatstat.view.implementation.chart.*
import de.namnodorel.chatstat.view.implementation.utils.StupidWorkaround
import net.grandcentrix.thirtyinch.TiActivity
import org.androidannotations.annotations.EActivity
import org.androidannotations.annotations.Extra
import org.androidannotations.annotations.ViewById

@EActivity(R.layout.activity_fullscreen_chart)
open class FullscreenChartActivity : AppCompatActivity() {

    @Extra(CHART_CHART)
    lateinit var chart: ChartSettings<out DisplaySettings>

    lateinit var data: Any

    @Extra(COLOR_REPO)
    lateinit var colorRepository: ColorRepository

    @ViewById(R.id.fullscreen_chart_content_layout)
    lateinit var content: FrameLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        data = StupidWorkaround.staticReferenceToDataObject!!

        val prefs = getSharedPreferences("de.namnodorel.chatstat", TiActivity.MODE_PRIVATE)
        val themeString = prefs.getString(Theme.SETTINGS_KEY, Theme.DARK_BLUE.name)!!
        setTheme(getThemeIdFor(Theme.valueOf(themeString)))

        title = chart.title
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun getThemeIdFor(theme: Theme): Int{
        return when(theme){
            Theme.DARK_BLUE -> R.style.DarkBlue
            Theme.LIGHT_BLUE -> R.style.LightBlue
        }
    }

    override fun onStart() {
        super.onStart()

        content.addView(makeGraphView())
    }

    private fun makeGraphView(): View {

        return when (val data = data) {
            null -> LoadingStyler()
                    .makeView(null, chart, colorRepository, this)

            is ErrorData -> ErrorStyler()
                    .makeView(data, chart, colorRepository, this)

            is BarData -> BarChartStyler()
                    .makeView(data, chart as ChartSettings<out BarChartDisplaySettings>, colorRepository, this)

            is RadarData -> RadarChartStyler()
                    .makeView(data, chart as ChartSettings<out RadarChartDisplaySettings>, colorRepository, this)

            is ScatterData -> ScatterChartStyler()
                    .makeView(data, chart as ChartSettings<out ScatterChartDisplaySettings>, colorRepository, this)

            is PieData -> PieChartStyler()
                    .makeView(data, chart as ChartSettings<out PieChartDisplaySettings>, colorRepository, this)

            is LineData -> LineChartStyler()
                    .makeView(data, chart as ChartSettings<out LineChartDisplaySettings>, colorRepository, this)

            is Map<*, *> -> TextChartStyler()
                    .makeView(data as MutableMap<String, Any>, chart as ChartSettings<out TextDisplaySettings>, colorRepository, this)

            else -> throw IllegalArgumentException("Data type not recognized: ${data.javaClass}")
        }
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    companion object{

        const val CHART_CHART = "chart_chart"
        const val CHART_DATA = "chart_data"
        const val COLOR_REPO = "color_repository"

    }
}