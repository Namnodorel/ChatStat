/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation.formelements

import android.view.ViewGroup
import de.namnodorel.ofv.ValueChangeCallback
import de.namnodorel.ofv.annotations.ObjectFormElement
import java.util.*

@ObjectFormElement("java.util.Set<java.lang.String>")
class TargetFormElement {

    fun attachView(prefill: Any?, fieldID: String, changeCallback: ValueChangeCallback, parent: ViewGroup) {
        val participants = LinkedList<String>()

        if (prefill != null) {
            participants.addAll((prefill as Set<String>))
        }

        StringListFormElement().attachView(participants, fieldID, ValueChangeCallback { updatedValue ->
            val addedParticipants = updatedValue as List<String>
            val target = HashSet(addedParticipants)
            changeCallback.updateVariable(target)
        }, parent)

    }

}
