/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view

import de.namnodorel.chatstat.model.ColorRepository
import de.namnodorel.chatstat.model.chart.DisplaySettings
import net.grandcentrix.thirtyinch.TiView
import net.grandcentrix.thirtyinch.callonmainthread.CallOnMainThread

import de.namnodorel.chatstat.model.data.ChartSettings

interface ChartView : TiView {

    @CallOnMainThread
    fun addChart(chart: ChartSettings<*>, data: Any?, colorRepository: ColorRepository): Int?

    @CallOnMainThread
    fun removeChart(chartId: Int)

    @CallOnMainThread
    fun updateChart(chartId: Int, chart: ChartSettings<*>, data: Any?, colorRepository: ColorRepository)

    fun hasCharts(): Boolean

    @CallOnMainThread
    fun goToLandingPage()

    @CallOnMainThread
    fun promptUserToSelectChart()

    @CallOnMainThread
    fun addLegendEntry(label: String, color: Int)

    @CallOnMainThread
    fun showColorChangeDialog(oldColor: Int)

    @CallOnMainThread
    fun setLegendColorForLabel(label: String, newColor: Int)

    @CallOnMainThread
    fun goToFullscreenChartView(chart: ChartSettings<out DisplaySettings>, data: Any, colorRepository: ColorRepository)

    fun getString(id: String): String
}
