/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation

import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.presenter.FullscreenSettingsDialogPresenter
import de.namnodorel.chatstat.view.FullscreenSettingsDialogView
import de.namnodorel.chatstat.view.Theme
import de.namnodorel.chatstat.view.implementation.utils.getString
import de.namnodorel.ofv.FormViewGenerator
import net.grandcentrix.thirtyinch.TiActivity
import org.androidannotations.annotations.EActivity
import org.androidannotations.annotations.Extra
import org.androidannotations.annotations.ViewById
import java.io.Serializable


@EActivity(R.layout.activity_fullscreen_settings_dialog)
open class FullscreenSettingsDialogActivity: TiActivity<FullscreenSettingsDialogPresenter, FullscreenSettingsDialogView>(), FullscreenSettingsDialogView {

    @Extra(SETTINGS_OBJECT)
    lateinit var settingsObject: Serializable

    @ViewById(R.id.fullscreen_settings_content_view)
    lateinit var contentView: ViewGroup

    override fun providePresenter(): FullscreenSettingsDialogPresenter {
        return FullscreenSettingsDialogPresenter(settingsObject)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val prefs = getSharedPreferences("de.namnodorel.chatstat", MODE_PRIVATE)
        val themeString = prefs.getString(Theme.SETTINGS_KEY, Theme.DARK_BLUE.name)!!
        setTheme(getThemeIdFor(Theme.valueOf(themeString)))

        title = getString(settingsObject.javaClass.simpleName.toLowerCase() + "_simple_name")

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_abort)

    }

    private fun getThemeIdFor(theme: Theme): Int{
        return when(theme){
            Theme.DARK_BLUE -> R.style.DarkBlue
            Theme.LIGHT_BLUE -> R.style.LightBlue
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.dialog, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.done -> presenter.donePressed()
        }
        return false
    }

    override fun clearView() {
        contentView.removeAllViews()
        contentView.invalidate()
    }

    override fun populateView(settingsObject: Any) {
        val gen = FormViewGenerator.find(settingsObject)
        contentView.addView(gen.generateFormView(this))
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun setResultAndFinish(result: Serializable) {
        val intent = Intent()
        intent.putExtra(RESULT, result)
        setResult(RESULT_OK, intent)
        finish()
    }

    companion object{
        const val SETTINGS_OBJECT = "settings_object"
        const val RESULT = "fullscreen_settings_dialog_result"
    }
}