/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.view.implementation

import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItems
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textview.MaterialTextView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.drag.ItemTouchCallback
import com.mikepenz.fastadapter.swipe.SimpleSwipeCallback
import com.mikepenz.fastadapter.swipe_drag.SimpleSwipeDragCallback
import de.namnodorel.chatstat.R
import de.namnodorel.chatstat.model.persist.ComponentType
import de.namnodorel.chatstat.presenter.ComponentListEditorPresenter
import de.namnodorel.chatstat.view.ChartCreatorListComponentView
import de.namnodorel.chatstat.view.Theme
import de.namnodorel.chatstat.view.implementation.utils.getString
import net.grandcentrix.thirtyinch.TiActivity
import org.androidannotations.annotations.Click
import org.androidannotations.annotations.EActivity
import org.androidannotations.annotations.Extra
import org.androidannotations.annotations.ViewById
import java.io.Serializable
import java.util.*


@EActivity(R.layout.activity_component_list_editor)
open class ComponentListEditorActivity
    : TiActivity<ComponentListEditorPresenter, ChartCreatorListComponentView>(),
        ChartCreatorListComponentView, ItemTouchCallback, SimpleSwipeCallback.ItemSwipeCallback {

    @Extra(HANDED_TYPE)
    internal lateinit var handedType: String

    @Extra(COMPONENT_TYPE)
    internal lateinit var componentType: ComponentType

    @Extra(TYPE)
    internal lateinit var chosenType: Class<*>

    //No generics here 'cause AA doesn't support it
    @Extra(LIST_COMPONENT_ARGS)
    internal lateinit var args: ArrayList<*>

    @Extra(EMPTY_STATE_TEXT)
    internal lateinit var emptyText: String

    @ViewById(R.id.chart_creator_list_component_recyclerview)
    internal lateinit var contentView: RecyclerView

    @ViewById(R.id.manage_items_fab)
    internal lateinit var manageButton: FloatingActionButton

    @ViewById(R.id.chart_creator_list_component_empty_view)
    internal lateinit var emptyTv: MaterialTextView

    private lateinit var itemAdapter: ItemAdapter<ComponentListEditorItem>
    private lateinit var fastAdapter: FastAdapter<ComponentListEditorItem>

    override fun providePresenter(): ComponentListEditorPresenter {
        if(handedType == LIST){
            return ComponentListEditorPresenter(componentType, args as MutableList<Any>)
        }else if(handedType == PRESELECTED){
            return ComponentListEditorPresenter(chosenType, args as MutableList<Any>)
        }
        throw IllegalArgumentException("Either COMPONENT_TYPE or TYPE needs to be passed!")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val prefs = getSharedPreferences("de.namnodorel.chatstat", MODE_PRIVATE)
        val themeString = prefs.getString(Theme.SETTINGS_KEY, Theme.DARK_BLUE.name)!!
        setTheme(getThemeIdFor(Theme.valueOf(themeString)))
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        if(handedType == LIST){
            title = getString(componentType.name.toLowerCase())
        }else if(handedType == PRESELECTED){
            title = getString(chosenType.simpleName.toLowerCase() + "_simple_name")
        }
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val touchCallback = SimpleSwipeDragCallback(
                this,
                this,
                getDrawable(R.drawable.ic_delete)!!
        )

        val touchHelper = ItemTouchHelper(touchCallback)
        touchHelper.attachToRecyclerView(contentView)

        contentView.layoutManager = LinearLayoutManager(this)
    }

    private fun getThemeIdFor(theme: Theme): Int{
        return when(theme){
            Theme.DARK_BLUE -> R.style.DarkBlue
            Theme.LIGHT_BLUE -> R.style.LightBlue
        }
    }

    override fun onStart() {
        itemAdapter = ItemAdapter()

        fastAdapter = FastAdapter.with(itemAdapter)
        fastAdapter.onClickListener = {_, _, _, position ->
            presenter.editItemBtnClicked(position)
            false
        }

        contentView.adapter = fastAdapter

        updateEmtpyState()

        super.onStart()
    }

    override fun addItem(title: String, subTitle: String, valid: Boolean) {

        val item = ComponentListEditorItem(
                title,
                subTitle,
                valid
        )

        itemAdapter.add(item)

        updateEmtpyState()
    }

    override fun removeItem(index: Int) {
        itemAdapter.remove(index)
        fastAdapter.notifyAdapterDataSetChanged()

        updateEmtpyState()
    }

    override fun clearItems() {
        itemAdapter.clear()
        fastAdapter.notifyAdapterDataSetChanged()

        updateEmtpyState()
    }

    override fun swapItems(oldPosition: Int, newPosition: Int) {
        Collections.swap(itemAdapter.adapterItems, oldPosition, newPosition)
        fastAdapter.notifyAdapterItemMoved(oldPosition, newPosition)
    }

    override fun itemTouchOnMove(oldPosition: Int, newPosition: Int): Boolean {
        presenter.itemMoved(oldPosition, newPosition)
        return true
    }

    override fun itemTouchDropped(oldPosition: Int, newPosition: Int) {

    }

    override fun itemSwiped(position: Int, direction: Int) {
        presenter.deletePressed(position)
    }

    @Click(R.id.manage_items_fab)
    fun manageItemsBtnClicked() {
        presenter.addItemBtnClicked()
    }

    override fun chooseFromDialog(options: List<String>) {
        MaterialDialog(this).show{
            listItems(items = options){_, position, _ ->
                presenter.dialogChosen(position)
            }

        }
    }

    override fun goToDetailsView(preFill: Serializable) {

        val intent = Intent(this, FullscreenSettingsDialogActivity_::class.java)
        intent.putExtra(FullscreenSettingsDialogActivity.SETTINGS_OBJECT, preFill)

        startActivityForResult(intent, 0)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            RESULT_OK -> {
                val result = data!!.getSerializableExtra(FullscreenSettingsDialogActivity.RESULT)
                presenter.receivedResultFromDetails(result)
            }
            RESULT_CANCELED -> {
                //This is fine, don't save anything
            }
            else -> throw IllegalStateException("Received code $requestCode from detail view; this should not be possible!")
        }
    }

    private fun updateEmtpyState(){
        if(itemAdapter.adapterItemCount == 0){
            emptyTv.text = emptyText
            emptyTv.visibility = View.VISIBLE
            contentView.visibility = View.GONE
        }else{
            emptyTv.visibility = View.GONE
            contentView.visibility = View.VISIBLE
        }
    }

    //Since this is not a regular start for result but just a submenu in reality, pressing back should actually deliver the result
    override fun onBackPressed() {
        presenter.backPressed()
    }

    override fun onSupportNavigateUp(): Boolean {
        presenter.backPressed()
        return true
    }

    override fun setResultAndFinish(result: Serializable) {
        val intent = Intent()
        intent.putExtra(LIST_COMPONENT_RESULT, result)
        setResult(RESULT_OK, intent)
        finish()
        overridePendingTransition(R.transition.right_in, R.transition.right_out)
    }

    override fun getString(id: String): String {
        return baseContext.getString(id)
    }

    companion object {
        const val LIST_COMPONENT_RESULT = "list_result"
        const val HANDED_TYPE = "handed_type"
        const val LIST = "selection"
        const val PRESELECTED = "preselected"
        const val TYPE = "chosen_type"
        const val COMPONENT_TYPE = "component_type"
        const val LIST_COMPONENT_ARGS = "list_args"
        const val EMPTY_STATE_TEXT = "empty_state_text"
    }
}
