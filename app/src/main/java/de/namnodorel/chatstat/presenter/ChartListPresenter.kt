/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.presenter

import net.grandcentrix.thirtyinch.TiPresenter

import org.androidannotations.annotations.Background
import org.androidannotations.annotations.Bean
import org.androidannotations.annotations.EBean

import java.io.InputStream
import java.io.OutputStream
import java.io.Serializable
import java.util.HashSet

import de.namnodorel.chatstat.model.chart.DisplaySettings
import de.namnodorel.chatstat.model.chart.bar.BarChartDisplaySettings
import de.namnodorel.chatstat.model.collector.MessageCollectorType
import de.namnodorel.chatstat.model.data.ChartSettings
import de.namnodorel.chatstat.model.groupselector.StandardTimeIntervalGroupSelector
import de.namnodorel.chatstat.model.persist.ChartFileAccess
import de.namnodorel.chatstat.model.persist.defaults.DefaultCharts
import de.namnodorel.chatstat.view.ChartListView
import net.grandcentrix.thirtyinch.kotlin.deliverToView

@EBean
open class ChartListPresenter : TiPresenter<ChartListView>() {

    companion object {
        const val PURPOSE_KEY = "purpose"
        const val PURPOSE_BROWSE = "purpose_browse"
        const val PURPOSE_SELECT = "purpose_select"
    }

    lateinit var purpose: String

    @Bean
    lateinit var rfa: ChartFileAccess

    private var charts: MutableList<ChartSettings<out DisplaySettings>>? = null
    private lateinit var selection: Set<Int>

    val isInSelectionMode: Boolean
        get() = purpose == PURPOSE_SELECT

    val isInBrowsingMode: Boolean
        get() = purpose == PURPOSE_BROWSE

    override fun onCreate() {
        super.onCreate()

        selection = HashSet()

        deliverToView { checkAndReturnSaveFileInput() }
    }

    override fun onAttachView(view: ChartListView) {
        super.onAttachView(view)
        if (charts != null) {
            view.clearEntries()

            addEntriesToView(charts)

            if (isInSelectionMode) {
                view.clearSelection()

                for (id in selection) {
                    view.selectEntry(id)
                }
            }
        }
    }

    @Background
    open fun chartFileInputFound(inputStream: InputStream) {
        try {
            charts = rfa.readChartFile(inputStream).toMutableList()
            addEntriesToView(charts)
        } catch (e: Exception) {
            handleIOError(e)
        }

    }

    @Background
    open fun chartFileOutputFound(outputStream: OutputStream) {
        try {
            rfa.writeChartFile(charts!!.toList(), outputStream)

            view?.showSaveSuccessful()
        } catch (e: Exception) {
            handleIOError(e)
        }

    }

    private fun addEntriesToView(charts: List<ChartSettings<out DisplaySettings>>?) {
        deliverToView {

            for (rec in charts!!) {
                val valid = rec.valid()
                if (isInBrowsingMode || valid) {
                    view!!.addEntry(rec.title, rec.description, rec.displaySettings, valid)
                }
            }
        }
    }

    fun chartFileMissing() {
        //Generate default config and show its entries already, even if it's not saved yet
        charts = DefaultCharts.getDefaultCharts()
        addEntriesToView(charts)

        view!!.returnSaveFileOutput()
    }

    private fun handleIOError(ex: Exception) {
        ex.printStackTrace()
    }

    fun addChartBtnClicked() {
        val newChart = ChartSettings(
                title = "New Chart",
                description = "",
                preTransformers = ArrayList(),
                dataGroupSelector = StandardTimeIntervalGroupSelector(),
                collectorType = MessageCollectorType(),
                dataCollectors = ArrayList(),
                dataTransformers = ArrayList(),
                displaySettings = BarChartDisplaySettings()
        )
        view!!.goToChartCreator(newChart)
    }

    fun chartAdded(chart: ChartSettings<out DisplaySettings>) {
        charts!!.add(chart)

        deliverToView {
            view!!.returnSaveFileOutput()
        }
        //We don't add an entry to the View because attachView() does it for us
    }

    fun editChartClicked(chartID: Int) {
        view!!.goToChartEditor(chartID, charts!![chartID])
    }

    fun chartEdited(index: Int?, result: ChartSettings<out DisplaySettings>) {
        charts!![index!!] = result

        deliverToView {
            view!!.returnSaveFileOutput()
            replaceEntry(index, result.title, result.description, isInBrowsingMode, result.displaySettings, result.valid())
        }
    }

    fun deleteChartClicked(id: Int?) {
        charts!!.removeAt(id as Int)
        view!!.deleteEntry(id)

        view!!.returnSaveFileOutput()
    }

    fun chartSelectionDoneBtnClicked() {
        val selectionIDs = view!!.getSelection()

        val selectedCharts = HashSet<ChartSettings<out DisplaySettings>>()
        for (id in selectionIDs) {
            selectedCharts.add(charts!![id])
        }

        view!!.setResultAndFinish(selectedCharts as Serializable)
    }

    override fun onDetachView() {

        if (isInSelectionMode) {
            selection = view!!.getSelection()
        } else {
            view!!.returnSaveFileOutput()
        }

        super.onDetachView()
    }
}
