/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.presenter

import android.util.Pair
import de.namnodorel.chatstat.model.Analysis
import de.namnodorel.chatstat.model.chart.DataToChartConverter
import de.namnodorel.chatstat.model.chart.DisplaySettings
import de.namnodorel.chatstat.model.chart.bar.BarChartDisplaySettings
import de.namnodorel.chatstat.model.chart.bar.DataToBarChartConverter
import de.namnodorel.chatstat.model.chart.line.DataToLineChartConverter
import de.namnodorel.chatstat.model.chart.line.LineChartDisplaySettings
import de.namnodorel.chatstat.model.chart.pie.DataToPieChartConverter
import de.namnodorel.chatstat.model.chart.pie.PieChartDisplaySettings
import de.namnodorel.chatstat.model.chart.radar.DataToRadarChartConverter
import de.namnodorel.chatstat.model.chart.radar.RadarChartDisplaySettings
import de.namnodorel.chatstat.model.chart.scatter.DataToScatterChartConverter
import de.namnodorel.chatstat.model.chart.scatter.ScatterChartDisplaySettings
import de.namnodorel.chatstat.model.chart.text.DataToTextConverter
import de.namnodorel.chatstat.model.chart.text.TextDisplaySettings
import de.namnodorel.chatstat.model.data.ChartSettings
import de.namnodorel.chatstat.model.input.DefaultParser
import de.namnodorel.chatstat.model.input.SourceModel
import de.namnodorel.chatstat.model.input.TextFileReader
import de.namnodorel.chatstat.model.input.ZippedTextFileReader
import de.namnodorel.chatstat.view.ChartView
import de.namnodorel.chatstat.model.ColorRepository
import de.namnodorel.chatstat.model.chart.ErrorData
import io.reactivex.rxjava3.schedulers.Schedulers
import net.grandcentrix.thirtyinch.TiPresenter
import net.grandcentrix.thirtyinch.kotlin.deliverToView
import org.androidannotations.annotations.Background
import org.androidannotations.annotations.EBean
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*

@EBean
open class ChartPresenter : TiPresenter<ChartView>(), ColorRepository.ColorCreationListener {

    private var contentType: String? = null
    private var source: InputStream? = null

    private var sourceModel: SourceModel? = null

    private lateinit var locale: Locale
    private lateinit var userCustomTimeFormat: SimpleDateFormat

    private var indexedChartDataPair: MutableMap<Int, Pair<ChartSettings<out DisplaySettings>, Any>>? = null

    private lateinit var colorRepository: ColorRepository

    override fun onCreate() {
        super.onCreate()

        colorRepository = ColorRepository(this)

        if (source != null) {
            processSource(source!!)
        } else {
            deliverToView { goToLandingPage() }
        }
    }

    override fun onAttachView(view: ChartView) {
        super.onAttachView(view)

        if (indexedChartDataPair != null && !view.hasCharts()) {
            //Re-add all charts to the layout, and reassign ids if necessary
            val tmp = HashMap<Int, Pair<ChartSettings<out DisplaySettings>, Any>>()

            for (id in indexedChartDataPair!!.keys) {
                val newId = view.addChart(indexedChartDataPair!![id]!!.first, indexedChartDataPair!![id]!!.second, colorRepository)
                tmp[newId!!] = Pair(indexedChartDataPair!![id]!!.first, indexedChartDataPair!![id]!!.second)
            }

            indexedChartDataPair = tmp
        }
    }

    fun setSource(contentType: String, source: InputStream) {
        this.contentType = contentType
        this.source = source
    }

    //Not the cleanest way of doing things, but these arguments are required for correct parsing
    fun setAdditionalArgs(locale: Locale, userCustomTimeFormat: SimpleDateFormat) {
        this.locale = locale
        this.userCustomTimeFormat = userCustomTimeFormat
    }

    @Background
    open fun processSource(source: InputStream) {
        when (contentType) {
            "txt" -> sourceModel = TextFileReader(source)
            "zip" -> sourceModel = ZippedTextFileReader(source)
            else -> throw IllegalArgumentException("Only TXT and ZIP files are supported at the moment!")
        }

        deliverToView { promptUserToSelectChart() }
    }

    fun receivedChartSelection(result: Set<ChartSettings<out DisplaySettings>>) {

        indexedChartDataPair = HashMap()

        //Put loading icons in the UI and save their ID, so the charts can be
        //displayed in the correct place when the analysis is finished.
        for (rec in result) {
            deliverToView {
                val index = addChart(rec, null, colorRepository)
                indexedChartDataPair!![index!!] = Pair<ChartSettings<out DisplaySettings>, Any>(rec, null)
            }
        }

        var text = sourceModel!!.readSource()
        text = text.observeOn(Schedulers.computation())

        val history = DefaultParser(locale, userCustomTimeFormat).parse(text)

        for (chart in result) {

            val analysis = Analysis(chart)
            val converter: DataToChartConverter<*> = when {
                chart.displaySettings is BarChartDisplaySettings -> DataToBarChartConverter()
                chart.displaySettings is RadarChartDisplaySettings -> DataToRadarChartConverter()
                chart.displaySettings is ScatterChartDisplaySettings -> DataToScatterChartConverter()
                chart.displaySettings is PieChartDisplaySettings -> DataToPieChartConverter()
                chart.displaySettings is LineChartDisplaySettings -> DataToLineChartConverter()
                chart.displaySettings is TextDisplaySettings -> DataToTextConverter()
                else -> throw IllegalArgumentException("DisplaySettings type ${chart.displaySettings.javaClass.name} not recognized!")
            }

            converter.dataToChart(analysis.analyze(history))
                    //Success
                    .subscribe({ data ->
                        deliverToView {
                            for ((key, pair) in indexedChartDataPair!!) {
                                if (pair.first == chart) {
                                    updateChart(key, chart, data, colorRepository)
                                    indexedChartDataPair!![key] = Pair<ChartSettings<out DisplaySettings>, Any>(chart, data)

                                    return@deliverToView
                                }
                            }

                            throw IllegalStateException("Can't find the index for chart!")
                        }
                    //Error
                    }, { throwable ->
                        (throwable as Throwable).printStackTrace()
                        deliverToView {
                            for ((key, pair) in indexedChartDataPair!!) {
                                if (pair.first == chart) {

                                    val errorData = ErrorData(throwable)

                                    //Explain common error caused by misconfiguration
                                    if(throwable.message!!.contains("Sequence contains more than one element!") && view != null){
                                        errorData.explanation = view!!.getString("sequence_too_many_elements_explanation")
                                    }

                                    updateChart(key, chart, errorData, colorRepository)
                                    indexedChartDataPair!![key] = Pair<ChartSettings<out DisplaySettings>, Any>(chart, errorData)

                                    return@deliverToView
                                }
                            }

                            throw IllegalStateException("Can't find the index for chart!")
                        }
                    })
        }
    }

    private fun refreshCharts(){
        //TODO Update just the color without resetting the chart itself
        for((key, pair) in indexedChartDataPair!!){
            view!!.updateChart(key, pair.first, pair.second, colorRepository)
        }
    }

    override fun onNewColorCreated(label: String, color: Int) {

        view!!.addLegendEntry(label, color)

    }

    private var labelToBeChanged: String? = null

    fun colorChangeClicked(ofLabel: String){
        labelToBeChanged = ofLabel
        view!!.showColorChangeDialog(colorRepository.getColor(ofLabel))
    }

    fun colorChanged(newColor: Int) {
        colorRepository.setColorForLabel(labelToBeChanged!!, newColor)
        view!!.setLegendColorForLabel(labelToBeChanged!!, newColor)

        labelToBeChanged = null

        refreshCharts()
    }

    fun colorUnchanged(){
        labelToBeChanged = null
    }

    fun largeGraphRequested(chart: ChartSettings<out DisplaySettings>, data: Any?) {
        view!!.goToFullscreenChartView(chart, data!!, ColorRepository(colorRepository))
    }
}
