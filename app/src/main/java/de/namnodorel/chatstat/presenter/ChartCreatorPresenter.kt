/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.presenter

import de.namnodorel.chatstat.model.chart.DisplaySettings
import de.namnodorel.chatstat.model.collector.CollectorType
import de.namnodorel.chatstat.model.collector.DataCollector
import de.namnodorel.chatstat.model.collector.SimpleCollector
import de.namnodorel.chatstat.model.data.ChartSettings
import de.namnodorel.chatstat.model.groupselector.DataGroupSelector
import de.namnodorel.chatstat.model.persist.ComponentType
import de.namnodorel.chatstat.model.pretransformer.PreTransformer
import de.namnodorel.chatstat.model.transformer.Transformer
import de.namnodorel.chatstat.model.validation.Validatable
import de.namnodorel.chatstat.view.ChartCreatorView
import net.grandcentrix.thirtyinch.TiPresenter


class ChartCreatorPresenter(private val chartID: Int, preFillChart: ChartSettings<out DisplaySettings>) : TiPresenter<ChartCreatorView>() {

    private var title: String
    private var description: String
    private var preTransformers: MutableList<PreTransformer>
    private var dataGroupSelector: DataGroupSelector
    private var collectorType: CollectorType
    private var dataCollectors: MutableList<DataCollector>
    private var transformers: MutableList<Transformer>
    private var displaySettings: DisplaySettings

    //The reference to this is updated depending on where the result from the list component needs to go
    private var listPointer: MutableList<*>? = null

    init {
        title = preFillChart.title
        description = preFillChart.description
        preTransformers = preFillChart.preTransformers.toMutableList()
        dataGroupSelector = preFillChart.dataGroupSelector
        collectorType = preFillChart.collectorType
        dataCollectors = preFillChart.dataCollectors.toMutableList()
        transformers = preFillChart.dataTransformers.toMutableList()
        displaySettings = preFillChart.displaySettings
    }

    override fun onAttachView(view: ChartCreatorView) {
        super.onAttachView(view)

        view.setChartTitle(title)
        view.setWarnTitle(title.isEmpty())
        view.setChartDescription(description)

        view.setCollectorTypeSub(makeSubTitleFromClasses(collectorType.javaClass))

        view.setDataGroupSelectorSub(makeSubTitleFromClasses(dataGroupSelector.javaClass))
        view.setWarnDataGroupSelector(!dataGroupSelector.valid())

        val dataCollectorsLabels = Array(dataCollectors.size) { dataCollectors[it].label }
        val dataCollectorSub =
                if(dataCollectorsLabels.isNotEmpty()){
                    dataCollectorsLabels
                            .map {
                                if(it.isEmpty()){
                                    return@map "<no label>"
                                }else{
                                    return@map it
                                }
                            }
                            .reduce { soFar, next ->
                                return@reduce "$soFar, $next"
                            }
                }else{
                    view.getString("no_collectors")
                }
        view.setDataCollectorSub(dataCollectorSub)
        view.setWarnDataCollector((dataCollectors.isEmpty() && !SimpleCollector::class.java.isAssignableFrom(collectorType.collector)) || !Validatable.iterableValid(dataCollectors))

        val preTransformersClasses = Array<Class<*>>(preTransformers.size) { preTransformers[it].javaClass }
        view.setPreTransformerSub(makeSubTitleFromClasses(*preTransformersClasses))
        view.setWarnPreTransformer(!Validatable.iterableValid(preTransformers))

        val transformersClasses = Array<Class<*>>(transformers.size) { transformers[it].javaClass }
        view.setTransformerSub(makeSubTitleFromClasses(*transformersClasses))
        view.setWarnTransformer(!Validatable.iterableValid(transformers))

        view.setDisplaySettingsSub(makeSubTitleFromClasses(displaySettings.javaClass))
        view.setWarnDisplaySettings(false)
    }

    private fun makeSubTitleFromClasses(vararg c: Class<*>): String {
        val subtitle = StringBuilder()

        for (clazz in c) {
            if (subtitle.isNotEmpty()) {
                subtitle.append(", ")
            }
            subtitle.append(view!!.getString(clazz.simpleName.toLowerCase() + "_simple_name"))
        }

        if (subtitle.isEmpty()) {
            subtitle.append(view!!.getString("none"))
        }
        return subtitle.toString()
    }

    fun groupSelectorViewClicked() {
        view!!.goToSelectionComponent(ComponentType.GROUP_SELECTOR, dataGroupSelector)
    }

    fun displaySettingsViewClicked() {
        view!!.goToSelectionComponent(ComponentType.CHART_TYPE, displaySettings)
    }

    fun preTransformersViewClicked() {
        view!!.goToListComponent(ComponentType.PRE_TRANSFORMER, preTransformers)
        listPointer = preTransformers
    }

    fun collectorTypeViewClicked() {
        view!!.goToSelectionComponent(ComponentType.COLLECTOR_TYPES, collectorType)
    }

    fun dataCollectorsViewClicked() {
        view!!.goToTypedListComponent(collectorType.collector, dataCollectors)
        listPointer = dataCollectors
    }

    fun transformersViewClicked() {
        view!!.goToListComponent(ComponentType.TRANSFORMER, transformers)
        listPointer = transformers
    }

    fun titleChanged(newTitle: String) {
        title = newTitle
    }

    fun descriptionChanged(newDescription: String) {
        description = newDescription
    }

    fun receivedResultFromListComponent(result: Collection<Nothing>) {
        listPointer!!.clear()
        listPointer!!.addAll(result)

        listPointer = null
    }

    fun receivedResultFromSelection(result: Any) {
        when (result) {
            is DataGroupSelector -> dataGroupSelector = result
            is CollectorType -> {
                if(collectorType != result){
                    dataCollectors = ArrayList()
                }
                collectorType = result
            }
            is DisplaySettings -> displaySettings = result
            else -> throw IllegalStateException("Tf is going on, got a result from single selection with class " + result.javaClass)
        }
    }

    fun backPressed() {
        val chart = ChartSettings(
                title,
                description,
                preTransformers,
                dataGroupSelector,
                collectorType,
                dataCollectors,
                transformers,
                displaySettings
        )

        view!!.setResultAndFinish(chartID, chart)
    }
}
