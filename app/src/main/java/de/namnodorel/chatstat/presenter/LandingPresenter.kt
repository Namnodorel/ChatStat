/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.presenter

import net.grandcentrix.thirtyinch.TiPresenter

import de.namnodorel.chatstat.view.LandingView
import de.namnodorel.chatstat.view.Theme


class LandingPresenter(var theme: Theme) : TiPresenter<LandingView>() {

    fun importBtnClicked() {
        view!!.openImportDialog()
    }

    fun confirmationBtnClicked() {
        view!!.closeImportDialog()
    }

    fun chartListBtnClicked() {
        view!!.goToChartManagement()
    }

    fun changeThemeClicked() {
        theme = Theme.next(theme)
        view!!.changeThemeTo(theme)
    }

    fun aboutClicked() {
        view!!.goToAboutScreen()
    }

}
