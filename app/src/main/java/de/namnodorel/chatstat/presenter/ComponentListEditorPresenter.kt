/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.presenter

import de.namnodorel.chatstat.model.collector.DataCollector
import net.grandcentrix.thirtyinch.TiPresenter

import java.io.Serializable
import java.util.Collections

import de.namnodorel.chatstat.model.persist.ComponentType
import de.namnodorel.chatstat.model.validation.Validatable
import de.namnodorel.chatstat.view.ChartCreatorListComponentView
import de.namnodorel.ofv.FormViewGenerator

class ComponentListEditorPresenter : TiPresenter<ChartCreatorListComponentView> {

    private val activeType: ComponentType?
    private val chosenType: Class<*>?

    private val result: MutableList<Any>

    private var editPosition = -1

    constructor(activeType: ComponentType?, result: MutableList<Any>) : super() {
        this.activeType = activeType
        this.result = result
        chosenType = null
    }

    constructor(chosenType: Class<*>?, result: MutableList<Any>) : super() {
        this.chosenType = chosenType
        this.result = result
        activeType = null
    }

    override fun onAttachView(view: ChartCreatorListComponentView) {
        super.onAttachView(view)

        view.clearItems()

        for (obj in result) {

            addItemToView(obj)
        }
    }

    private fun addItemToView(obj: Any) {
        val type = obj.javaClass

        if(obj is DataCollector){
            var title = obj.label
            if(title.isEmpty()){
                title = "<no label>"
            }
            view!!.addItem(title, view!!.getString(type.simpleName.toLowerCase() + "_simple_name"), obj.valid())
        }else{
            view!!.addItem(view!!.getString(type.simpleName.toLowerCase() + "_simple_name"), valid=(obj as Validatable).valid())
        }
    }

    fun editItemBtnClicked(index: Int){
        //If there are no attributes to be edited, don't edit anything
        if(FormViewGenerator.find(result[index]).countAttributes() == 0){
            return
        }

        view!!.goToDetailsView(result[index] as Serializable)
        editPosition = index
    }

    fun addItemBtnClicked() {
        if(activeType != null){
            val options = activeType.subTypes.map {
                view!!.getString(it.simpleName.toLowerCase() + "_simple_name")
            }

            view!!.chooseFromDialog(options)
        }else{

            val newItem = chosenType!!.newInstance()

            //If there are no attributes to be edited, add the new item directly
            if(FormViewGenerator.find(newItem).countAttributes() == 0){
                result.add(newItem)
                addItemToView(newItem)
                return
            }

            view!!.goToDetailsView(newItem as Serializable)
        }
    }

    fun dialogChosen(position: Int) {
        val newItem = activeType!!.subTypes[position].newInstance()

        //If there are no attributes to be edited, add the new item directly
        if(FormViewGenerator.find(newItem).countAttributes() == 0){
            result.add(newItem)
            addItemToView(newItem)
            return
        }

        view!!.goToDetailsView(newItem as Serializable)
    }

    fun itemMoved(oldIndex: Int, newIndex: Int) {
        Collections.swap(result, oldIndex, newIndex)
        view!!.swapItems(oldIndex, newIndex)
    }

    fun deletePressed(onItemIndex: Int) {
        result.removeAt(onItemIndex)
        view!!.removeItem(onItemIndex)
    }

    fun backPressed() {
        view!!.setResultAndFinish(result as Serializable)
    }

    fun receivedResultFromDetails(detailResult: Any) {
        if(editPosition != -1){
            result[editPosition] = detailResult
            editPosition = -1
        }else{
            result.add(detailResult)
        }
    }
}
