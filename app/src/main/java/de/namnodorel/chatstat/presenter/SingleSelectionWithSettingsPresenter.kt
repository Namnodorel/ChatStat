/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.presenter

import de.namnodorel.chatstat.model.persist.ComponentType
import net.grandcentrix.thirtyinch.TiPresenter

import java.io.Serializable
import java.util.LinkedList

import de.namnodorel.chatstat.view.SingleSelectionWithSettingsView

class SingleSelectionWithSettingsPresenter(private val type: ComponentType, preFill: Any?) : TiPresenter<SingleSelectionWithSettingsView>() {

    private var result: Any

    init {

        if (preFill == null) {
            val defaultOption = 0

            result = type.subTypes[defaultOption].newInstance()

        }else{
            result = preFill
        }
    }

    override fun onAttachView(view: SingleSelectionWithSettingsView) {
        super.onAttachView(view)

        var selectedOption = type.subTypes.indexOf(result.javaClass)

        val stringOptions = LinkedList<String>()

        for (c in type.subTypes) {
            stringOptions.add(c.simpleName.toLowerCase())

            if (c.isAssignableFrom(result.javaClass)) {
                selectedOption = type.subTypes.indexOf(c)
            }
        }

        view.setSelectableOptions(stringOptions)

        view.selectOption(selectedOption)
    }

    fun optionSelected(index: Int) {

        try {
            if (!type.subTypes[index].isAssignableFrom(result.javaClass)) {
                result = type.subTypes[index].newInstance()
            }

            view!!.clearSettings()

            view!!.generateSettingsFor(result)

        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InstantiationException) {
            e.printStackTrace()
        }

    }

    fun backPressed() {
        view!!.setResultAndFinish(result as Serializable)
    }

}
