/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.input

import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream

class ZippedTextFileReader(private val inputStream: InputStream) : SourceModel {


    override fun readSource(): Single<String> {
        return Single.create<String> { emitter ->

            val zipInputStream = ZipInputStream(inputStream)

            var text: ByteArrayOutputStream? = null

            var ze: ZipEntry?

            var count: Int

            ze = zipInputStream.nextEntry
            while (ze != null) {
                if (ze.name.contains(".txt")) {
                    text = ByteArrayOutputStream()

                    count = zipInputStream.read()
                    while (count != -1) {
                        text.write(count)

                        count = zipInputStream.read()
                    }
                }
                zipInputStream.closeEntry()
                ze = zipInputStream.nextEntry
            }
            zipInputStream.close()

            if (text != null) {
                emitter.onSuccess(text.toString())
            } else {
                throw IllegalArgumentException("Zip file did NOT contain txt!")
            }

        }.subscribeOn(Schedulers.io())
    }
}
