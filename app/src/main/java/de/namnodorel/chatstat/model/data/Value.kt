/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.data

data class Value(var valueData: Float,
                 /**
                  * Specifies the amount of data points (messages) that were
                  * in the group this value was part of. Required for possible calculation of averages.
                  */
                 var amount: Int,
                 var label: String? = null) : Comparable<Value> {

    fun hasLabel(): Boolean {
        return !label.isNullOrEmpty()
    }

    //Compare by label, then by amount, then by valueData
    override fun compareTo(other: Value): Int {
        if(hasLabel() && other.hasLabel()){
            return label!!.compareTo(other.label!!)

        }else if(hasLabel()){
            return 1

        }else if(other.hasLabel()){
            return -1

        }else{

            if(amount > other.amount){
                return 1

            }else if(amount < other.amount){
                return -1

            }else{

                if(valueData > other.valueData){
                    return 1

                }else if(valueData < other.valueData){

                    return -1
                }else{

                    return 0
                }
            }
        }
    }
}
