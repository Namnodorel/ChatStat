/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.chart.radar;

import de.namnodorel.chatstat.model.chart.DisplaySettings;
import de.namnodorel.ofv.annotations.GenerateObjectForm;

@GenerateObjectForm
public class RadarChartDisplaySettings extends DisplaySettings {

    private Boolean drawXLabelsEnabled = true;
    private Boolean drawYLabelsEnabled = false;
    private Boolean drawValuesEnabled = true;

    public RadarChartDisplaySettings() {
    }

    public Boolean getDrawXLabelsEnabled() {
        return this.drawXLabelsEnabled;
    }

    public Boolean getDrawYLabelsEnabled() {
        return this.drawYLabelsEnabled;
    }

    public Boolean getDrawValuesEnabled() {
        return this.drawValuesEnabled;
    }

    public void setDrawXLabelsEnabled(Boolean drawXLabelsEnabled) {
        this.drawXLabelsEnabled = drawXLabelsEnabled;
    }

    public void setDrawYLabelsEnabled(Boolean drawYLabelsEnabled) {
        this.drawYLabelsEnabled = drawYLabelsEnabled;
    }

    public void setDrawValuesEnabled(Boolean drawValuesEnabled) {
        this.drawValuesEnabled = drawValuesEnabled;
    }

    public String toString() {
        return "RadarChartDisplaySettings(drawXLabelsEnabled=" + this.getDrawXLabelsEnabled() + ", drawYLabelsEnabled=" + this.getDrawYLabelsEnabled() + ", drawValuesEnabled=" + this.getDrawValuesEnabled() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof RadarChartDisplaySettings))
            return false;
        final RadarChartDisplaySettings other = (RadarChartDisplaySettings) o;
        if (!other.canEqual((Object) this)) return false;
        if (!super.equals(o)) return false;
        final Object this$drawXLabelsEnabled = this.getDrawXLabelsEnabled();
        final Object other$drawXLabelsEnabled = other.getDrawXLabelsEnabled();
        if (this$drawXLabelsEnabled == null ? other$drawXLabelsEnabled != null : !this$drawXLabelsEnabled.equals(other$drawXLabelsEnabled))
            return false;
        final Object this$drawYLabelsEnabled = this.getDrawYLabelsEnabled();
        final Object other$drawYLabelsEnabled = other.getDrawYLabelsEnabled();
        if (this$drawYLabelsEnabled == null ? other$drawYLabelsEnabled != null : !this$drawYLabelsEnabled.equals(other$drawYLabelsEnabled))
            return false;
        final Object this$drawValuesEnabled = this.getDrawValuesEnabled();
        final Object other$drawValuesEnabled = other.getDrawValuesEnabled();
        if (this$drawValuesEnabled == null ? other$drawValuesEnabled != null : !this$drawValuesEnabled.equals(other$drawValuesEnabled))
            return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof RadarChartDisplaySettings;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = super.hashCode();
        final Object $drawXLabelsEnabled = this.getDrawXLabelsEnabled();
        result = result * PRIME + ($drawXLabelsEnabled == null ? 43 : $drawXLabelsEnabled.hashCode());
        final Object $drawYLabelsEnabled = this.getDrawYLabelsEnabled();
        result = result * PRIME + ($drawYLabelsEnabled == null ? 43 : $drawYLabelsEnabled.hashCode());
        final Object $drawValuesEnabled = this.getDrawValuesEnabled();
        result = result * PRIME + ($drawValuesEnabled == null ? 43 : $drawValuesEnabled.hashCode());
        return result;
    }
}
