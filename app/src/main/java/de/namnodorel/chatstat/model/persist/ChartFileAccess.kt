/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.persist

import com.google.gson.ExclusionStrategy
import com.google.gson.FieldAttributes
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import de.namnodorel.chatstat.model.chart.DisplaySettings
import de.namnodorel.chatstat.model.collector.CollectorType
import de.namnodorel.chatstat.model.collector.DataCollector
import de.namnodorel.chatstat.model.data.ChartSettings
import de.namnodorel.chatstat.model.groupselector.DataGroupSelector
import de.namnodorel.chatstat.model.persist.serialization.SerClass
import de.namnodorel.chatstat.model.persist.serialization.SerList
import de.namnodorel.chatstat.model.persist.serialization.SerTypeOf
import de.namnodorel.chatstat.model.pretransformer.PreTransformer
import de.namnodorel.chatstat.model.transformer.Transformer
import org.androidannotations.annotations.EBean
import java.io.*
import java.util.*

@EBean
open class ChartFileAccess {

    private val gson: Gson

    init {
        val builder = GsonBuilder()
        //This is needed since the type of some objects needs to be serialized
        builder.serializeNulls()
        builder.setExclusionStrategies(object : ExclusionStrategy {
            override fun shouldSkipField(f: FieldAttributes): Boolean {
                return false
            }

            override fun shouldSkipClass(clazz: Class<*>): Boolean {
                return false
            }
        })


        builder.registerTypeAdapter(ChartSettings::class.java, SerTypeOf<ChartSettings<out DisplaySettings>>())
        builder.registerTypeAdapter(List::class.java, SerList())
        builder.registerTypeAdapter(PreTransformer::class.java, SerTypeOf<PreTransformer>())
        builder.registerTypeAdapter(DataGroupSelector::class.java, SerTypeOf<DataGroupSelector>())
        builder.registerTypeAdapter(Class::class.java, SerClass())
        builder.registerTypeAdapter(CollectorType::class.java, SerTypeOf<CollectorType>())
        builder.registerTypeAdapter(DataCollector::class.java, SerTypeOf<DataCollector>())
        builder.registerTypeAdapter(Transformer::class.java, SerTypeOf<Transformer>())
        //For some reason generic types have to be registered as hierarchy adapter
        builder.registerTypeHierarchyAdapter(DisplaySettings::class.java, SerTypeOf<DisplaySettings>())
        gson = builder.create()
    }

    @Throws(IOException::class)
    fun readChartFile(inputStream: InputStream): List<ChartSettings<out DisplaySettings>> {

        val text = getTextFromFile(inputStream)

        val type = object : TypeToken<Set<ChartSettings<out DisplaySettings>>>() {

        }.type

        val set = gson.fromJson<Set<ChartSettings<out DisplaySettings>>>(text, type)

        return ArrayList(set)
    }

    @Throws(IOException::class)
    fun writeChartFile(charts: List<ChartSettings<out DisplaySettings>>, outputStream: OutputStream) {
        val type = object : TypeToken<Set<ChartSettings<out DisplaySettings>>>() {

        }.type

        val text = gson.toJson(HashSet(charts), type)

        putTextInFile(text, outputStream)

    }

    @Throws(IOException::class)
    private fun putTextInFile(text: String, outputStream: OutputStream) {
        val outputStreamWriter = OutputStreamWriter(outputStream)
        outputStreamWriter.write(text)
        outputStreamWriter.close()
    }

    @Throws(IOException::class)
    private fun getTextFromFile(inputStream: InputStream): String {
        val inputStreamReader = InputStreamReader(inputStream)
        val bufferedReader = BufferedReader(inputStreamReader)
        var receiveString: String?
        val stringBuilder = StringBuilder()

        receiveString = bufferedReader.readLine()
        while (receiveString != null) {
            stringBuilder.append(receiveString)

            receiveString = bufferedReader.readLine()
        }

        inputStream.close()

        return stringBuilder.toString()
    }

}
