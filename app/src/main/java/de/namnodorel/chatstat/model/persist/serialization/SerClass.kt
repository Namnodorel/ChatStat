/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.persist.serialization

import com.google.gson.*
import java.lang.reflect.Type


class SerClass : JsonSerializer<Class<*>>, JsonDeserializer<Class<*>> {

    override fun serialize(src: Class<*>, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
        val jsonObj = JsonObject()
        jsonObj.addProperty(CLASS_NAME, src.name)
        return jsonObj
    }

    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Class<*> {
        val jsonObj = json.asJsonObject

        val clsName = jsonObj.get(CLASS_NAME) as JsonPrimitive

        return Class.forName(clsName.asString)
    }

    companion object {
        const val CLASS_NAME = "class_name"
    }
}