/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.pretransformer

import de.namnodorel.chatstat.model.data.Message
import de.namnodorel.ofv.annotations.GenerateObjectForm
import io.reactivex.rxjava3.core.Observable
import org.joda.time.Interval

@GenerateObjectForm
class TimeFilterPreTransformer : PreTransformer() {

    var timespan: Interval? = null


    override fun apply(messages: Observable<Message>): Observable<Message> {
        return messages.filter{(date) -> timespan!!.contains(date.toDateTime())}
    }

    override fun valid(): Boolean {
        return timespan != null
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is TimeFilterPreTransformer) return false

        if (timespan != other.timespan) return false

        return true
    }

    override fun hashCode(): Int {
        return timespan?.hashCode() ?: 0
    }


}
