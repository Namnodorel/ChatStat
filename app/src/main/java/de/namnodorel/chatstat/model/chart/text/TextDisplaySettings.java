/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.chart.text;

import de.namnodorel.chatstat.model.chart.DisplaySettings;
import de.namnodorel.ofv.annotations.GenerateObjectForm;

@GenerateObjectForm
public class TextDisplaySettings extends DisplaySettings {

    private String preText = "";
    private String mainText = "";
    private String postText = "";

    public TextDisplaySettings() {
    }

    public String getPreText() {
        return this.preText;
    }

    public String getMainText() {
        return this.mainText;
    }

    public String getPostText() {
        return this.postText;
    }

    public void setPreText(String preText) {
        this.preText = preText;
    }

    public void setMainText(String mainText) {
        this.mainText = mainText;
    }

    public void setPostText(String postText) {
        this.postText = postText;
    }

    public String toString() {
        return "TextDisplaySettings(preText=" + this.getPreText() + ", mainText=" + this.getMainText() + ", postText=" + this.getPostText() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof TextDisplaySettings))
            return false;
        final TextDisplaySettings other = (TextDisplaySettings) o;
        if (!other.canEqual((Object) this)) return false;
        if (!super.equals(o)) return false;
        final Object this$preText = this.getPreText();
        final Object other$preText = other.getPreText();
        if (this$preText == null ? other$preText != null : !this$preText.equals(other$preText))
            return false;
        final Object this$mainText = this.getMainText();
        final Object other$mainText = other.getMainText();
        if (this$mainText == null ? other$mainText != null : !this$mainText.equals(other$mainText))
            return false;
        final Object this$postText = this.getPostText();
        final Object other$postText = other.getPostText();
        if (this$postText == null ? other$postText != null : !this$postText.equals(other$postText))
            return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof TextDisplaySettings;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = super.hashCode();
        final Object $preText = this.getPreText();
        result = result * PRIME + ($preText == null ? 43 : $preText.hashCode());
        final Object $mainText = this.getMainText();
        result = result * PRIME + ($mainText == null ? 43 : $mainText.hashCode());
        final Object $postText = this.getPostText();
        result = result * PRIME + ($postText == null ? 43 : $postText.hashCode());
        return result;
    }
}
