/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.input

import de.namnodorel.chatstat.model.data.ChatHistory
import de.namnodorel.chatstat.model.data.Message
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import org.joda.time.format.DateTimeFormat
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


class DefaultParser(private val locale: Locale, private val userTimeFormat: SimpleDateFormat) : ParserModel {

    companion object {
        private const val DATE_HOUR_SEPERATOR_REGEX = "\\D*"
        private const val DATETIME_CONTENT_SEPERATOR = " - "
    }

    override fun parse(text: Single<String>): ChatHistory {

        val messageStream = text
                .flatMapObservable { s ->
                    val dateRegex = getDateFormatRegex(locale)
                    val timeRegex = getTimeFormatRegex(userTimeFormat, locale)

                    val dateTimeRegex = ("($dateRegex)($DATE_HOUR_SEPERATOR_REGEX)($timeRegex)$DATETIME_CONTENT_SEPERATOR")

                    //Match any characters, until the dateRegex is found again. Result excludes the second dateRegex.
                    val regex = "$dateTimeRegex.+:(?:(?!($dateRegex))(.|\\n))*"

                    //Ugly workaround since Java 8 doesn't support Scanner.findAll() yet and the minimum required API version is 21
                    return@flatMapObservable Observable.fromArray(*getAllMatches(regex, s))

                }
                .map { origMsg ->

                    val dateEndIndex = getFirstRegexEndIndex(getDateFormatRegex(locale), origMsg)

                    var parsedMsg = origMsg.substring(0, dateEndIndex) + origMsg.substring(dateEndIndex).replaceFirst(DATE_HOUR_SEPERATOR_REGEX.toRegex(), "_")

                    val dateRegex = getDateFormatRegex(locale)
                    val timeRegex = getTimeFormatRegex(userTimeFormat, locale)
                    val dateTimeRegex = ("(" + dateRegex + ")"
                            + "(" + DATE_HOUR_SEPERATOR_REGEX + ")"
                            + "(" + timeRegex + ")")

                    val fullDateEndIndex = getFirstRegexEndIndex(dateTimeRegex, parsedMsg)

                    val pattern = getDateFormat(locale).toPattern() + "_" + userTimeFormat.toPattern()

                    val date = DateTimeFormat.forPattern(pattern).parseLocalDateTime(parsedMsg.substring(0, fullDateEndIndex))

                    //Remove the date, we have it parsed now
                    parsedMsg = parsedMsg.substring(fullDateEndIndex + DATETIME_CONTENT_SEPERATOR.length)

                    val name = parsedMsg.substring(0, parsedMsg.indexOf(':'))

                    //Remove the name, we have it parsed now
                    parsedMsg = parsedMsg.substring(parsedMsg.indexOf(':') + 1).trim { it <= ' ' }

                    return@map Message(date, name, parsedMsg)
                }

        return ChatHistory(messageStream.cache())

    }

    private fun getAllMatches(pattern: String, s: String): Array<String> {
        val results = ArrayList<String>()

        val m = Pattern.compile(pattern).matcher(s)
        while (m.find()) {
            results.add(m.toMatchResult().group())
        }

        return results.toTypedArray()
    }

    private fun getDateFormat(locale: Locale?): SimpleDateFormat {
        return DateFormat.getDateInstance(DateFormat.SHORT, locale) as SimpleDateFormat
    }

    //Kindly provided by Andreas https://stackoverflow.com/a/52393852/5489898
    private fun getDateFormatRegex(dateFormatLocale: Locale?): String {
        val regex = StringBuilder()

        //We have to use DateFormat to make sure we have the exact same format results as the source
        val format = getDateFormat(dateFormatLocale).toPattern()

        val m = Pattern.compile("[^a-zA-Z]+|([a-zA-Z])\\1*").matcher(format)
        while (m.find()) {
            val part = m.group()
            if (m.start(1) == -1) { // Not letter(s): Literal text
                regex.append(Pattern.quote(part))
            } else {
                when (part[0]) {
                    'G' // Era designator
                    -> regex.append("\\p{L}+")
                    'y' // Year
                    -> regex.append("\\d{1,4}")
                    'M' // Month in year
                    -> {
                        if (part.length > 2)
                            throw UnsupportedOperationException("Date format part: $part")
                        regex.append("(?:0?[1-9]|1[0-2])")
                    }
                    'd' // Day in month
                    -> regex.append("(?:0?[1-9]|[12][0-9]|3[01])")
                    else -> throw UnsupportedOperationException("Date format part: $part")
                }
            }
        }
        return regex.toString()
    }

    private fun getTimeFormatRegex(timeFormat: SimpleDateFormat, dateFormatLocale: Locale?): String {
        val regex = StringBuilder()

        val format = SimpleDateFormat(timeFormat.toPattern(), dateFormatLocale!!).toPattern()

        val m = Pattern.compile("[^a-zA-Z]+|([a-zA-Z])\\1*").matcher(format)
        while (m.find()) {
            val part = m.group()
            if (m.start(1) == -1) { // Not letter(s): Literal text
                regex.append(Pattern.quote(part))
            } else {
                when (part[0]) {
                    'a' //AM/PM
                    -> regex.append("(AM)|(PM)")
                    'H' //Hour in day 0-23
                        , 'k' //Hour in day 1-24
                        , 'K' //Hour in am/pm (0-11)
                        , 'h' //Hour in am/pm (1-12)
                        , 'm' //Minute in hour
                    -> regex.append("\\d{1,2}")
                    else -> throw UnsupportedOperationException("Date format part: $part")
                }
            }
        }
        return regex.toString()
    }


    private fun getFirstRegexEndIndex(regex: String, s: String): Int {
        val pt = Pattern.compile(regex)
        val matcher = pt.matcher(s)
        matcher.find()
        return matcher.end()
    }
}
