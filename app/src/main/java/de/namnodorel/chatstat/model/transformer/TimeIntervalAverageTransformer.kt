/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.transformer

import de.namnodorel.chatstat.model.data.DataTimeInterval
import de.namnodorel.chatstat.model.data.DataTimeInterval.*
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import de.namnodorel.chatstat.model.groupselector.DataGroupSelector
import de.namnodorel.chatstat.model.groupselector.StandardTimeIntervalGroupSelector
import de.namnodorel.ofv.annotations.GenerateObjectForm
import de.namnodorel.ofv.annotations.NoFormField
import org.joda.time.LocalDateTime

@GenerateObjectForm
class TimeIntervalAverageTransformer : DetailTransformer() {

    var newInterval: DataTimeInterval = MONTH

    @NoFormField
    override val updatedYUnit: String = "Average {original} per ${newInterval.name}"

    @NoFormField
    private lateinit var selector: StandardTimeIntervalGroupSelector

    fun init(groupSelector: DataGroupSelector) {
        if (groupSelector is StandardTimeIntervalGroupSelector) {
            selector = groupSelector
        } else {
            throw IllegalStateException("TimeIntervalAverageTransformer must be used together with StandardTimeIntervalGroupSelector!")
        }
    }

    override fun transformDetail(data: ValueGroup): ValueGroup {
        return ValueGroup(
                data.key,
                data.content
                        .map { (valueData, amount, label) ->
                            Value(
                                    calculateAverage(valueData, calculateOriginalDate(selector.interval, selector.countFrom!!, data.key), amount)!!,
                                    amount,
                                    label
                            )
                        }
        )
    }

    private fun calculateAverage(data: Float, referenceDate: LocalDateTime, messageCount: Int): Float? {

        return when (newInterval) {
            MESSAGE -> data / if (messageCount > 0) messageCount else 1
            HOUR -> when (selector.interval) {
                DAY -> data / referenceDate.hourOfDay().maximumValue
                WEEK -> data / (referenceDate.dayOfWeek().maximumValue * referenceDate.hourOfDay().maximumValue)
                MONTH -> data / (referenceDate.dayOfMonth().maximumValue * referenceDate.hourOfDay().maximumValue)
                YEAR -> data / (referenceDate.dayOfYear().maximumValue * referenceDate.hourOfDay().maximumValue)
                else -> data
            }
            DAY -> when (selector.interval) {
                WEEK -> data / referenceDate.dayOfWeek().maximumValue
                MONTH -> data / referenceDate.dayOfMonth().maximumValue
                YEAR -> data / referenceDate.dayOfYear().maximumValue
                else -> data
            }
            WEEK -> when (selector.interval) {
                MONTH -> data / (referenceDate.dayOfMonth().maximumValue / referenceDate.dayOfWeek().maximumValue)
                YEAR -> data / referenceDate.weekOfWeekyear().maximumValue
                else -> data
            }
            MONTH -> if (selector.interval === YEAR) {
                data / referenceDate.monthOfYear().maximumValue
            } else {
                data
            }
            else -> data
        }
    }

    private fun calculateOriginalDate(interval: DataTimeInterval, firstDate: LocalDateTime, value: Int): LocalDateTime {
        return when (interval) {
            HOUR -> firstDate.plusHours(value)
            DAY -> firstDate.plusDays(value)
            WEEK -> firstDate.plusWeeks(value)
            MONTH -> firstDate.plusMonths(value)
            YEAR -> firstDate.plusYears(value)
            else -> throw IllegalArgumentException("$interval is not a permitted interval argument for calculateOriginalDate()!")
        }
    }
}
