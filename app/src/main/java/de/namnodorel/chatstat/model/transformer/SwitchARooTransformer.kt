/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.transformer


import de.namnodorel.chatstat.model.data.ChartDataHolder
import de.namnodorel.chatstat.model.data.DataSetDataHolder
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import de.namnodorel.ofv.annotations.GenerateObjectForm
import de.namnodorel.ofv.annotations.NoFormField
import io.reactivex.rxjava3.core.Observable

@GenerateObjectForm
class SwitchARooTransformer : Transformer() {

    @NoFormField
    override val updatedYUnit: String = "{original}"

    override fun transform(upstream: ChartDataHolder): ChartDataHolder {

        return ChartDataHolder(upstream.content
                //Step one: Gather all possible new DataSets by flatMapping the LabelledValue from values and only keeping unique ones
                .flatMap { dataSetDataHolder ->
                    dataSetDataHolder.content
                            .flatMap { valueGroup ->
                                valueGroup.content
                                        .map { value ->
                                            if (value.hasLabel()) {
                                                return@map DataSetDataHolder(value.label!!, Observable.empty())
                                            } else {
                                                return@map DataSetDataHolder("", Observable.empty())
                                            }
                                        }
                            }
                }
                .distinct { it.label }
                //Step two: Map the old data to the new structure
                .map { newDataSetHolder ->
                    DataSetDataHolder(
                            newDataSetHolder.label,
                            upstream.content
                                    .flatMap { oldDataSetHolder ->
                                        oldDataSetHolder.content
                                                .map { valueGroup ->
                                                    ValueGroup(
                                                            valueGroup.key,
                                                            valueGroup.content
                                                                    .filter { value ->
                                                                        if (value.hasLabel()) {
                                                                            return@filter value.label == newDataSetHolder.label
                                                                        } else {
                                                                            return@filter newDataSetHolder.label == ""
                                                                        }
                                                                    }
                                                                    .map { value -> Value(value.valueData, value.amount, oldDataSetHolder.label) }
                                                    )
                                                }
                                    }
                                    .groupBy { it.key }
                                    .flatMapSingle { integerValueGroupGroupedObservable ->
                                        integerValueGroupGroupedObservable
                                                .reduce(
                                                        ValueGroup(integerValueGroupGroupedObservable.key!!, Observable.empty()),
                                                        { valueGroup1, valueGroup2 -> ValueGroup(valueGroup1.key, valueGroup1.content.concatWith(valueGroup2.content)) }
                                                )
                                    }
                    )
                }
        )
    }
}
