/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.pretransformer

import de.namnodorel.chatstat.model.data.Message
import de.namnodorel.ofv.annotations.GenerateObjectForm
import io.reactivex.rxjava3.core.Observable

@GenerateObjectForm
class TextCleanupPreTransformer : PreTransformer() {

    var removeNonLettersEnabled: Boolean = false

    var makeLowerCaseEnabled: Boolean = false

    override fun apply(messages: Observable<Message>): Observable<Message> {
        return messages.map{message -> Message(message.date, message.sender, clean(message.message))}
    }

    private fun clean(msg: String): String {
        var mutableMsg = msg
        if (removeNonLettersEnabled) {
            mutableMsg = mutableMsg.replace("[^A-Za-z0-9]".toRegex(), " ")
        }
        if (makeLowerCaseEnabled) {
            mutableMsg = mutableMsg.toLowerCase()
        }
        return mutableMsg
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is TextCleanupPreTransformer) return false

        if (removeNonLettersEnabled != other.removeNonLettersEnabled) return false
        if (makeLowerCaseEnabled != other.makeLowerCaseEnabled) return false

        return true
    }

    override fun hashCode(): Int {
        var result = removeNonLettersEnabled.hashCode()
        result = 31 * result + makeLowerCaseEnabled.hashCode()
        return result
    }


}
