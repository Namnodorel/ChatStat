/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.persist.defaults;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import de.namnodorel.chatstat.model.chart.DisplaySettings;
import de.namnodorel.chatstat.model.chart.bar.BarChartDisplaySettings;
import de.namnodorel.chatstat.model.chart.pie.PieChartDisplaySettings;
import de.namnodorel.chatstat.model.chart.radar.RadarChartDisplaySettings;
import de.namnodorel.chatstat.model.chart.scatter.ScatterChartDisplaySettings;
import de.namnodorel.chatstat.model.chart.text.TextDisplaySettings;
import de.namnodorel.chatstat.model.collector.CollectorType;
import de.namnodorel.chatstat.model.collector.DataCollector;
import de.namnodorel.chatstat.model.collector.MessageCollectorType;
import de.namnodorel.chatstat.model.collector.WordCollectorType;
import de.namnodorel.chatstat.model.data.ChartSettings;
import de.namnodorel.chatstat.model.data.DataTimeInterval;
import de.namnodorel.chatstat.model.groupselector.EverythingGroupSelector;
import de.namnodorel.chatstat.model.groupselector.LooseTimeIntervalGroupSelector;
import de.namnodorel.chatstat.model.groupselector.StandardTimeIntervalGroupSelector;
import de.namnodorel.chatstat.model.pretransformer.PreTransformer;
import de.namnodorel.chatstat.model.transformer.DetailPercentageTransformer;
import de.namnodorel.chatstat.model.transformer.SwitchARooTransformer;
import de.namnodorel.chatstat.model.transformer.Transformer;
import de.namnodorel.chatstat.model.transformer.ValueReduceTransformer;

public final class DefaultCharts {

    //There's not need to instantiate this; it only needs to statically hold default charts
    private DefaultCharts(){}


    public static List<ChartSettings<? extends DisplaySettings>> getDefaultCharts(){

        List<ChartSettings<? extends DisplaySettings>> charts = new ArrayList<>();

        charts.add(wordsOverMonths());
        charts.add(wordsOverWeeks());
        charts.add(messagesPerWeekday());
        charts.add(totalContribution());
        charts.add(wordsPerMessage());
        charts.add(totalMessages());

        return charts;
    }

    private static ChartSettings<BarChartDisplaySettings> wordsOverMonths(){
        String title = "Word % over months";

        String description = "How much % of the total words per month were sent?";

        List<PreTransformer> preTransformers = new LinkedList<>();

        StandardTimeIntervalGroupSelector groupSelector = new StandardTimeIntervalGroupSelector();
        groupSelector.setInterval(DataTimeInterval.MONTH);

        CollectorType collectorType = new WordCollectorType();

        List<DataCollector> dataCollectors = new LinkedList<>();

        List<Transformer> transformers = new LinkedList<>();
        transformers.add(new ValueReduceTransformer());
        transformers.add(new SwitchARooTransformer());
        transformers.add(new DetailPercentageTransformer());

        BarChartDisplaySettings displaySettings = new BarChartDisplaySettings();
        displaySettings.setSize(600);
        displaySettings.setIsVertical(true);
        displaySettings.setDrawXLabelsEnabled(true);
        displaySettings.setDrawYLabelsEnabled(true);
        displaySettings.setDrawBarValuesEnabled(false);
        displaySettings.setInitialZoom(5);
        displaySettings.setZoomEnabled(false);
        displaySettings.setDrawAxisLine(true);
        displaySettings.setDrawXGrid(false);
        displaySettings.setDrawYGrid(false);

        return new ChartSettings<>(title, description, preTransformers, groupSelector, collectorType, dataCollectors, transformers, displaySettings);
    }

    private static ChartSettings<BarChartDisplaySettings> wordsOverWeeks(){

        String title = "Words over weeks";

        String description = "How many words were sent by everyone in a week?";

        List<PreTransformer> preTransformers = new LinkedList<>();

        StandardTimeIntervalGroupSelector groupSelector = new StandardTimeIntervalGroupSelector();
        groupSelector.setInterval(DataTimeInterval.WEEK);

        CollectorType collectorType = new WordCollectorType();

        List<DataCollector> dataCollectors = new LinkedList<>();

        List<Transformer> transformers = new LinkedList<>();
        transformers.add(new ValueReduceTransformer());

        BarChartDisplaySettings displaySettings = new BarChartDisplaySettings();
        displaySettings.setSize(600);
        displaySettings.setIsVertical(true);
        displaySettings.setDrawXLabelsEnabled(true);
        displaySettings.setDrawYLabelsEnabled(true);
        displaySettings.setDrawBarValuesEnabled(false);
        displaySettings.setInitialZoom(10);
        displaySettings.setZoomEnabled(true);
        displaySettings.setDrawAxisLine(true);
        displaySettings.setDrawXGrid(false);
        displaySettings.setDrawYGrid(false);

        return new ChartSettings<>(title, description, preTransformers, groupSelector, collectorType, dataCollectors, transformers, displaySettings);
    }

    private static ChartSettings<RadarChartDisplaySettings> messagesPerWeekday(){

        String title = "Messages per Weekday";

        String description = "What day makes you the most chatty?";

        List<PreTransformer> preTransformers = new LinkedList<>();

        LooseTimeIntervalGroupSelector groupSelector = new LooseTimeIntervalGroupSelector();
        groupSelector.setInterval(DataTimeInterval.DAY);

        CollectorType collectorType = new MessageCollectorType();

        List<DataCollector> dataCollectors = new LinkedList<>();

        List<Transformer> transformers = new LinkedList<>();
        transformers.add(new ValueReduceTransformer());

        RadarChartDisplaySettings displaySettings = new RadarChartDisplaySettings();
        displaySettings.setSize(1000);
        displaySettings.setDrawXLabelsEnabled(true);
        displaySettings.setDrawYLabelsEnabled(true);
        displaySettings.setDrawValuesEnabled(false);

        return new ChartSettings<>(title, description, preTransformers, groupSelector, collectorType, dataCollectors, transformers, displaySettings);
    }

    private static ChartSettings<PieChartDisplaySettings> totalContribution(){

        String title = "Total conversation contributions";

        String description = "How much % of messages were sent in total by everyone?";

        List<PreTransformer> preTransformers = new LinkedList<>();

        EverythingGroupSelector groupSelector = new EverythingGroupSelector();

        CollectorType collectorType = new MessageCollectorType();

        List<DataCollector> dataCollectors = new LinkedList<>();

        List<Transformer> transformers = new LinkedList<>();
        transformers.add(new SwitchARooTransformer());
        transformers.add(new DetailPercentageTransformer());

        PieChartDisplaySettings displaySettings = new PieChartDisplaySettings();
        displaySettings.setSize(600);
        displaySettings.setHoleEnabled(true);
        displaySettings.setDrawLabelsEnabled(false);
        displaySettings.setDrawValuesEnabled(true);
        displaySettings.setHalfPieEnabled(false);

        return new ChartSettings<>(title, description, preTransformers, groupSelector, collectorType, dataCollectors, transformers, displaySettings);
    }

    private static ChartSettings<ScatterChartDisplaySettings> wordsPerMessage(){

        String title = "[!] Amount of words in every message";

        String description = "[This chart can be very performance intensive! Use with care] See all the details! In this graph, you can see how many words were sent in any message sent by anyone.";

        List<PreTransformer> preTransformers = new LinkedList<>();

        StandardTimeIntervalGroupSelector groupSelector = new StandardTimeIntervalGroupSelector();
        groupSelector.setInterval(DataTimeInterval.DAY);

        CollectorType collectorType = new WordCollectorType();

        List<DataCollector> dataCollectors = new LinkedList<>();

        List<Transformer> transformers = new LinkedList<>();

        ScatterChartDisplaySettings displaySettings = new ScatterChartDisplaySettings();
        displaySettings.setSize(1000);
        displaySettings.setDrawPointValuesEnabled(false);
        displaySettings.setDrawXLabelsEnabled(true);
        displaySettings.setDrawYLabelsEnabled(true);
        displaySettings.setInitialZoom(20);
        displaySettings.setZoomEnabled(true);

        return new ChartSettings<>(title, description, preTransformers, groupSelector, collectorType, dataCollectors, transformers, displaySettings);
    }

    private static ChartSettings<TextDisplaySettings> totalMessages(){

        String title = "Total messages sent";

        String description = "";

        List<PreTransformer> preTransformers = new LinkedList<>();

        EverythingGroupSelector groupSelector = new EverythingGroupSelector();

        CollectorType collectorType = new MessageCollectorType();

        List<DataCollector> dataCollectors = new LinkedList<>();

        List<Transformer> transformers = new LinkedList<>();
        transformers.add(new SwitchARooTransformer());
        transformers.add(new ValueReduceTransformer());

        TextDisplaySettings displaySettings = new TextDisplaySettings();
        displaySettings.setSize(500);
        displaySettings.setPreText("");
        displaySettings.setMainText("[val]");
        displaySettings.setPostText("messages sent");

        return new ChartSettings<>(title, description, preTransformers, groupSelector, collectorType, dataCollectors, transformers, displaySettings);
    }
}
