/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.persist;

import java.io.Serializable;

import de.namnodorel.chatstat.model.chart.DisplaySettings;
import de.namnodorel.chatstat.model.chart.bar.BarChartDisplaySettings;
import de.namnodorel.chatstat.model.chart.line.LineChartDisplaySettings;
import de.namnodorel.chatstat.model.chart.pie.PieChartDisplaySettings;
import de.namnodorel.chatstat.model.chart.radar.RadarChartDisplaySettings;
import de.namnodorel.chatstat.model.chart.scatter.ScatterChartDisplaySettings;
import de.namnodorel.chatstat.model.chart.text.TextDisplaySettings;
import de.namnodorel.chatstat.model.collector.EmojiStackCollectorType;
import de.namnodorel.chatstat.model.collector.MessageCollectorType;
import de.namnodorel.chatstat.model.collector.RegexMatchCollectorType;
import de.namnodorel.chatstat.model.collector.TimeCollectorType;
import de.namnodorel.chatstat.model.collector.CollectorType;
import de.namnodorel.chatstat.model.collector.WordStackCollectorType;
import de.namnodorel.chatstat.model.collector.WordCollectorType;
import de.namnodorel.chatstat.model.collector.XPerYCollectorType;
import de.namnodorel.chatstat.model.groupselector.DataGroupSelector;
import de.namnodorel.chatstat.model.groupselector.EverythingGroupSelector;
import de.namnodorel.chatstat.model.groupselector.LooseTimeIntervalGroupSelector;
import de.namnodorel.chatstat.model.groupselector.StandardTimeIntervalGroupSelector;
import de.namnodorel.chatstat.model.pretransformer.PreTransformer;
import de.namnodorel.chatstat.model.pretransformer.TextCleanupPreTransformer;
import de.namnodorel.chatstat.model.pretransformer.TimeFilterPreTransformer;
import de.namnodorel.chatstat.model.pretransformer.TimeShiftingPreTransformer;
import de.namnodorel.chatstat.model.transformer.AverageTransformer;
import de.namnodorel.chatstat.model.transformer.CountStackTransformer;
import de.namnodorel.chatstat.model.transformer.DetailPercentageTransformer;
import de.namnodorel.chatstat.model.transformer.MinMaxDetailTransformer;
import de.namnodorel.chatstat.model.transformer.PercentageTransformer;
import de.namnodorel.chatstat.model.transformer.StreakIsolationTransformer;
import de.namnodorel.chatstat.model.transformer.SwitchARooTransformer;
import de.namnodorel.chatstat.model.transformer.TimeIntervalAverageTransformer;
import de.namnodorel.chatstat.model.transformer.Transformer;
import de.namnodorel.chatstat.model.transformer.ValueReduceTransformer;

public enum ComponentType implements Serializable {

    PRE_TRANSFORMER(PreTransformer.class,
            TimeFilterPreTransformer.class,
            TimeShiftingPreTransformer.class,
            TextCleanupPreTransformer.class),
    GROUP_SELECTOR(DataGroupSelector.class,
            EverythingGroupSelector.class,
            LooseTimeIntervalGroupSelector.class,
            StandardTimeIntervalGroupSelector.class),
    COLLECTOR_TYPES(CollectorType.class,
            //XPerYCollectorType.class,  //This is left out as long as the implementation isn't functional
            EmojiStackCollectorType.class,
            MessageCollectorType.class,
            RegexMatchCollectorType.class,
            TimeCollectorType.class,
            WordCollectorType.class,
            WordStackCollectorType.class),
    TRANSFORMER(Transformer.class,
            AverageTransformer.class,
            DetailPercentageTransformer.class,
            PercentageTransformer.class,
            SwitchARooTransformer.class,
            TimeIntervalAverageTransformer.class,
            ValueReduceTransformer.class,
            MinMaxDetailTransformer.class,
            StreakIsolationTransformer.class,
            CountStackTransformer.class),
    CHART_TYPE(DisplaySettings.class,
            BarChartDisplaySettings.class,
            LineChartDisplaySettings.class,
            RadarChartDisplaySettings.class,
            ScatterChartDisplaySettings.class,
            PieChartDisplaySettings.class,
            TextDisplaySettings.class);

    public final Class type;

    public final Class[] subTypes;

    <T> ComponentType(Class<T> type, Class<? extends T>... subTypes) {
        this.type = type;
        this.subTypes = subTypes;
    }
}
