/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.chart.radar

import com.github.mikephil.charting.data.RadarData
import com.github.mikephil.charting.data.RadarDataSet
import com.github.mikephil.charting.data.RadarEntry
import de.namnodorel.chatstat.model.chart.DataToChartConverter
import de.namnodorel.chatstat.model.data.ChartDataHolder
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.functions.BiFunction
import java.util.*


class DataToRadarChartConverter : DataToChartConverter<RadarData>() {

    companion object {
        private val EMPTY_VALUES = Observable.just(Value(0f, -1))
    }

    override fun dataToChart(input: Single<ChartDataHolder>): Single<RadarData> {
        return input.flatMap { (content) ->
            content
                    .flatMapSingle { dataSetDataHolder ->
                        val radarDataSet = RadarDataSet(ArrayList(), dataSetDataHolder.label)

                        //We start from this so we have access to the max and min x value later on
                        dataSetDataHolder.content

                                .reduce(Integer.MIN_VALUE, { count, valueGroup -> if (valueGroup.key > count) valueGroup.key else count })
                                .zipWith<Int, Single<RadarDataSet>>(dataSetDataHolder.content
                                        .reduce(Integer.MAX_VALUE, { count, valueGroup -> if (valueGroup.key < count) valueGroup.key else count }),

                                        BiFunction{ xMax, xMin ->
                                            dataSetDataHolder.content
                                                    //If the valuegroups don't get added in correct order, MPAndroidChart throws a fit
                                                    .sorted { firstGroup, secondGroup -> firstGroup.key - secondGroup.key }
                                                    //Put empty values were there are none - without this,
                                                    //MPAndroidChart messes up the X Axis because it doesn't expect missing values
                                                    .mergeWith(
                                                            Observable.range(xMin, xMax)
                                                                    .map { key -> ValueGroup(key, EMPTY_VALUES) }
                                                    )
                                                    .groupBy { it.key }
                                                    .flatMapSingle { groupedValueGroups ->
                                                        groupedValueGroups.reduce(ValueGroup(groupedValueGroups.key!!, EMPTY_VALUES), { valueGroup, valueGroup2 ->
                                                            if (valueGroup.content == EMPTY_VALUES) {
                                                                return@reduce valueGroup2
                                                            } else {
                                                                return@reduce valueGroup
                                                            }
                                                        })
                                                    }

                                                    //Finally, do the actual mapping
                                                    .flatMapMaybe { valueGroup ->
                                                        valueGroup.content
                                                                .singleElement()
                                                                .map { value -> RadarEntry(value.valueData) }
                                                    }
                                                    .collectInto(radarDataSet, { obj, e -> obj.addEntry(e) })
                                        }
                                )
                                .flatMap { radarDataSetSingle -> radarDataSetSingle }
                    }
                    .collectInto(RadarData(), { radarData, d -> radarData.addDataSet(d) })
        }
    }
}
