/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.groupselector

import org.joda.time.DateTimeFieldType
import org.joda.time.IllegalFieldValueException
import org.joda.time.LocalDate
import org.joda.time.LocalDateTime
import org.joda.time.format.DateTimeFormat

import de.namnodorel.chatstat.model.data.DataTimeInterval
import de.namnodorel.chatstat.model.data.Message
import de.namnodorel.ofv.annotations.GenerateObjectForm

@GenerateObjectForm
class LooseTimeIntervalGroupSelector : DataGroupSelector() {

    var interval: DataTimeInterval = DataTimeInterval.MONTH

    override fun identifyGroup(msg: Message): Int {
        return msg.date.get(intervalToDateTimeFieldType(interval))
    }

    override fun getXLabel(key: Int): String {
        return try {

            when (interval) {
                DataTimeInterval.HOUR -> DateTimeFormat.forPattern("HH:mm").print(LocalDateTime().withHourOfDay(key))
                DataTimeInterval.DAY -> LocalDate().withDayOfWeek(key).dayOfWeek().asShortText
                DataTimeInterval.WEEK -> LocalDate().withWeekOfWeekyear(key).weekOfWeekyear().asShortText
                DataTimeInterval.MONTH -> LocalDate().withMonthOfYear(key).monthOfYear().asShortText
                DataTimeInterval.YEAR -> LocalDate().withYear(key).year().asShortText

                else -> throw IllegalArgumentException("$interval is not a permitted interval argument for getXLabel()!")
            }

        } catch (ex: IllegalFieldValueException) { //MPAndroidChart tried to get a label of something that has no label (eg. 0th day of the week)
            ""
        }

    }

    private fun intervalToDateTimeFieldType(inter: DataTimeInterval): DateTimeFieldType {
        return when (inter) {
            DataTimeInterval.HOUR -> DateTimeFieldType.hourOfDay()
            DataTimeInterval.DAY -> DateTimeFieldType.dayOfWeek()
            DataTimeInterval.WEEK -> DateTimeFieldType.weekOfWeekyear()
            DataTimeInterval.MONTH -> DateTimeFieldType.monthOfYear()
            DataTimeInterval.YEAR -> DateTimeFieldType.year()

            else -> throw IllegalArgumentException("$inter is not a permitted interval argument for intervalToDateTimeFieldType()!")
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is LooseTimeIntervalGroupSelector) return false

        if (interval != other.interval) return false

        return true
    }

    override fun hashCode(): Int {
        return interval.hashCode()
    }


}
