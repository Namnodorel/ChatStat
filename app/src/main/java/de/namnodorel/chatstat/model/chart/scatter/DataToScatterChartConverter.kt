/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.chart.scatter

import com.github.mikephil.charting.charts.ScatterChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.ScatterData
import com.github.mikephil.charting.data.ScatterDataSet
import de.namnodorel.chatstat.model.chart.DataToChartConverter
import de.namnodorel.chatstat.model.data.ChartDataHolder
import io.reactivex.rxjava3.core.Single
import java.util.*


class DataToScatterChartConverter : DataToChartConverter<ScatterData>() {
    override fun dataToChart(input: Single<ChartDataHolder>): Single<ScatterData> {
        return input
                .flatMap { (content) ->
                    content

                            .flatMapSingle { dataSetDataHolder ->
                                dataSetDataHolder.content

                                        //If the valuegroups don't get added in correct order, MPAndroidChart throws a fit
                                        .sorted { firstGroup, secondGroup -> firstGroup.key - secondGroup.key }

                                        .flatMap { valueGroup ->
                                            valueGroup.content
                                                    .map { value -> Entry(valueGroup.key.toFloat(), value.valueData) }
                                        }

                                        .collect({
                                            val dataSet = ScatterDataSet(LinkedList(), dataSetDataHolder.label)

                                            //TODO Make customizable
                                            dataSet.setScatterShape(ScatterChart.ScatterShape.TRIANGLE)
                                            dataSet
                                        }, { obj, e -> obj.addEntry(e) })
                            }

                            .collectInto(ScatterData(), { obj, d -> obj.addDataSet(d) })
                }
    }
}
