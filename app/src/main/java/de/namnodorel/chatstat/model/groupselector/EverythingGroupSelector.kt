/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.groupselector

import de.namnodorel.chatstat.model.data.Message
import de.namnodorel.ofv.annotations.GenerateObjectForm

@GenerateObjectForm
class EverythingGroupSelector : DataGroupSelector() {
    override fun identifyGroup(msg: Message): Int {
        return 0
    }

    override fun getXLabel(key: Int): String {
        return "All Time"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is EverythingGroupSelector) return false
        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }


}
