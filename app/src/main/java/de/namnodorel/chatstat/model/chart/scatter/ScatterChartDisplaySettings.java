/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.chart.scatter;

import de.namnodorel.chatstat.model.chart.DisplaySettings;
import de.namnodorel.chatstat.view.implementation.formelements.FormParamIntegerFormElement;
import de.namnodorel.ofv.annotations.GenerateObjectForm;

@GenerateObjectForm
public class ScatterChartDisplaySettings extends DisplaySettings {

    private Boolean drawXLabelsEnabled = true;
    private Boolean drawYLabelsEnabled = true;
    private Boolean drawPointValuesEnabled = false;
    @FormParamIntegerFormElement(min = 1, max = 200, allowAuto = false)
    private Integer initialZoom;
    private Boolean zoomEnabled = true;

    public ScatterChartDisplaySettings() {
    }

    public Boolean getDrawXLabelsEnabled() {
        return this.drawXLabelsEnabled;
    }

    public Boolean getDrawYLabelsEnabled() {
        return this.drawYLabelsEnabled;
    }

    public Boolean getDrawPointValuesEnabled() {
        return this.drawPointValuesEnabled;
    }

    public Integer getInitialZoom() {
        return this.initialZoom;
    }

    public Boolean getZoomEnabled() {
        return this.zoomEnabled;
    }

    public void setDrawXLabelsEnabled(Boolean drawXLabelsEnabled) {
        this.drawXLabelsEnabled = drawXLabelsEnabled;
    }

    public void setDrawYLabelsEnabled(Boolean drawYLabelsEnabled) {
        this.drawYLabelsEnabled = drawYLabelsEnabled;
    }

    public void setDrawPointValuesEnabled(Boolean drawPointValuesEnabled) {
        this.drawPointValuesEnabled = drawPointValuesEnabled;
    }

    public void setInitialZoom(Integer initialZoom) {
        this.initialZoom = initialZoom;
    }

    public void setZoomEnabled(Boolean zoomEnabled) {
        this.zoomEnabled = zoomEnabled;
    }

    public String toString() {
        return "ScatterChartDisplaySettings(drawXLabelsEnabled=" + this.getDrawXLabelsEnabled() + ", drawYLabelsEnabled=" + this.getDrawYLabelsEnabled() + ", drawPointValuesEnabled=" + this.getDrawPointValuesEnabled() + ", initialZoom=" + this.getInitialZoom() + ", zoomEnabled=" + this.getZoomEnabled() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof ScatterChartDisplaySettings))
            return false;
        final ScatterChartDisplaySettings other = (ScatterChartDisplaySettings) o;
        if (!other.canEqual((Object) this)) return false;
        if (!super.equals(o)) return false;
        final Object this$drawXLabelsEnabled = this.getDrawXLabelsEnabled();
        final Object other$drawXLabelsEnabled = other.getDrawXLabelsEnabled();
        if (this$drawXLabelsEnabled == null ? other$drawXLabelsEnabled != null : !this$drawXLabelsEnabled.equals(other$drawXLabelsEnabled))
            return false;
        final Object this$drawYLabelsEnabled = this.getDrawYLabelsEnabled();
        final Object other$drawYLabelsEnabled = other.getDrawYLabelsEnabled();
        if (this$drawYLabelsEnabled == null ? other$drawYLabelsEnabled != null : !this$drawYLabelsEnabled.equals(other$drawYLabelsEnabled))
            return false;
        final Object this$drawPointValuesEnabled = this.getDrawPointValuesEnabled();
        final Object other$drawPointValuesEnabled = other.getDrawPointValuesEnabled();
        if (this$drawPointValuesEnabled == null ? other$drawPointValuesEnabled != null : !this$drawPointValuesEnabled.equals(other$drawPointValuesEnabled))
            return false;
        final Object this$initialZoom = this.getInitialZoom();
        final Object other$initialZoom = other.getInitialZoom();
        if (this$initialZoom == null ? other$initialZoom != null : !this$initialZoom.equals(other$initialZoom))
            return false;
        final Object this$zoomEnabled = this.getZoomEnabled();
        final Object other$zoomEnabled = other.getZoomEnabled();
        if (this$zoomEnabled == null ? other$zoomEnabled != null : !this$zoomEnabled.equals(other$zoomEnabled))
            return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof ScatterChartDisplaySettings;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = super.hashCode();
        final Object $drawXLabelsEnabled = this.getDrawXLabelsEnabled();
        result = result * PRIME + ($drawXLabelsEnabled == null ? 43 : $drawXLabelsEnabled.hashCode());
        final Object $drawYLabelsEnabled = this.getDrawYLabelsEnabled();
        result = result * PRIME + ($drawYLabelsEnabled == null ? 43 : $drawYLabelsEnabled.hashCode());
        final Object $drawPointValuesEnabled = this.getDrawPointValuesEnabled();
        result = result * PRIME + ($drawPointValuesEnabled == null ? 43 : $drawPointValuesEnabled.hashCode());
        final Object $initialZoom = this.getInitialZoom();
        result = result * PRIME + ($initialZoom == null ? 43 : $initialZoom.hashCode());
        final Object $zoomEnabled = this.getZoomEnabled();
        result = result * PRIME + ($zoomEnabled == null ? 43 : $zoomEnabled.hashCode());
        return result;
    }
}
