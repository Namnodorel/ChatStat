/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.chart.text

import de.namnodorel.chatstat.model.chart.DataToChartConverter
import de.namnodorel.chatstat.model.data.ChartDataHolder
import io.reactivex.rxjava3.core.Single
import java.util.*


class DataToTextConverter : DataToChartConverter<Map<String, Any>>() {

    override fun dataToChart(input: Single<ChartDataHolder>): Single<Map<String, Any>> {
        //Extract all variables we care about
        return input
                .flatMap { (content) ->
                    content
                            .singleOrError()
                            .flatMap { dataSetDataHolder ->
                                dataSetDataHolder.content
                                        .singleOrError()
                                        .flatMap { valueGroup ->
                                            valueGroup.content
                                                    .singleOrError()
                                                    .map { value ->
                                                        val vals = HashMap<String, Any>()

                                                        vals["dataSetLabel"] = dataSetDataHolder.label

                                                        vals["key"] = valueGroup.key

                                                        vals["value"] = value.valueData

                                                        if (value.hasLabel()) {
                                                            vals["valueLabel"] = value.label!!
                                                        }

                                                        return@map vals
                                                    }
                                        }
                            }
                }
    }
}
