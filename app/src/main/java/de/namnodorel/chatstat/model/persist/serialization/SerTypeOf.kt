/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.persist.serialization

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParseException
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import com.google.gson.internal.UnsafeAllocator

import java.lang.reflect.Constructor
import java.lang.reflect.Field
import java.lang.reflect.Type
import java.util.ArrayList
import java.util.Arrays

import ru.vyarus.java.generics.resolver.GenericsResolver
import ru.vyarus.java.generics.resolver.context.GenericsContext
import ru.vyarus.java.generics.resolver.context.container.ParameterizedTypeImpl

class SerTypeOf<T : Any> : JsonSerializer<T>, JsonDeserializer<T> {

    override fun serialize(src: T, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {
        val retValue = JsonObject()
        val className = src.javaClass.name
        retValue.addProperty(TYPE_KEY, className)

        val obj = JsonObject()

        for (f in getAllFields(ArrayList(), src.javaClass)) {
            //Variables from the object class that we don't need
            if (f.name.startsWith("shadow$")) {
                continue
            }
            f.isAccessible = true

            try {
                if (f.type.isPrimitive) {

                    val el = context.serialize(f.get(src))
                    obj.add(f.name, el)

                } else {
                    val genContext = GenericsResolver.resolve(src.javaClass).fieldType(f)
                    val t = if (genContext.generics().isEmpty()) genContext.currentClass() else ParameterizedTypeImpl(genContext.currentClass(), *genContext.generics().toTypedArray())
                    val el = context.serialize(f.get(src), t)
                    obj.add(f.name, el)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        retValue.add(INSTANCE_KEY, obj)
        return retValue
    }

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): T {
        val jsonObject = json.asJsonObject

        val prim = jsonObject.get(TYPE_KEY) as JsonPrimitive?

        if (prim != null) {
            val className = prim.asString

            val klass: Class<*>?
            try {
                klass = Class.forName(className)
            } catch (e: ClassNotFoundException) {
                e.printStackTrace()
                throw JsonParseException(e.message)
            }

            return context.deserialize(jsonObject.get(INSTANCE_KEY), klass)

            //Has been selected as hierarchy adapter for compatibility with generics, now we have to do the job of the default gson deserialization
        } else {
            try {
                val klass = typeOfT as Class<T>

                val result: T

                result = if (hasParameterlessPublicConstructor(klass)) {
                    klass.newInstance() as T
                } else {
                    UnsafeAllocator.create().newInstance<T>(klass)
                }

                for (fieldName in jsonObject.keySet()) {

                    for (f in getAllFields(ArrayList(), klass)) {
                        if (f.name == fieldName) {
                            f.isAccessible = true
                            f.set(result, context.deserialize(jsonObject.get(fieldName), f.type))
                            break
                        }
                    }
                }

                return result
            } catch (e: Exception) {
                e.printStackTrace()
                throw JsonParseException(e.message)
            }

        }
    }

    private fun getAllFields(fields: MutableList<Field>, type: Class<*>): List<Field> {
        fields.addAll(listOf(*type.declaredFields))

        if (type.superclass != null) {
            getAllFields(fields, type.superclass!!)
        }

        return fields
    }

    private fun hasParameterlessPublicConstructor(clazz: Class<*>): Boolean {
        for (constructor in clazz.constructors) {
            if (constructor.parameterTypes.isEmpty()) {
                return true
            }
        }
        return false
    }

    companion object {

        private const val TYPE_KEY = "type"
        private const val INSTANCE_KEY = "instance"
    }
}
