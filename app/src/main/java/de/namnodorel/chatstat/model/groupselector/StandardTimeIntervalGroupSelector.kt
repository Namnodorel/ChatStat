/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.groupselector

import de.namnodorel.chatstat.model.data.DataTimeInterval
import de.namnodorel.chatstat.model.data.Message
import de.namnodorel.ofv.annotations.GenerateObjectForm
import de.namnodorel.ofv.annotations.NoFormField
import io.reactivex.rxjava3.core.Observable
import org.joda.time.*

@GenerateObjectForm
class StandardTimeIntervalGroupSelector : DataGroupSelector() {

    var interval: DataTimeInterval = DataTimeInterval.MONTH

    @NoFormField
    var countFrom: LocalDateTime? = null
        private set

    override fun init(messages: Observable<Message>): Observable<Message> {
        return messages.map { message ->
            if (countFrom == null || countFrom!!.isAfter(message.date)) {
                countFrom = message.date
            }
            message
        }
    }

    override fun identifyGroup(msg: Message): Int {
        if (msg.date.isBefore(countFrom!!)) {
            throw IllegalArgumentException("The message $msg has a date too early; counting from $countFrom")
        }

        return calculateIntervalDifference(interval, countFrom!!, msg.date)
    }

    override fun getXLabel(key: Int): String {
        val date = cleanDate(countFrom!!, this.interval)

        return when (this.interval) {

            DataTimeInterval.HOUR -> date.plusHours(key).hourOfDay().asText + " " + date.plusHours(key).dayOfMonth().asText + " " + date.plusHours(key).monthOfYear().asShortText + " " + date.plusHours(key).year().asShortText
            DataTimeInterval.DAY -> date.plusDays(key).dayOfMonth().asText + " " + date.plusDays(key).monthOfYear().asShortText + " " + date.plusDays(key).year().asShortText
            DataTimeInterval.WEEK -> date.plusWeeks(key).weekOfWeekyear.toString() + "w" + " " + date.plusWeeks(key).year
            DataTimeInterval.MONTH -> date.plusMonths(key).monthOfYear().asText + " " + date.plusMonths(key).year
            DataTimeInterval.YEAR -> date.plusYears(key).year().asText

            else -> throw IllegalArgumentException("$interval is not a permitted interval argument for getXLabel()!")
        }
    }

    private fun calculateIntervalDifference(interval: DataTimeInterval, baseDate: LocalDateTime, valueDate: LocalDateTime): Int {
        return when (interval) {
            DataTimeInterval.HOUR -> Hours.hoursBetween(cleanDate(baseDate, interval), cleanDate(valueDate, interval)).hours
            DataTimeInterval.DAY -> Days.daysBetween(cleanDate(baseDate, interval), cleanDate(valueDate, interval)).days
            DataTimeInterval.WEEK -> Weeks.weeksBetween(cleanDate(baseDate, interval), cleanDate(valueDate, interval)).weeks
            DataTimeInterval.MONTH -> Months.monthsBetween(cleanDate(baseDate, interval), cleanDate(valueDate, interval)).months
            DataTimeInterval.YEAR -> Years.yearsBetween(cleanDate(baseDate, interval), cleanDate(valueDate, interval)).years

            else -> throw IllegalArgumentException("$interval is not a permitted interval argument for calculateIntervalDifference()!")
        }
    }

    private fun cleanDate(date: LocalDateTime, forInterval: DataTimeInterval): LocalDateTime {
        return when (forInterval) {
            DataTimeInterval.HOUR -> date
                    .millisOfSecond().withMaximumValue()
                    .secondOfMinute().withMaximumValue()
                    .minuteOfHour().withMaximumValue()

            DataTimeInterval.DAY -> date
                    .millisOfSecond().withMaximumValue()
                    .secondOfMinute().withMaximumValue()
                    .minuteOfHour().withMaximumValue()
                    .hourOfDay().withMaximumValue()

            DataTimeInterval.WEEK -> date
                    .millisOfSecond().withMaximumValue()
                    .secondOfMinute().withMaximumValue()
                    .minuteOfHour().withMaximumValue()
                    .hourOfDay().withMaximumValue()
                    .dayOfWeek().withMaximumValue()

            DataTimeInterval.MONTH -> date
                    .millisOfSecond().withMaximumValue()
                    .secondOfMinute().withMaximumValue()
                    .minuteOfHour().withMaximumValue()
                    .hourOfDay().withMaximumValue()
                    .dayOfMonth().withMaximumValue()

            DataTimeInterval.YEAR -> date
                    .millisOfSecond().withMaximumValue()
                    .secondOfMinute().withMaximumValue()
                    .minuteOfHour().withMaximumValue()
                    .hourOfDay().withMaximumValue()
                    .dayOfYear().withMaximumValue()

            else -> date
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is StandardTimeIntervalGroupSelector) return false

        if (interval != other.interval) return false
        if (countFrom != other.countFrom) return false

        return true
    }

    override fun hashCode(): Int {
        var result = interval.hashCode()
        result = 31 * result + (countFrom?.hashCode() ?: 0)
        return result
    }


}
