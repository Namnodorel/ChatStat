/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.chart.bar

import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.ChartData

import java.util.ArrayList

import de.namnodorel.chatstat.model.chart.DataToChartConverter
import de.namnodorel.chatstat.model.data.ChartDataHolder
import de.namnodorel.chatstat.model.data.DataSetDataHolder
import de.namnodorel.chatstat.model.data.ValueGroup
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleSource
import io.reactivex.rxjava3.functions.BiConsumer
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.functions.Function


class DataToBarChartConverter : DataToChartConverter<BarData>() {

    //Cannot be CollectorType Tested, because MPAndroidChart doesn't properly separate its data from its view logic, AND does not
    //provide equals() and hashcode() implementations with its data objects.

    override fun dataToChart(input: Single<ChartDataHolder>): Single<BarData> {

        return input.flatMap { (content) ->
            content
                    .flatMapSingle { dataSetDataHolder ->
                        val barDataSet = BarDataSet(ArrayList(), dataSetDataHolder.label)

                        dataSetDataHolder.content
                                //If the valuegroups don't get added in correct order, MPAndroidChart throws a fit
                                .sorted { firstGroup, secondGroup -> firstGroup.key - secondGroup.key }
                                //Sort the labelled values to avoid confusion
                                .map { valueGroup ->
                                    ValueGroup(valueGroup.key, valueGroup.content
                                            .sorted { o1, o2 ->
                                                if (o1.hasLabel() && o2.hasLabel()) {
                                                    //Sort alphabetically - if this is not possible, labelled values are ranked higher
                                                    return@sorted o1.label!!.compareTo(o2.label!!, ignoreCase = true)
                                                } else if (o1.hasLabel()) {
                                                    return@sorted 1
                                                } else if (o2.hasLabel()) {
                                                    return@sorted -1
                                                } else {
                                                    return@sorted 0
                                                }
                                            }
                                    )
                                }
                                .flatMapSingle { valueGroup ->

                                    val values = valueGroup.content
                                            .collectInto(ArrayList<Float>(), { values1, value -> values1.add(value.valueData) })


                                    val labels = valueGroup.content
                                            .collectInto(ArrayList<String>(), { values1, value ->
                                                if (value.hasLabel())
                                                    values1.add(value.label!!)
                                            })

                                    Single.zip<List<Float>, List<String>, BarEntry>(values, labels, BiFunction{ values12, labels1 ->

                                        val arrayValues = FloatArray(values12.size)
                                        for (i in values12.indices) {
                                            arrayValues[i] = values12[i]
                                        }

                                        val barEntry = BarEntry(valueGroup.key.toFloat(), arrayValues)
                                        barEntry.data = labels1

                                        if (barDataSet.stackLabels.size < labels1.size) {
                                            barDataSet.stackLabels = labels1.toTypedArray()
                                        }

                                        return@BiFunction barEntry
                                    })
                                }
                                .collectInto(barDataSet, { dataSet, e -> dataSet.addEntry(e) })
                    }
                    .collectInto(BarData(), { barData, d -> barData.addDataSet(d) })
        }

    }
}
