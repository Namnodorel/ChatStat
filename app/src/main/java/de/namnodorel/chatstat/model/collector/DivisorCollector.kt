/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.collector

import de.namnodorel.chatstat.model.data.Message
import de.namnodorel.chatstat.model.data.ValueGroup
import de.namnodorel.ofv.annotations.GenerateObjectForm
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

@GenerateObjectForm
class DivisorCollector : DataCollector() {

    var collector1: CollectorType? = null

    var collector2: CollectorType? = null

    override fun collectData(messages: Observable<Message>, key: Int?): Single<ValueGroup> {
        TODO("Better implementation that respects labels and can deal with uneven value groups")
    }

    override fun valid(): Boolean {
        return collector1 != null && collector2 != null
    }
}

