/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.collector

import de.namnodorel.chatstat.model.data.Message
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import de.namnodorel.ofv.annotations.GenerateObjectForm
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.functions.BiFunction
import org.joda.time.Minutes

@GenerateObjectForm
class TimeCollector : DataCollector() {

    var callers: Set<String> = HashSet()

    var responders: Set<String> = HashSet()

    override fun collectData(messages: Observable<Message>, key: Int?): Single<ValueGroup> {

        return Single.just(
                ValueGroup(
                        key!!,
                        //Calculate value
                        messages.filter { msg -> isCaller(msg.sender) || isResponder(msg.sender) }
                                .buffer(2, 1)
                                .filter { messages1 -> messages1.size == 2 && isCaller(messages1[0].sender) && isResponder(messages1[1].sender) }
                                .map { pairedMessages -> Minutes.minutesBetween(pairedMessages[0].date, pairedMessages[1].date).minutes }
                                .flatMapSingle { minutesBetweenMessages ->
                                    Single.zip(
                                            Single.just(minutesBetweenMessages),
                                            //Get amount of data points needed for this value
                                            messages.filter { msg -> isCaller(msg.sender) || isResponder(msg.sender) }
                                                    .buffer(2, 1)
                                                    .filter { messages1 -> messages1.size == 2 && isCaller(messages1[0].sender) && isResponder(messages1[1].sender) }
                                                    .count(),
                                            BiFunction<Int, Long, Value>{ minutesValue, amount -> Value(minutesValue.toFloat(), amount.toInt()) }
                                    )
                                }
                )
        )
    }

    private fun isCaller(sender: String): Boolean {
        return callers.contains(sender)
    }

    private fun isResponder(sender: String): Boolean {
        return responders.contains(sender)
    }


    override fun valid(): Boolean {
        return (callers.isNotEmpty()
                && responders.isNotEmpty()
                && callers.none { it.isEmpty() }
                && responders.none { it.isEmpty() })
    }
}
