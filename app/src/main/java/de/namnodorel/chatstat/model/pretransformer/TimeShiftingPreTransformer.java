/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.pretransformer;

import org.joda.time.DurationFieldType;

import de.namnodorel.chatstat.model.data.Message;
import de.namnodorel.chatstat.view.implementation.formelements.FormParamIntegerFormElement;
import de.namnodorel.ofv.annotations.GenerateObjectForm;
import io.reactivex.rxjava3.core.Observable;

@GenerateObjectForm
public class TimeShiftingPreTransformer extends PreTransformer {

    @FormParamIntegerFormElement(min = -12, max = 12, allowAuto = false)
    private Integer hourShift;

    @Override
    public Observable<Message> apply(Observable<Message> messages) {
        return messages.map(message -> new Message(message.getDate().withFieldAdded(DurationFieldType.hours(), getHourShift()), message.getSender(), message.getMessage()));
    }

    @Override
    public boolean valid() {
        return hourShift != null;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof TimeShiftingPreTransformer))
            return false;
        final TimeShiftingPreTransformer other = (TimeShiftingPreTransformer) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$hourShift = this.hourShift;
        final Object other$hourShift = other.hourShift;
        if (this$hourShift == null ? other$hourShift != null : !this$hourShift.equals(other$hourShift))
            return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof TimeShiftingPreTransformer;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $hourShift = this.hourShift;
        result = result * PRIME + ($hourShift == null ? 43 : $hourShift.hashCode());
        return result;
    }

    public Integer getHourShift() {
        return this.hourShift;
    }

    public void setHourShift(Integer hourShift) {
        this.hourShift = hourShift;
    }
}
