/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.chart.bar;

import de.namnodorel.chatstat.model.chart.DisplaySettings;
import de.namnodorel.chatstat.view.implementation.formelements.FormParamIntegerFormElement;
import de.namnodorel.ofv.annotations.GenerateObjectForm;

@GenerateObjectForm
public class BarChartDisplaySettings extends DisplaySettings{

    private Boolean isVertical = false;
    private Boolean drawXLabelsEnabled = true;
    private Boolean drawYLabelsEnabled = true;
    private Boolean drawBarValuesEnabled = false;
    @FormParamIntegerFormElement(min = 1, max = 30, allowAuto = false)
    private Integer initialZoom;
    private Boolean zoomEnabled = false;
    private Boolean drawAxisLine = false;
    private Boolean drawXGrid = false;
    private Boolean drawYGrid = false;

    public BarChartDisplaySettings() {
    }

    public Boolean getIsVertical() {
        return this.isVertical;
    }

    public Boolean getDrawXLabelsEnabled() {
        return this.drawXLabelsEnabled;
    }

    public Boolean getDrawYLabelsEnabled() {
        return this.drawYLabelsEnabled;
    }

    public Boolean getDrawBarValuesEnabled() {
        return this.drawBarValuesEnabled;
    }

    public Integer getInitialZoom() {
        return this.initialZoom;
    }

    public Boolean getZoomEnabled() {
        return this.zoomEnabled;
    }

    public void setIsVertical(Boolean isVertical) {
        this.isVertical = isVertical;
    }

    public void setDrawXLabelsEnabled(Boolean drawXLabelsEnabled) {
        this.drawXLabelsEnabled = drawXLabelsEnabled;
    }

    public void setDrawYLabelsEnabled(Boolean drawYLabelsEnabled) {
        this.drawYLabelsEnabled = drawYLabelsEnabled;
    }

    public void setDrawBarValuesEnabled(Boolean drawBarValuesEnabled) {
        this.drawBarValuesEnabled = drawBarValuesEnabled;
    }

    public void setInitialZoom(Integer initialZoom) {
        this.initialZoom = initialZoom;
    }

    public void setZoomEnabled(Boolean zoomEnabled) {
        this.zoomEnabled = zoomEnabled;
    }

    public String toString() {
        return "BarChartDisplaySettings(isVertical=" + this.getIsVertical() + ", drawXLabelsEnabled=" + this.getDrawXLabelsEnabled() + ", drawYLabelsEnabled=" + this.getDrawYLabelsEnabled() + ", drawBarValuesEnabled=" + this.getDrawBarValuesEnabled() + ", initialZoom=" + this.getInitialZoom() + ", zoomEnabled=" + this.getZoomEnabled() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof BarChartDisplaySettings))
            return false;
        final BarChartDisplaySettings other = (BarChartDisplaySettings) o;
        if (!other.canEqual((Object) this)) return false;
        if (!super.equals(o)) return false;
        final Object this$isVertical = this.getIsVertical();
        final Object other$isVertical = other.getIsVertical();
        if (this$isVertical == null ? other$isVertical != null : !this$isVertical.equals(other$isVertical))
            return false;
        final Object this$drawXLabelsEnabled = this.getDrawXLabelsEnabled();
        final Object other$drawXLabelsEnabled = other.getDrawXLabelsEnabled();
        if (this$drawXLabelsEnabled == null ? other$drawXLabelsEnabled != null : !this$drawXLabelsEnabled.equals(other$drawXLabelsEnabled))
            return false;
        final Object this$drawYLabelsEnabled = this.getDrawYLabelsEnabled();
        final Object other$drawYLabelsEnabled = other.getDrawYLabelsEnabled();
        if (this$drawYLabelsEnabled == null ? other$drawYLabelsEnabled != null : !this$drawYLabelsEnabled.equals(other$drawYLabelsEnabled))
            return false;
        final Object this$drawBarValuesEnabled = this.getDrawBarValuesEnabled();
        final Object other$drawBarValuesEnabled = other.getDrawBarValuesEnabled();
        if (this$drawBarValuesEnabled == null ? other$drawBarValuesEnabled != null : !this$drawBarValuesEnabled.equals(other$drawBarValuesEnabled))
            return false;
        final Object this$initialZoom = this.getInitialZoom();
        final Object other$initialZoom = other.getInitialZoom();
        if (this$initialZoom == null ? other$initialZoom != null : !this$initialZoom.equals(other$initialZoom))
            return false;
        final Object this$zoomEnabled = this.getZoomEnabled();
        final Object other$zoomEnabled = other.getZoomEnabled();
        if (this$zoomEnabled == null ? other$zoomEnabled != null : !this$zoomEnabled.equals(other$zoomEnabled))
            return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof BarChartDisplaySettings;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = super.hashCode();
        final Object $isVertical = this.getIsVertical();
        result = result * PRIME + ($isVertical == null ? 43 : $isVertical.hashCode());
        final Object $drawXLabelsEnabled = this.getDrawXLabelsEnabled();
        result = result * PRIME + ($drawXLabelsEnabled == null ? 43 : $drawXLabelsEnabled.hashCode());
        final Object $drawYLabelsEnabled = this.getDrawYLabelsEnabled();
        result = result * PRIME + ($drawYLabelsEnabled == null ? 43 : $drawYLabelsEnabled.hashCode());
        final Object $drawBarValuesEnabled = this.getDrawBarValuesEnabled();
        result = result * PRIME + ($drawBarValuesEnabled == null ? 43 : $drawBarValuesEnabled.hashCode());
        final Object $initialZoom = this.getInitialZoom();
        result = result * PRIME + ($initialZoom == null ? 43 : $initialZoom.hashCode());
        final Object $zoomEnabled = this.getZoomEnabled();
        result = result * PRIME + ($zoomEnabled == null ? 43 : $zoomEnabled.hashCode());
        return result;
    }

    public Boolean getDrawYGrid() {
        return drawYGrid;
    }

    public void setDrawYGrid(Boolean drawYGrid) {
        this.drawYGrid = drawYGrid;
    }

    public Boolean getDrawXGrid() {
        return drawXGrid;
    }

    public void setDrawXGrid(Boolean drawXGrid) {
        this.drawXGrid = drawXGrid;
    }

    public Boolean getDrawAxisLine() {
        return drawAxisLine;
    }

    public void setDrawAxisLine(Boolean drawZeroLine) {
        this.drawAxisLine = drawZeroLine;
    }
}
