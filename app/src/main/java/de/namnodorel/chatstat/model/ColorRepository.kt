/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model

import com.github.lzyzsd.randomcolor.RandomColor
import java.io.Serializable

import java.util.HashMap

class ColorRepository(private val creationListener: ColorCreationListener? = null): Serializable {

    private val colorStore = HashMap<String, Int>()

    val labels: Set<String>
        get() = colorStore.keys


    //Create a copy without the color creation listener which can be serialized
    constructor(copyFrom: ColorRepository) : this() {
        this.colorStore.putAll(copyFrom.colorStore)
    }

    fun getColor(forLabel: String): Int {

        if (colorStore.containsKey(forLabel)) {
            return colorStore[forLabel]!!

        } else {

            //Generate, store and return a new (unused) color
            var col: Int? = null
            while (col == null || colorStore.containsValue(col)) {
                col = RandomColor().randomColor()
            }

            colorStore[forLabel] = col

            creationListener?.onNewColorCreated(forLabel, col)

            return col
        }
    }

    fun setColorForLabel(label: String, col: Int?) {
        colorStore[label] = col!!
    }

    interface ColorCreationListener {
        fun onNewColorCreated(label: String, color: Int)
    }
}
