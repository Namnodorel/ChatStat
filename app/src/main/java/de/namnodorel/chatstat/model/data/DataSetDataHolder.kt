/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.data

import io.reactivex.rxjava3.core.Observable

data class DataSetDataHolder(val label: String, val content: Observable<ValueGroup>){

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is DataSetDataHolder) return false

        if (label != other.label) return false
        if (!Observable.sequenceEqual<Any>(content, other.content).blockingGet()) return false

        return true
    }

    override fun hashCode(): Int {
        var result = label.hashCode()
        result = 31 * result + content.hashCode()
        return result
    }

    override fun toString(): String {
        return "DataSetDataHolder(label='$label', content=${content.toList().blockingGet()})"
    }

}
