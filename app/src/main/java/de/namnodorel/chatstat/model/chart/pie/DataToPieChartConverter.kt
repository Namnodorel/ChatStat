/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.chart.pie

import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import de.namnodorel.chatstat.model.chart.DataToChartConverter
import de.namnodorel.chatstat.model.data.ChartDataHolder
import io.reactivex.rxjava3.core.Single
import java.util.*


class DataToPieChartConverter : DataToChartConverter<PieData>() {
    override fun dataToChart(input: Single<ChartDataHolder>): Single<PieData> {
        return input
                .flatMap { (content) ->
                    content

                            .flatMapSingle { dataSetDataHolder ->
                                dataSetDataHolder.content

                                        .singleOrError()
                                        .flatMap { valueGroup ->
                                            valueGroup.content

                                                    .collectInto(LinkedList<PieEntry>(), { pieEntries, value -> pieEntries.add(PieEntry(value.valueData, if (value.hasLabel()) value.label else null)) })
                                        }

                                        .map { pieEntries -> PieDataSet(pieEntries, dataSetDataHolder.label) }
                            }

                            .singleOrError()
                            .map { PieData(it) }
                }
    }
}
