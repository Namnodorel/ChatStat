/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.transformer

import de.namnodorel.chatstat.model.data.ChartDataHolder
import de.namnodorel.chatstat.model.data.DataSetDataHolder
import de.namnodorel.chatstat.model.data.ValueGroup
import io.reactivex.rxjava3.functions.Function

abstract class DetailTransformer : Transformer() {

    override fun transform(upstream: ChartDataHolder): ChartDataHolder {
        return ChartDataHolder(upstream.content
                .map{ (label, content) ->
                    DataSetDataHolder(
                            label,
                            content.map{transformDetail(it)}
                    )
                }
        )
    }

    abstract fun transformDetail(data: ValueGroup): ValueGroup

}
