/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.transformer

import de.namnodorel.chatstat.model.data.ChartDataHolder
import de.namnodorel.chatstat.model.data.DataSetDataHolder
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import de.namnodorel.ofv.annotations.GenerateObjectForm
import de.namnodorel.ofv.annotations.NoFormField
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.functions.BiFunction
import kotlin.math.abs

@GenerateObjectForm
class StreakIsolationTransformer : Transformer() {

    @NoFormField
    override val updatedYUnit: String = "Streaks of {original}"

    override fun transform(upstream: ChartDataHolder): ChartDataHolder {
        return ChartDataHolder(upstream.content
                .map { (label, content) ->
                    DataSetDataHolder(
                            label,
                            content
                                    //Remove 0 values
                                    .flatMapMaybe { (key, content1) ->
                                        content1
                                                .singleElement()
                                                .filter { (valueData) -> valueData != 0f }
                                                .map { value -> ValueGroup(key, Observable.just(value)) }
                                    }

                                    //Stack values with monotonically increasing keys
                                    .sorted { firstGroup, secondGroup -> firstGroup.key - secondGroup.key }
                                    .scan { valueGroup, valueGroup2 ->
                                        val diff = valueGroup.key + valueGroup.content.count().blockingGet().toInt() - valueGroup2.key
                                        if (abs(diff) >= 1) {
                                            return@scan valueGroup2
                                        } else {
                                            return@scan ValueGroup(valueGroup.key, valueGroup.content.concatWith(valueGroup2.content))
                                        }
                                    }
                                    //Only preserve the biggest stack for each key
                                    .groupBy { it.key }
                                    .flatMapSingle { integerValueGroupGroupedObservable ->
                                        integerValueGroupGroupedObservable
                                                .reduce(ValueGroup(integerValueGroupGroupedObservable.key!!, Observable.empty()), { valueGroup, valueGroup2 ->

                                                    val count1 = valueGroup.content.count()
                                                    val count2 = valueGroup2.content.count()

                                                    return@reduce ValueGroup(valueGroup.key, Single.zip(
                                                            count1,
                                                            count2,
                                                            BiFunction<Long, Long, Observable<Value>> { aLong, aLong2 ->
                                                                if (aLong > aLong2) {
                                                                    return@BiFunction valueGroup.content
                                                                } else {
                                                                    return@BiFunction valueGroup2.content
                                                                }
                                                            }
                                                    ).flatMapObservable { content -> content })
                                                })
                                    }
                    )
                }

        )
    }
}
