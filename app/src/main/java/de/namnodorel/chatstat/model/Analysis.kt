/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model


import de.namnodorel.chatstat.model.chart.DisplaySettings
import de.namnodorel.chatstat.model.collector.DataCollector
import de.namnodorel.chatstat.model.collector.SimpleCollector
import de.namnodorel.chatstat.model.data.ChartDataHolder
import de.namnodorel.chatstat.model.data.ChatHistory
import de.namnodorel.chatstat.model.data.DataSetDataHolder
import de.namnodorel.chatstat.model.data.ChartSettings
import de.namnodorel.chatstat.model.transformer.TimeIntervalAverageTransformer
import io.reactivex.rxjava3.core.Single


class Analysis(val chart: ChartSettings<out DisplaySettings>) {

    fun analyze(history: ChatHistory): Single<ChartDataHolder> {

        val messages = history.messages.cache()

        val dataHolder = ChartDataHolder(
                chart.dataCollectorsObservable
                        .switchIfEmpty (messages
                                .map { it.sender }
                                .distinct()
                                .map { participant ->
                                    val simpleCollector = chart.collectorType.collector.newInstance() as SimpleCollector
                                    simpleCollector.target = hashSetOf(participant)
                                    val collector = simpleCollector as DataCollector
                                    collector.label = participant
                                    return@map collector
                                }
                        )
                        .map { dataCollector -> DataSetDataHolder(dataCollector.label, Single.just(messages)

                            .flatMap { upstream -> chart.preTransformersObservable.reduce(upstream, { messageObservable, preTransformer -> preTransformer.apply(messageObservable) }) }

                            //Initialize GroupSelector, some need this to reset eg. the reference date to count from
                            .map { messageObservable -> chart.dataGroupSelector.init(messageObservable) }

                            .flatMapObservable { messageObservable -> messageObservable

                                        //Group by "X" value the entries get later on - how this value is determined is up to the DataGroupSelector
                                        .groupBy { message -> chart.dataGroupSelector.identifyGroup(message) }
                                        //Summarize important values (words/messages) of grouped messages and return them as entrys
                                        .flatMapSingle { xGroupedMsgStream -> dataCollector.collectData(xGroupedMsgStream.cache(), xGroupedMsgStream.key as Int) }
                            }
                    )
                }
        )

        return chart.dataTransformersObservable
                .map { transformer ->
                    if (transformer is TimeIntervalAverageTransformer) {
                        transformer.init(chart.dataGroupSelector)
                    }
                    return@map transformer
                }
                .reduce(
                        Single.just(dataHolder),
                        { data, transformer -> data.map<ChartDataHolder> { transformer.transform(it) } }
                ).flatMap { gdh -> gdh }
    }
}
