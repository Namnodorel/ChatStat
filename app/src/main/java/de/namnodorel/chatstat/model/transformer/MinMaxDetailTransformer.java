/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.transformer;

import de.namnodorel.chatstat.model.data.Value;
import de.namnodorel.chatstat.model.data.ValueGroup;
import de.namnodorel.chatstat.view.implementation.formelements.FormParamIntegerFormElement;
import de.namnodorel.ofv.annotations.GenerateObjectForm;

@GenerateObjectForm
public class MinMaxDetailTransformer extends DetailTransformer {

    //If true, take the minimum values. If false, take the maximum ones
    private Boolean takeMin;

    @FormParamIntegerFormElement(min = 1, max = 100, allowAuto = false)
    private Integer count;

    private Boolean keepRestSummedUp;

    @Override
    public ValueGroup transformDetail(ValueGroup data) {
        return new ValueGroup(data.getKey(), data.getContent()
                .sorted((o1, o2) -> {
                    float res = o1.getValueData() - o2.getValueData();
                    if((res > 0 && takeMin) || (res < 0 && !takeMin)){
                        return 1;
                    }else if((res < 0 && takeMin) || (res > 0 && !takeMin)){
                        return -1;
                    }else{
                        return 0;
                    }
                })
                .take(count)
                .compose(upstream -> {
                    if(!keepRestSummedUp){
                        return upstream;
                    }else{
                        return upstream
                                .concatWith(data.getContent()
                                        .sorted((o1, o2) -> {
                                            float res = o1.getValueData() - o2.getValueData();
                                            if((res > 0 && takeMin) || (res < 0 && !takeMin)){
                                                return 1;
                                            }else if((res < 0 && takeMin) || (res > 0 && !takeMin)){
                                                return -1;
                                            }else{
                                                return 0;
                                            }
                                        })
                                        .skip(count)
                                        .reduce((value, value2) -> new Value(value.getValueData() + value2.getValueData(), value.getAmount() + value2.getAmount(), "Rest"))
                                        .toObservable()
                                );
                    }
                })
        );
    }

    @Override
    public String getUpdatedYUnit() {
        return takeMin?"Lowest":"Highest" + " " + count + " {original}";
    }

    @Override
    public boolean valid() {
        return takeMin != null
                && count != null
                && count > 0
                && keepRestSummedUp != null;
    }

    public Boolean getTakeMin() {
        return this.takeMin;
    }

    public Integer getCount() {
        return this.count;
    }

    public Boolean getKeepRestSummedUp() {
        return this.keepRestSummedUp;
    }

    public void setTakeMin(Boolean takeMin) {
        this.takeMin = takeMin;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public void setKeepRestSummedUp(Boolean keepRestSummedUp) {
        this.keepRestSummedUp = keepRestSummedUp;
    }
}
