/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.transformer

import de.namnodorel.chatstat.model.data.ChartDataHolder
import de.namnodorel.chatstat.model.data.DataSetDataHolder
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import de.namnodorel.ofv.annotations.GenerateObjectForm
import de.namnodorel.ofv.annotations.NoFormField
import io.reactivex.rxjava3.core.Observable

@GenerateObjectForm
class PercentageTransformer : Transformer() {

    @NoFormField
    override val updatedYUnit: String = "{original} in total %"

    override fun transform(upstream: ChartDataHolder): ChartDataHolder {

        val summedValueGroups = upstream.content
                .flatMap { it.content }
                .groupBy { it.key }
                .flatMapSingle { integerValueGroupGroupedObservable ->
                    integerValueGroupGroupedObservable
                            .flatMap { it.content }
                            .reduce(
                                    Value(0f, 0),
                                    { soFar, value -> Value(soFar.valueData + value.valueData, soFar.amount + value.amount) }
                            )
                            .cache()
                            .map { value -> ValueGroup(integerValueGroupGroupedObservable.key!!, Observable.just(value)) }
                }

        return ChartDataHolder(
                upstream.content
                        .map { (label, content) -> DataSetDataHolder(
                                label,
                                content
                                        .flatMap{ valueGroup: ValueGroup -> summedValueGroups
                                                .filter { summedGroup -> summedGroup.key == valueGroup.key }
                                                .map { summedGroup ->
                                                    ValueGroup(
                                                            valueGroup.key,
                                                            valueGroup.content
                                                                    .flatMapSingle{ value ->
                                                                        summedGroup.content
                                                                                .singleOrError()
                                                                                .map { summedVal ->

                                                                                    var newValueData = 0F
                                                                                    if(summedVal.valueData > 0){
                                                                                        newValueData = value.valueData / summedVal.valueData
                                                                                    }

                                                                                    Value(newValueData, value.amount, value.label)
                                                                                }
                                                                    }
                                                        )
                                                    }
                                        }
                        )}
        )
    }

}
