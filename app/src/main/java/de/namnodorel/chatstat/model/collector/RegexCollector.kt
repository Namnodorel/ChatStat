/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.collector

import de.namnodorel.chatstat.model.data.Message
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import de.namnodorel.ofv.annotations.GenerateObjectForm
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import java.util.regex.Pattern

@GenerateObjectForm
class RegexCollector : DataCollector() {

    var target: Set<String> = HashSet()

    var regexes: List<String> = ArrayList()

    override fun collectData(messages: Observable<Message>, key: Int?): Single<ValueGroup> {

        return Single.just(ValueGroup(key!!, Observable.fromIterable(regexes)
                .flatMapMaybe { regex ->
                    messages
                            .filter { msg -> target.isEmpty() || target.contains(msg.sender) }
                            .flatMap { message -> Observable.fromIterable(getAllMatches(regex, message.message)) }
                            .count()
                            .flatMapMaybe { value ->
                                messages.filter { msg -> target.isEmpty() || target.contains(msg.sender) }
                                        .count()
                                        .filter { amount -> amount != 0L && value != 0L }
                                        .map { amount -> Value(value.toFloat(), amount.toInt(), regex) }
                            }
                }
                //Sort by value, then by amount, then by label
                .sorted { o1, o2 ->
                    val diffData = o1.valueData - o2.valueData
                    if (diffData < 0) {
                        return@sorted 1
                    } else if (diffData > 0) {
                        return@sorted - 1
                    } else {
                        val diffAmount = o1.amount - o2.amount

                        if (diffAmount < 0) {
                            return@sorted 1
                        } else if (diffAmount > 0) {
                            return@sorted - 1
                        } else {
                            if (o1.hasLabel()) {
                                if (o2.hasLabel()) {
                                    return@sorted o1.label!!.toLowerCase().compareTo(o2.label!!.toLowerCase())
                                } else {
                                    return@sorted 0
                                }
                            } else if (o2.hasLabel()) {
                                return@sorted 1
                            } else {
                                return@sorted 0
                            }
                        }
                    }
                }
        ))

    }


    private fun getAllMatches(pattern: String, s: String): List<String> {
        val results = ArrayList<String>()

        val m = Pattern.compile(pattern).matcher(s)
        while (m.find()) {
            results.add(m.toMatchResult().group())
        }
        return results
    }

    override fun valid(): Boolean {
        return (regexes.isNotEmpty()
                && regexes[0].isNotEmpty())
    }
}
