/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.chart.pie;

import de.namnodorel.chatstat.model.chart.DisplaySettings;
import de.namnodorel.ofv.annotations.GenerateObjectForm;

@GenerateObjectForm
public class PieChartDisplaySettings extends DisplaySettings {

    private Boolean holeEnabled = true;
    private Boolean drawLabelsEnabled = false;
    private Boolean drawValuesEnabled = true;
    private Boolean halfPieEnabled = false;

    public PieChartDisplaySettings() {
    }

    public Boolean getHoleEnabled() {
        return this.holeEnabled;
    }

    public Boolean getDrawLabelsEnabled() {
        return this.drawLabelsEnabled;
    }

    public Boolean getDrawValuesEnabled() {
        return this.drawValuesEnabled;
    }

    public Boolean getHalfPieEnabled() {
        return this.halfPieEnabled;
    }

    public void setHoleEnabled(Boolean holeEnabled) {
        this.holeEnabled = holeEnabled;
    }

    public void setDrawLabelsEnabled(Boolean drawLabelsEnabled) {
        this.drawLabelsEnabled = drawLabelsEnabled;
    }

    public void setDrawValuesEnabled(Boolean drawValuesEnabled) {
        this.drawValuesEnabled = drawValuesEnabled;
    }

    public void setHalfPieEnabled(Boolean halfPieEnabled) {
        this.halfPieEnabled = halfPieEnabled;
    }

    public String toString() {
        return "PieChartDisplaySettings(holeEnabled=" + this.getHoleEnabled() + ", drawLabelsEnabled=" + this.getDrawLabelsEnabled() + ", drawValuesEnabled=" + this.getDrawValuesEnabled() + ", halfPieEnabled=" + this.getHalfPieEnabled() + ")";
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof PieChartDisplaySettings))
            return false;
        final PieChartDisplaySettings other = (PieChartDisplaySettings) o;
        if (!other.canEqual((Object) this)) return false;
        if (!super.equals(o)) return false;
        final Object this$holeEnabled = this.getHoleEnabled();
        final Object other$holeEnabled = other.getHoleEnabled();
        if (this$holeEnabled == null ? other$holeEnabled != null : !this$holeEnabled.equals(other$holeEnabled))
            return false;
        final Object this$drawLabelsEnabled = this.getDrawLabelsEnabled();
        final Object other$drawLabelsEnabled = other.getDrawLabelsEnabled();
        if (this$drawLabelsEnabled == null ? other$drawLabelsEnabled != null : !this$drawLabelsEnabled.equals(other$drawLabelsEnabled))
            return false;
        final Object this$drawValuesEnabled = this.getDrawValuesEnabled();
        final Object other$drawValuesEnabled = other.getDrawValuesEnabled();
        if (this$drawValuesEnabled == null ? other$drawValuesEnabled != null : !this$drawValuesEnabled.equals(other$drawValuesEnabled))
            return false;
        final Object this$halfPieEnabled = this.getHalfPieEnabled();
        final Object other$halfPieEnabled = other.getHalfPieEnabled();
        if (this$halfPieEnabled == null ? other$halfPieEnabled != null : !this$halfPieEnabled.equals(other$halfPieEnabled))
            return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof PieChartDisplaySettings;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = super.hashCode();
        final Object $holeEnabled = this.getHoleEnabled();
        result = result * PRIME + ($holeEnabled == null ? 43 : $holeEnabled.hashCode());
        final Object $drawLabelsEnabled = this.getDrawLabelsEnabled();
        result = result * PRIME + ($drawLabelsEnabled == null ? 43 : $drawLabelsEnabled.hashCode());
        final Object $drawValuesEnabled = this.getDrawValuesEnabled();
        result = result * PRIME + ($drawValuesEnabled == null ? 43 : $drawValuesEnabled.hashCode());
        final Object $halfPieEnabled = this.getHalfPieEnabled();
        result = result * PRIME + ($halfPieEnabled == null ? 43 : $halfPieEnabled.hashCode());
        return result;
    }
}
