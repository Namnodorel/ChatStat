/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.chart;

import java.io.Serializable;

import de.namnodorel.chatstat.model.validation.Validatable;
import de.namnodorel.chatstat.view.implementation.formelements.FormParamIntegerFormElement;

public abstract class DisplaySettings implements Serializable, Validatable {

    @FormParamIntegerFormElement(min = 50, max = 1000, allowAuto = true)
    public Integer size = 500;

    public DisplaySettings() {
    }

    public Integer getSize() {
        return this.size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof DisplaySettings)) return false;
        final DisplaySettings other = (DisplaySettings) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$size = this.getSize();
        final Object other$size = other.getSize();
        if (this$size == null ? other$size != null : !this$size.equals(other$size)) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof DisplaySettings;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $size = this.getSize();
        result = result * PRIME + ($size == null ? 43 : $size.hashCode());
        return result;
    }

    public String toString() {
        return "DisplaySettings(size=" + this.getSize() + ")";
    }
}
