/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.persist.serialization

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParseException
import com.google.gson.JsonPrimitive
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer

import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.util.ArrayList

import ru.vyarus.java.generics.resolver.context.container.ParameterizedTypeImpl

class SerList : JsonSerializer<List<*>>, JsonDeserializer<List<*>> {

    override fun serialize(src: List<*>, typeOfSrc: Type, context: JsonSerializationContext): JsonElement {

        val retValue = JsonObject()
        val className = ((typeOfSrc as ParameterizedType).actualTypeArguments[0] as Class<*>).name
        retValue.addProperty(TYPE_KEY, className)
        val el = context.serialize(src, ParameterizedTypeImpl(ArrayList::class.java, *typeOfSrc.actualTypeArguments))
        retValue.add(INSTANCE_KEY, el)

        return retValue
    }

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): List<*> {

        val jsonObject = json.asJsonObject

        val prim = jsonObject.get(TYPE_KEY) as JsonPrimitive?

        if (prim != null) {
            val className = prim.asString

            val klass: Class<*>?
            try {
                klass = Class.forName(className)
            } catch (e: ClassNotFoundException) {
                e.printStackTrace()
                throw JsonParseException(e.message)
            }

            return context.deserialize(jsonObject.get(INSTANCE_KEY), ParameterizedTypeImpl(ArrayList::class.java, klass!!))

        } else {
            throw JsonParseException("No type found for serialized list object! This should not be possible, somebody might have meddled with the saved files :@")
        }

    }

    companion object {

        private const val TYPE_KEY = "list_type"
        private const val INSTANCE_KEY = "instance"
    }

}
