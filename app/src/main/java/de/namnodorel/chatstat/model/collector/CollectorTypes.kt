/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.collector

import de.namnodorel.chatstat.model.validation.Validatable
import de.namnodorel.ofv.annotations.GenerateObjectForm
import de.namnodorel.ofv.annotations.NoFormField
import java.io.Serializable

abstract class CollectorType(@NoFormField val name: String, @NoFormField val collector: Class<out DataCollector>) : Serializable, Validatable

@GenerateObjectForm
class MessageCollectorType : CollectorType("Messages", MessageCollector::class.java)

@GenerateObjectForm
class WordCollectorType : CollectorType("Words", WordCollector::class.java)

@GenerateObjectForm
class RegexMatchCollectorType : CollectorType("Regex Matches", RegexCollector::class.java)

@GenerateObjectForm
class TimeCollectorType: CollectorType("Time between messages", TimeCollector::class.java)

@GenerateObjectForm
class WordStackCollectorType: CollectorType("Word Popularity", WordPopularityCollector::class.java)

@GenerateObjectForm
class EmojiStackCollectorType: CollectorType("Emoji Popularity", EmojiPopularityCollector::class.java)

@GenerateObjectForm
class XPerYCollectorType : CollectorType("?", DivisorCollector::class.java) {
    var xUnit: CollectorType? = null
    var yUnit: CollectorType? = null

    override fun valid(): Boolean {
        return xUnit != null && yUnit != null
    }
}
