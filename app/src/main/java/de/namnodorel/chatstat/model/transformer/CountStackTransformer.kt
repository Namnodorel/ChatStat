/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.transformer

import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import de.namnodorel.ofv.annotations.GenerateObjectForm
import de.namnodorel.ofv.annotations.NoFormField
import io.reactivex.rxjava3.functions.BiFunction

@GenerateObjectForm
class CountStackTransformer : DetailTransformer() {

    @NoFormField
    override val updatedYUnit: String = "{original}"

    override fun transformDetail(data: ValueGroup): ValueGroup {
        return ValueGroup(
                data.key,
                data.content
                        .reduce(
                            Value(0f, 0),
                            { value1, value2 -> Value(value1.valueData + 1, value1.amount + value2.amount) }
                        )
                        .toObservable()
        )
    }
}
