/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.transformer

import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import de.namnodorel.ofv.annotations.GenerateObjectForm
import de.namnodorel.ofv.annotations.NoFormField
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleSource
import io.reactivex.rxjava3.functions.BiFunction
import io.reactivex.rxjava3.functions.Function

@GenerateObjectForm
class DetailPercentageTransformer : DetailTransformer() {

    @NoFormField
    override val updatedYUnit: String = "{original} in %"

    override fun transformDetail(data: ValueGroup): ValueGroup {
        val dataObservable = data.content.cache()

        val totalCount = dataObservable
                .reduce(0f, {aFloat, floatSimpleData -> aFloat + floatSimpleData.valueData })

        val result = dataObservable
                .flatMapSingle{value ->
                    totalCount.map {total ->
                        val newValue: Value

                        var newValueData = 0F
                        if (total > 0){
                            newValueData = value.valueData / total
                        }

                        newValue = Value(newValueData, value.amount, value.label)

                        return@map newValue
                    }
                }

        return ValueGroup(data.key, result)
    }
}
