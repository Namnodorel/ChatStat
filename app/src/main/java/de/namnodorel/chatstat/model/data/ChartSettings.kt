/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.data

import de.namnodorel.chatstat.model.chart.DisplaySettings
import de.namnodorel.chatstat.model.collector.CollectorType
import de.namnodorel.chatstat.model.collector.DataCollector
import de.namnodorel.chatstat.model.collector.SimpleCollector
import de.namnodorel.chatstat.model.groupselector.DataGroupSelector
import de.namnodorel.chatstat.model.pretransformer.PreTransformer
import de.namnodorel.chatstat.model.transformer.Transformer
import de.namnodorel.chatstat.model.validation.Validatable
import io.reactivex.rxjava3.core.Observable
import java.io.Serializable

class ChartSettings<T : DisplaySettings>(
        val title: String,
        val description: String,
        val preTransformers: List<PreTransformer>,
        val dataGroupSelector: DataGroupSelector,
        val collectorType: CollectorType,
        val dataCollectors: List<DataCollector>,
        val dataTransformers: List<Transformer>,
        val displaySettings:T
    ) : Serializable, Validatable {

    val preTransformersObservable: Observable<PreTransformer>
        get() = Observable.fromIterable(preTransformers)

    val dataCollectorsObservable: Observable<DataCollector>
        get() = Observable.fromIterable(dataCollectors)

    val dataTransformersObservable: Observable<Transformer>
        get() = Observable.fromIterable(dataTransformers)

    override fun valid(): Boolean {
        return (title.isNotEmpty()
                && (dataCollectors.isNotEmpty() || SimpleCollector::class.java.isAssignableFrom(collectorType.collector))
                && Validatable.iterableValid(preTransformers)
                && dataGroupSelector.valid()
                && Validatable.iterableValid(dataCollectors)
                && Validatable.iterableValid(dataTransformers)
                && displaySettings.valid())
    }
}
