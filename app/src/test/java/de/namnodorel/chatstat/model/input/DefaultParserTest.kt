/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.input

import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime
import org.joda.time.tz.UTCProvider
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test

import java.text.SimpleDateFormat
import java.util.Locale

import de.namnodorel.chatstat.model.data.Message
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

class DefaultParserTest {

    @Test
    fun `parses info from chat string`() {

        val textChat = Single.just(
                "30.06.16, 17:09 - Some content.\n" +
                        "30.06.16, 17:08 - Arne: Hi\n" +
                        "05.08.17, 17:09 - Joe: whos there\n" +
                        "09.12.18, 17:11 - Arne: no u")

        val expectedResult = arrayOf(
                Message(LocalDateTime(2016, 6, 30, 17, 8), "Arne", "Hi"),
                Message(LocalDateTime(2017, 8, 5, 17, 9), "Joe", "whos there"),
                Message(LocalDateTime(2018, 12, 9, 17, 11), "Arne", "no u")
        )

        val parser = DefaultParser(Locale.GERMAN, SimpleDateFormat("kk:mm"))

        val result = parser.parse(textChat).messages

        result
                .test()
                .assertResult(*expectedResult)
    }

    @Test
    fun `parses info from chat string with an unusual date time separator`() {

        val textChat = Single.just(
                "9.12.2018 klo 17:11 - Arne: no u")

        val expectedResult = arrayOf(Message(LocalDateTime(2018, 12, 9, 17, 11), "Arne", "no u"))

        val parser = DefaultParser(Locale("fi"), SimpleDateFormat("kk:mm"))

        val result = parser.parse(textChat).messages

        result
                .test()
                .assertResult(*expectedResult)
    }

    companion object {

        @BeforeAll
        fun setupJodaTime() {
            DateTimeZone.setProvider(UTCProvider())
        }
    }

}
