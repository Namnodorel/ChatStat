/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.transformer

import org.junit.jupiter.api.Test

import de.namnodorel.chatstat.model.data.ChartDataHolder
import de.namnodorel.chatstat.model.data.DataSetDataHolder
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import io.mockk.Called
import io.mockk.every
import io.mockk.spyk
import io.mockk.verify
import io.reactivex.rxjava3.core.Observable

import org.junit.jupiter.api.Assertions.assertEquals


class DetailTransformerTest {

    @Test
    fun `calls transformDetail() for every valuegroup`() {

        val group1 = ValueGroup(NO_KEY, Observable.just(Value(0.1f, 1)))
        val group2 = ValueGroup(NO_KEY, Observable.just(Value(0.5f, 1)))
        val group3 = ValueGroup(NO_KEY, Observable.just(Value(0.7f, 1)))

        val group4 = ValueGroup(NO_KEY, Observable.just(Value(1.1f, 1)))
        val group5 = ValueGroup(NO_KEY, Observable.just(Value(1.5f, 1)))
        val group6 = ValueGroup(NO_KEY, Observable.just(Value(1.7f, 1)))

        val transformer = spyk<DetailTransformer>()

        every{ transformer.transformDetail(any()) } returns EMPTY_GROUP

        transformer.transform(ChartDataHolder(Observable.just(
                DataSetDataHolder("", Observable.just(
                        group1, group2, group3
                )),
                DataSetDataHolder("", Observable.just(
                        group4, group5, group6
                ))

        ))).content.blockingSubscribe { (_, content) -> content.blockingSubscribe { (_, _) -> } }
        //We have to subscribe all the way down for the method to actually execute properly, but don't do anything with the data

        verify{ transformer.transformDetail(group1) }
        verify{ transformer.transformDetail(group2) }
        verify{ transformer.transformDetail(group3) }
        verify{ transformer.transformDetail(group4) }
        verify{ transformer.transformDetail(group5) }

    }

    @Test
    fun `does not call transformDetail() when there are no values`() {

        val transformer = spyk<DetailTransformer>()

        transformer.transform(ChartDataHolder(Observable.just(
                DataSetDataHolder("", Observable.empty()),
                DataSetDataHolder("", Observable.empty())

        ))).content.blockingSubscribe { (_, content) -> content.blockingSubscribe { (_, _) -> } }
        //We have to subscribe all the way down for the method to actually execute properly, but don't do anything with the data

        verify{ transformer.transformDetail(any()) wasNot Called }
    }

    @Test
    fun `returns the transformed values when given data and a transformer`() {

        val untransformedData = ValueGroup(NO_KEY, Observable.just(Value(0.1f, 1)))
        val transformedData = ValueGroup(NO_KEY, Observable.just(Value(0.5f, 1)))

        val transformer = spyk<DetailTransformer>()

        every{ transformer.transformDetail(untransformedData) } returns transformedData

        val (content) = transformer.transform(ChartDataHolder(Observable.just(
                DataSetDataHolder("", Observable.just(untransformedData))
        )))

        assertEquals(transformedData, content.blockingFirst().content.blockingFirst())
    }

    companion object {

        private const val NO_KEY = -1
        private val EMPTY_GROUP = ValueGroup(-1, Observable.empty())
    }
}
