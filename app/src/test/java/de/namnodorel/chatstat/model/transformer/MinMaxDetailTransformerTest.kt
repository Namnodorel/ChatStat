/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.transformer

import org.junit.jupiter.api.Test

import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import io.reactivex.rxjava3.core.Observable

import org.junit.jupiter.api.Assertions.assertEquals

class MinMaxDetailTransformerTest {

    @Test
    fun `only keeps the 3 highest when required`() {

        val key = 1

        val input = ValueGroup(key, Observable.just(
                Value(-10f, 1),
                Value(10f, 1),
                Value(7.9f, 1),
                Value(8f, 1),
                Value(9f, 1)

        ))

        val expectedResult = arrayOf(
                Value(10f, 1),
                Value(9f, 1),
                Value(8f, 1)
        )

        val mmTransformer = MinMaxDetailTransformer()
        mmTransformer.takeMin = false
        mmTransformer.count = 3
        mmTransformer.keepRestSummedUp = false

        val (key1, content) = mmTransformer.transformDetail(input)

        assertEquals(key, key1)
        content
                .test()
                .assertResult(*expectedResult)
    }

    @Test
    fun `only keeps the 3 lowest when required`() {

        val key = 1

        val input = ValueGroup(key, Observable.just(
                Value(-10f, 1),
                Value(10f, 1),
                Value(7.9f, 1),
                Value(8f, 1),
                Value(9f, 1)

        ))

        val expectedResult = arrayOf(
                Value(-10f, 1),
                Value(7.9f, 1),
                Value(8f, 1)
        )

        val mmTransformer = MinMaxDetailTransformer()
        mmTransformer.takeMin = true
        mmTransformer.count = 3
        mmTransformer.keepRestSummedUp = false

        val (key1, content) = mmTransformer.transformDetail(input)

        assertEquals(key, key1)
        content
                .test()
                .assertResult(*expectedResult)
    }

    @Test
    fun `keeps the 3 highest and sums up the rest in an additional value when required`() {

        val key = 1

        val input = ValueGroup(key, Observable.just(
                Value(-10f, 1),
                Value(10f, 1),
                Value(7.9f, 1),
                Value(8f, 1),
                Value(9f, 1)

        ))

        val expectedResult = arrayOf(
                Value(10f, 1),
                Value(9f, 1),
                Value(8f, 1),
                Value(-2.1f, 2, "Rest")
        )

        val mmTransformer = MinMaxDetailTransformer()
        mmTransformer.takeMin = false
        mmTransformer.count = 3
        mmTransformer.keepRestSummedUp = true

        val (key1, content) = mmTransformer.transformDetail(input)

        assertEquals(key, key1)
        content
                .test()
                .assertResult(*expectedResult)
    }
}
