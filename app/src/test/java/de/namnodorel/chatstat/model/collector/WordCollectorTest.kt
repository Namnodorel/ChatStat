/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.collector

import org.joda.time.LocalDateTime
import org.junit.jupiter.api.Test

import java.util.HashSet

import de.namnodorel.chatstat.model.data.Message
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

class WordCollectorTest {

    @Test
    fun `counts words in messages`() {

        val messages = Observable.just(
                Message(NO_DATE, "somebody", "Bla Bla Bla"),
                Message(NO_DATE, "somebody", "This is a message with some words")
        )

        val key = 0

        val target = HashSet<String>()
        target.add("somebody")

        val expectedResult = ValueGroup(key, Observable.just(
                Value(3f, 1),
                Value(7f, 1)
        ))

        val wordCollector = WordCollector()
        wordCollector.target = target

        val result = wordCollector.collectData(messages, key)

        result
                .test()
                .assertResult(expectedResult)
    }

    companion object {

        private val NO_DATE = LocalDateTime.now()
    }

}
