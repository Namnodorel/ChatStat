/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.transformer

import org.junit.jupiter.api.Test

import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import io.reactivex.rxjava3.core.Observable

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull

class DetailPercentageTransformerTest {

    @Test
    fun `calculates percentage of a value within its valuegroup`() {

        val untransformedData = ValueGroup(NO_KEY, Observable.just(
                Value(5f, 1),
                Value(14f, 1),
                Value(3f, 1),
                Value(20f, 1)
        ))

        val expectedResult = arrayOf(
                Value(5f / 42f, 1),
                Value(14f / 42f, 1),
                Value(3f / 42f, 1),
                Value(20f / 42f, 1)
        )

        val dpt = DetailPercentageTransformer()

        val (key, content) = dpt.transformDetail(untransformedData)

        assertEquals(NO_KEY, key)
        content
                .test()
                .assertResult(*expectedResult)
    }

    companion object {

        private const val NO_KEY = -1
    }
}
