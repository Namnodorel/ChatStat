/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.transformer

import org.junit.jupiter.api.Test

import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import io.reactivex.rxjava3.core.Observable

import org.junit.jupiter.api.Assertions.assertEquals

class ValueReduceTransformerTest {

    @Test
    fun `sums up multiple values in a single one`() {

        val key = 1

        val input = ValueGroup(key, Observable.fromArray(
                Value(10f, 3),
                Value(15f, 4),
                Value(0.3f, 20)
        ))

        val expectedResult = arrayOf(Value(25.3f, 27))

        val vrt = ValueReduceTransformer()

        val (key1, content) = vrt.transformDetail(input)

        assertEquals(key, key1)
        content
                .test()
                .assertResult(*expectedResult)
    }


}
