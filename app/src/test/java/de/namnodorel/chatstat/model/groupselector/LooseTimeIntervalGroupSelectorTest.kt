/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.groupselector

import org.joda.time.LocalDateTime
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource

import java.util.stream.Stream

import de.namnodorel.chatstat.model.data.DataTimeInterval
import de.namnodorel.chatstat.model.data.Message

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS

@TestInstance(PER_CLASS)
class LooseTimeIntervalGroupSelectorTest {

    @ParameterizedTest
    @MethodSource("provideValidArguments")
    fun `identifies index from given date and loose time interval`(interval: DataTimeInterval, date: LocalDateTime, expectedResult: Int) {

        val message = Message(date, NO_SENDER, NO_MESSAGE)

        val ltigs = LooseTimeIntervalGroupSelector()
        ltigs.interval = interval

        val result = ltigs.identifyGroup(message)

        assertEquals(expectedResult, result)
    }

    private fun provideValidArguments() = Stream.of(
            Arguments.of(DataTimeInterval.MONTH, LocalDateTime(2000, 5, 1, 1, 1), 5),
            Arguments.of(DataTimeInterval.MONTH, LocalDateTime(2000, 10, 1, 1, 1), 10),
            Arguments.of(DataTimeInterval.DAY, LocalDateTime(2000, 1, 15, 1, 1), 6),
            Arguments.of(DataTimeInterval.DAY, LocalDateTime(2000, 2, 2, 1, 1), 3),
            Arguments.of(DataTimeInterval.YEAR, LocalDateTime(2020, 1, 1, 1, 1), 2020)
    )


    @Test
    fun `throws IllegalArgumentException when given an incompatible interval`() {
        val message = Message(LocalDateTime(1900, 5, 1, 1, 1), NO_SENDER, NO_MESSAGE)

        val stigs = LooseTimeIntervalGroupSelector()
        stigs.interval = DataTimeInterval.MESSAGE

        assertThrows(IllegalArgumentException::class.java) { stigs.identifyGroup(message) }
    }

    companion object {

        private const val NO_SENDER = ""
        private const val NO_MESSAGE = ""
    }

}
