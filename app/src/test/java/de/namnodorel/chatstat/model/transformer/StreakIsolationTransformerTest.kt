/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.transformer

import de.namnodorel.chatstat.model.data.ChartDataHolder
import de.namnodorel.chatstat.model.data.DataSetDataHolder
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import io.reactivex.rxjava3.core.Observable
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*

class StreakIsolationTransformerTest {

    @Test
    fun `picks and stacks streaks from multiple streaks`() {

        val input = ChartDataHolder(Observable.just(
                DataSetDataHolder(NO_LABEL, Observable.just(
                        ValueGroup(0, Observable.just(
                                Value(3f, 2)
                        )),
                        ValueGroup(1, Observable.just(
                                Value(5f, 2)
                        )),
                        ValueGroup(5, Observable.just(
                                Value(10f, 1)
                        )),
                        ValueGroup(10, Observable.just(
                                Value(10f, 1)
                        )),
                        ValueGroup(11, Observable.just(
                                Value(4f, 5)
                        )),
                        ValueGroup(12, Observable.just(
                                Value(1f, 1)
                        ))
                ))
        ))

        val expectedResult = DataSetDataHolder(NO_LABEL, Observable.just(
                ValueGroup(0, Observable.just(
                        Value(3f, 2),
                        Value(5f, 2)
                )),
                ValueGroup(5, Observable.just(
                        Value(10f, 1)
                )),
                ValueGroup(10, Observable.just(
                        Value(10f, 1),
                        Value(4f, 5),
                        Value(1f, 1)
                ))
        ))

        val sit = StreakIsolationTransformer()

        val (content) = sit.transform(input)

        content
                .test()
                .assertResult(expectedResult)

    }

    @Test
    fun `picks and stacks streaks correctly which are close by each other`() {

        val input = ChartDataHolder(Observable.just(
                DataSetDataHolder(NO_LABEL, Observable.just(
                        ValueGroup(0, Observable.just(
                                Value(3f, 2)
                        )),
                        ValueGroup(1, Observable.just(
                                Value(5f, 2)
                        )),
                        ValueGroup(3, Observable.just(
                                Value(10f, 1)
                        )),
                        ValueGroup(4, Observable.just(
                                Value(10f, 1)
                        )),
                        ValueGroup(5, Observable.just(
                                Value(4f, 5)
                        ))
                ))
        ))

        val expectedResult = DataSetDataHolder(NO_LABEL, Observable.just(
                ValueGroup(0, Observable.just(
                        Value(3f, 2),
                        Value(5f, 2)
                )),
                ValueGroup(3, Observable.just(
                        Value(10f, 1),
                        Value(10f, 1),
                        Value(4f, 5)
                ))
        ))

        val sit = StreakIsolationTransformer()

        val (content) = sit.transform(input)

        content
                .test()
                .assertResult(expectedResult)

    }

    @Test
    fun `picks and stacks streaks with labelled values`() {

        val input = ChartDataHolder(Observable.just(
                DataSetDataHolder(NO_LABEL, Observable.just(
                        ValueGroup(5, Observable.just(
                                Value(10f, 1, "label")
                        )),
                        ValueGroup(10, Observable.just(
                                Value(10f, 1)
                        )),
                        ValueGroup(11, Observable.just(
                                Value(4f, 5)
                        )),
                        ValueGroup(12, Observable.just(
                                Value(1f, 1)
                        ))
                ))
        ))

        val expectedResult = DataSetDataHolder(NO_LABEL, Observable.just(
                ValueGroup(5, Observable.just(
                        Value(10f, 1, "label")
                )),
                ValueGroup(10, Observable.just(
                        Value(10f, 1),
                        Value(4f, 5),
                        Value(1f, 1)
                ))
        ))

        val sit = StreakIsolationTransformer()

        val (content) = sit.transform(input)

        content
                .test()
                .assertResult(expectedResult)

    }

    @Test
    fun `throws exception when given stacked values`() {
        val input = ChartDataHolder(Observable.just(
                DataSetDataHolder(NO_LABEL, Observable.just(
                        ValueGroup(0, Observable.just(
                                Value(3f, 2)
                        )),
                        ValueGroup(1, Observable.just(
                                Value(5f, 2),
                                Value(10f, 1)
                        ))
                ))
        ))

        val sit = StreakIsolationTransformer()

        val (content) = sit.transform(input)

        //Can't check for the exception using .test().assertError(), because RxJava is being weird https://stackoverflow.com/questions/41239959/rxjava2-with-junit-no-exceptions-thrown-in-tests
        val list = Collections.synchronizedList(ArrayList<Throwable>())

        content.blockingSubscribe({ (_, content1) -> content1.blockingSubscribe() }, { list.add(it) })

        assertEquals("Sequence contains more than one element!", list[0].message)
    }

    companion object {

        private val NO_LABEL = ""
    }
}
