/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.collector

import org.joda.time.LocalDateTime
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource

import java.util.HashSet
import java.util.stream.Stream

import de.namnodorel.chatstat.model.data.Message
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS

@TestInstance(PER_CLASS)
class TimeCollectorTest {

    @ParameterizedTest
    @MethodSource("provideMessagesAndExpectedResults_TwoParticipantsAndUniqueTargetSets")
    fun `extracts response times between two unique participants`(messages: Array<Message>, expectedResult: ValueGroup) {

        val key = 1

        val callers = HashSet<String>()
        callers.add("Dave")

        val responders = HashSet<String>()
        responders.add("Alice")

        val messageObservable = Observable.fromArray(*messages)


        val tc = TimeCollector()
        tc.callers = callers
        tc.responders = responders
        val resultSingle = tc.collectData(messageObservable, key)
        resultSingle
                .test()
                .assertResult(expectedResult)
    }

    private fun provideMessagesAndExpectedResults_TwoParticipantsAndUniqueTargetSets() = Stream.of(
            Arguments.of(
                    arrayOf<Message>(),
                    ValueGroup(1, Observable.empty())
            ),
            Arguments.of(
                    arrayOf(
                            Message(LocalDateTime(2000, 1, 1, 1, 1), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 10), "Alice", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 30), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 50), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 55), "Alice", "")
                    ),
                    ValueGroup(1, Observable.just(
                        Value(9f, 2),
                        Value(5f, 2)
                    ))
            ),
            Arguments.of(
                    arrayOf(
                            Message(LocalDateTime(2000, 1, 1, 1, 1), "Alice", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 30), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 50), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 55), "Alice", "")
                    ),
                    ValueGroup(1, Observable.just(
                            Value(5f, 1)
                    ))
            ),
            Arguments.of(
                    arrayOf(
                            Message(LocalDateTime(2000, 1, 1, 1, 1), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 10), "Alice", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 30), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 50), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 55), "Dave", "")
                    ),
                    ValueGroup(1, Observable.just(
                            Value(9f, 1)
                    ))
            )
    )


    @ParameterizedTest
    @MethodSource("provideMessagesAndExpectedResults_TwoParticipantsAndSameSingleTarget")
    fun `extract response times of the same person`(messages: Array<Message>, expectedResult: ValueGroup) {

        val key = 1

        val callers = HashSet<String>()
        callers.add("Dave")

        val responders = HashSet<String>()
        responders.add("Dave")


        val messageObservable = Observable.fromArray(*messages)


        val tc = TimeCollector()
        tc.callers = callers
        tc.responders = responders
        val resultSingle = tc.collectData(messageObservable, key)
        resultSingle
                .test()
                .assertResult(expectedResult)
    }

    private fun provideMessagesAndExpectedResults_TwoParticipantsAndSameSingleTarget() = Stream.of(
            Arguments.of(
                    arrayOf<Message>(),
                    ValueGroup(1, Observable.empty())
            ),
            Arguments.of(
                    arrayOf(
                            Message(LocalDateTime(2000, 1, 1, 1, 1), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 10), "Alice", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 30), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 50), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 55), "Alice", "")
                    ),
                    ValueGroup(1, Observable.just(
                        Value(29f, 2),
                        Value(20f, 2))
                    )
            ),
            Arguments.of(
                    arrayOf(
                            Message(LocalDateTime(2000, 1, 1, 1, 1), "Alice", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 30), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 50), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 55), "Alice", "")
                    ),
                    ValueGroup(1, Observable.just(
                            Value(20f, 1))
                    )
            ),
            Arguments.of(
                    arrayOf(
                            Message(LocalDateTime(2000, 1, 1, 1, 1), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 10), "Alice", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 30), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 50), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 55), "Dave", "")
                    ),
                    ValueGroup(1, Observable.just(
                        Value(29f, 3),
                        Value(20f, 3),
                        Value(5f, 3))
                    )
            )
    )


    @ParameterizedTest
    @MethodSource("provideMessagesAndExpectedResults_MultipleParticipantsAndMixedTargets")
    fun `extracts response times of multiple callers and responders`(messages: Array<Message>, expectedResult: ValueGroup) {

        val key = 1

        val callers = HashSet<String>()
        callers.add("Dave")
        callers.add("Alice")


        val responders = HashSet<String>()
        responders.add("Dave")
        responders.add("Steve")
        responders.add("Lea")


        val messageFlowable = Observable.fromArray(*messages)


        val tc = TimeCollector()
        tc.callers = callers
        tc.responders = responders
        val resultSingle = tc.collectData(messageFlowable, key)
        resultSingle
                .test()
                .assertResult(expectedResult)
    }

    private fun provideMessagesAndExpectedResults_MultipleParticipantsAndMixedTargets() = Stream.of(
            Arguments.of(
                    arrayOf<Message>(),
                    ValueGroup(1, Observable.empty())
            ),
            Arguments.of(
                    arrayOf(
                            Message(LocalDateTime(2000, 1, 1, 1, 1), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 10), "Alice", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 30), "Christine", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 50), "Lea", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 55), "Alice", "")
                    ),
                    ValueGroup(1, Observable.just(
                            Value(40f, 1))
                    )
            ),
            Arguments.of(
                    arrayOf(
                            Message(LocalDateTime(2000, 1, 1, 1, 1), "Alice", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 30), "Lea", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 50), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 55), "Bob", "")
                    ),
                    ValueGroup(1, Observable.just(
                            Value(29f, 1))
                    )
            ),
            Arguments.of(
                    arrayOf(
                            Message(LocalDateTime(2000, 1, 1, 1, 1), "Bob", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 10), "Alice", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 30), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 50), "Dave", ""),
                            Message(LocalDateTime(2000, 1, 1, 1, 55), "Lea", "")
                    ),
                    ValueGroup(1, Observable.just(
                        Value(20f, 3),
                        Value(20f, 3),
                        Value(5f, 3))
                    )
            )
    )

}
