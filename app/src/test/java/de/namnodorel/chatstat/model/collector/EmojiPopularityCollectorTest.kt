/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.collector

import org.joda.time.LocalDateTime
import org.junit.jupiter.api.Test

import java.util.HashSet

import de.namnodorel.chatstat.model.data.Message
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

class EmojiPopularityCollectorTest {

    @Test
    fun `extracts emoji table with counts from messages`() {

        val key = 1

        val expectedValueGroup = ValueGroup(key, Observable.fromArray(
                Value(3f, 4, "\uD83D\uDE0C"),
                Value(1f, 4, "\uD83C\uDFE0"),
                Value(1f, 4, "\uD83D\uDE28")

        ))

        val participants = HashSet<String>()
        participants.add("Alice")
        participants.add("Bob")

        val messages = Observable.fromArray(
                Message(NO_DATE, "Alice", "Are you there already? \uD83D\uDE28"),
                Message(NO_DATE, "Bob", "No, am just leaving home \uD83C\uDFE0 Why?"),
                Message(NO_DATE, "Alice", "Me too! Was worried I'd keep you waiting since I'm late."),
                Message(NO_DATE, "Alice", "\uD83D\uDE0C\uD83D\uDE0C\uD83D\uDE0C")
        )

        val collector = EmojiPopularityCollector()
        collector.target = participants

        val result = collector.collectData(messages, key)

        result
                .test()
                .assertResult(expectedValueGroup)
    }

    companion object {

        private val NO_DATE = LocalDateTime.now()
    }

}
