/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.transformer

import org.junit.jupiter.api.Test

import de.namnodorel.chatstat.model.data.ChartDataHolder
import de.namnodorel.chatstat.model.data.DataSetDataHolder
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import io.reactivex.rxjava3.core.Observable

class PercentageTransformerTest {

    @Test
    fun `calculates percentage of a value across valuegroups and datasets`() {

        val input = ChartDataHolder(Observable.fromArray(
                DataSetDataHolder("Alice", Observable.fromArray(
                        ValueGroup(1, Observable.fromArray(
                                Value(10f, 1)
                        )),
                        ValueGroup(2, Observable.fromArray(
                                Value(5f, 1),
                                Value(3f, 1)
                        )),
                        ValueGroup(4, Observable.fromArray(
                                Value(7f, 1),
                                Value(2f, 1)
                        ))
                )),
                DataSetDataHolder("Bob", Observable.fromArray(
                        ValueGroup(1, Observable.fromArray(
                                Value(5f, 1)
                        )),
                        ValueGroup(2, Observable.fromArray(
                                Value(12f, 1)
                        ))
                ))
        ))

        val expectedResult = arrayOf(DataSetDataHolder("Alice", Observable.fromArray(
                ValueGroup(1, Observable.fromArray(
                        Value(10f / 15f, 1)
                )),
                ValueGroup(2, Observable.fromArray(
                        Value(5f / 20f, 1),
                        Value(3f / 20f, 1)
                )),
                ValueGroup(4, Observable.fromArray(
                        Value(7f / 9f, 1),
                        Value(2f / 9f, 1)
                ))
        )), DataSetDataHolder("Bob", Observable.fromArray(
                ValueGroup(1, Observable.fromArray(
                        Value(5f / 15f, 1)
                )),
                ValueGroup(2, Observable.fromArray(
                        Value(12f / 20f, 1)
                ))
        )))


        val pt = PercentageTransformer()

        val (content) = pt.transform(input)

        content
                .test()
                .assertResult(*expectedResult)
    }


}
