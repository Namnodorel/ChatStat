/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.transformer

import de.namnodorel.chatstat.model.data.DataTimeInterval
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import de.namnodorel.chatstat.model.groupselector.StandardTimeIntervalGroupSelector
import io.mockk.every
import io.mockk.mockk
import io.reactivex.rxjava3.core.Observable
import org.joda.time.LocalDateTime
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*

class TimeIntervalAverageTransformerTest {

    @Test
    fun `returns average value within a time interval`() {

        val key = 1

        val untransformedData = ValueGroup(key, Observable.just(
                Value(5f, 1),
                Value(14f, 1),
                Value(3f, 1),
                Value(20f, 1)
        ))

        val expectedResult = arrayOf(
                Value(5f / 7, 1),
                Value(14f / 7, 1),
                Value(3f / 7, 1),
                Value(20f / 7, 1)
        )

        val tiat = TimeIntervalAverageTransformer()

        val selector = mockk<StandardTimeIntervalGroupSelector>()
        every{ selector.interval } returns DataTimeInterval.WEEK
        every{ selector.countFrom } returns LocalDateTime(2000, 1, 1, 1, 1)

        tiat.init(selector)
        tiat.newInterval = DataTimeInterval.DAY

        val (key1, content) = tiat.transformDetail(untransformedData)

        assertEquals(key, key1)
        content
                .test()
                .assertResult(*expectedResult)
    }

    @Test
    fun `returns average value per message`() {

        val key = 1

        val untransformedData = ValueGroup(key, Observable.just(
                Value(5f, 2),
                Value(14f, 4),
                Value(3f, 3),
                Value(20f, 5)
        ))

        val expectedResult = arrayOf(
                Value(5f / 2, 2),
                Value(14f / 4, 4),
                Value(3f / 3, 3),
                Value(20f / 5, 5)
        )

        val tiat = TimeIntervalAverageTransformer()

        val selector = mockk<StandardTimeIntervalGroupSelector>()
        every{ selector.interval } returns DataTimeInterval.DAY
        every{ selector.countFrom } returns LocalDateTime(2000, 1, 1, 1, 1)

        tiat.init(selector)
        tiat.newInterval = DataTimeInterval.MESSAGE

        val (key1, content) = tiat.transformDetail(untransformedData)

        assertEquals(key, key1)
        content
                .test()
                .assertResult(*expectedResult)
    }

    @Test
    fun `throws exception when given an invalid new interval`() {
        val untransformedData = ValueGroup(1, Observable.just(
                Value(5f, 2)
        ))

        val tiat = TimeIntervalAverageTransformer()

        val selector = mockk<StandardTimeIntervalGroupSelector>()
        every{ selector.countFrom } returns null

        tiat.init(selector)
        tiat.newInterval = DataTimeInterval.MESSAGE

        val (_, content) = tiat.transformDetail(untransformedData)

        //Can't check for the exception using .test().assertError(), because RxJava is being weird https://stackoverflow.com/questions/41239959/rxjava2-with-junit-no-exceptions-thrown-in-tests
        val list = Collections.synchronizedList(ArrayList<Throwable>())

        content.blockingSubscribe({ (_, _, _) -> }, { list.add(it) })

        assertEquals(1, list.size)
    }
}
