/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.pretransformer

import org.joda.time.LocalDateTime
import org.junit.jupiter.api.Test

import de.namnodorel.chatstat.model.data.Message
import io.reactivex.rxjava3.core.Observable

class TimeShiftingPreTransformerTest {

    @Test
    fun `shifts message dates by given amount of hours`() {

        val messages = Observable.just(
                Message(LocalDateTime(2000, 1, 1, 10, 1), NO_SENDER, NO_MESSAGE),
                Message(LocalDateTime(2000, 4, 2, 1, 1), NO_SENDER, NO_MESSAGE),
                Message(LocalDateTime(1990, 5, 10, 10, 10), NO_SENDER, NO_MESSAGE)
        )

        val hoursToShift = -3

        val expectedResult = arrayOf(
                Message(LocalDateTime(2000, 1, 1, 7, 1), NO_SENDER, NO_MESSAGE),
                Message(LocalDateTime(2000, 4, 1, 22, 1), NO_SENDER, NO_MESSAGE),
                Message(LocalDateTime(1990, 5, 10, 7, 10), NO_SENDER, NO_MESSAGE)
        )

        val tspt = TimeShiftingPreTransformer()
        tspt.hourShift = hoursToShift

        val result = tspt.apply(messages)

        result
                .test()
                .assertResult(*expectedResult)
    }

    companion object {

        private const val NO_SENDER = ""
        private const val NO_MESSAGE = ""
    }

}
