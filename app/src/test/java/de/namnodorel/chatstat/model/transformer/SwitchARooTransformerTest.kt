/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.transformer

import org.junit.jupiter.api.Test

import de.namnodorel.chatstat.model.data.ChartDataHolder
import de.namnodorel.chatstat.model.data.DataSetDataHolder
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import io.reactivex.rxjava3.core.Observable

class SwitchARooTransformerTest {

    @Test
    fun `switches data of multiple datasets and stacked values`() {

        val data = ChartDataHolder(Observable.just(
                DataSetDataHolder("set1", Observable.just(
                        ValueGroup(0, Observable.just(
                                Value(1f, 0, "something"),
                                Value(5f, 0, "someotherthing")
                        ))
                )),
                DataSetDataHolder("set2", Observable.just(
                        ValueGroup(0, Observable.just(
                                Value(10f, 0, "something"),
                                Value(3f, 0, "someotherthing")
                        ))
                ))
        ))

        val expectedResult = arrayOf(DataSetDataHolder("something", Observable.just(
                ValueGroup(0, Observable.just(
                        Value(1f, 0, "set1"),
                        Value(10f, 0, "set2")
                )
                )
        )), DataSetDataHolder("someotherthing", Observable.just(
                ValueGroup(0, Observable.just(
                        Value(5f, 0, "set1"),
                        Value(3f, 0, "set2")
                ))
        )))

        val sart = SwitchARooTransformer()

        val (content) = sart.transform(data)

        content
                .test()
                .assertResult(*expectedResult)
    }


    @Test
    fun `switches data of multiple datasets and partially unlabelled values`() {

        val data = ChartDataHolder(Observable.just(
                DataSetDataHolder("set1", Observable.just(
                        ValueGroup(0, Observable.just(
                                Value(5f, 0)
                        ))
                )),
                DataSetDataHolder("set2", Observable.just(
                        ValueGroup(0, Observable.just(
                                Value(3f, 0, "someotherthing"),
                                Value(4f, 10)
                        ))
                ))
        ))

        val expectedResult = arrayOf(
                DataSetDataHolder("", Observable.just(
                        ValueGroup(0, Observable.just(
                                Value(5f, 0, "set1"),
                                Value(4f, 10, "set2")
                        ))
                )),
                DataSetDataHolder("someotherthing", Observable.just(
                        ValueGroup(0, Observable.just(
                                Value(3f, 0, "set2")
                        ))
                ))
        )

        val sart = SwitchARooTransformer()

        val (content) = sart.transform(data)

        content
                .test()
                .assertResult(*expectedResult)

    }
}
