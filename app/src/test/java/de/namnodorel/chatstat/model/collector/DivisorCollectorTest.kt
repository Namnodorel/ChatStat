/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.collector

import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import io.mockk.every
import io.mockk.mockk
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(PER_CLASS)
@Disabled
class DivisorCollectorTest {

    @ParameterizedTest
    @MethodSource("provideValuesAndExpectedResults")
    fun `divides values from one collector by the values from another`(firstReturn: Float, secondReturn: Float, expectedValue: Float) {

        val firstValueGroup = ValueGroup(NO_KEY, Observable.just(Value(firstReturn, 0)))
        val secondValueGroup = ValueGroup(NO_KEY, Observable.just(Value(secondReturn, 0)))

        val expectedValueGroup = ValueGroup(NO_KEY, Observable.just(Value(expectedValue, 0)))

        val collector1Mock = mockk<DataCollector>()
        val collector2Mock = mockk<DataCollector>()


        every { collector1Mock.collectData(any(), any()) } returns Single.just(firstValueGroup)
        every { collector2Mock.collectData(any(), any()) } returns Single.just(secondValueGroup)

        val collector = DivisorCollector()
        //collector.collector1 = collector1Mock
        //collector.collector2 = collector2Mock

        collector.collectData(mockk(), NO_KEY)
                .test()
                .assertResult(expectedValueGroup)
    }

    private fun provideValuesAndExpectedResults() =  Stream.of(
            Arguments.of(4f, 2f, 2f),
            Arguments.of(0.1f, 2f, 0.05f),
            Arguments.of(10f, 0f, 0f)
    )

    @Test
    fun `divides values from one collector by the values from another (sorted by their labels)`() {

        val firstValueGroup = ValueGroup(NO_KEY, Observable.just(
                Value(10F, 0, "e"),
                Value(5F, 0, "c"),
                Value(1F, 0, "d"),
                Value(3F, 0, "a")
        ))
        val secondValueGroup = ValueGroup(NO_KEY, Observable.just(
                Value(10F, 0, "a"),
                Value(5F, 0, "d"),
                Value(1F, 0, "c"),
                Value(3F, 0, "e")
        ))

        val expectedValueGroup = ValueGroup(NO_KEY, Observable.just(
                Value(3F/10F, 0, "a"),
                Value(5F/1F, 0, "c"),
                Value(1F/5F, 0, "d"),
                Value(10F/3F, 0, "e")
        ))

        val collector1Mock = mockk<DataCollector>()
        val collector2Mock = mockk<DataCollector>()


        every { collector1Mock.collectData(any(), any()) } returns Single.just(firstValueGroup)
        every { collector2Mock.collectData(any(), any()) } returns Single.just(secondValueGroup)

        val collector = DivisorCollector()
        //collector.collector1 = collector1Mock
        //collector.collector2 = collector2Mock

        collector.collectData(mockk(), NO_KEY)
                .test()
                .assertResult(expectedValueGroup)
    }

    @Test
    fun `throws error when the labels are not consistent`() {

        val firstValueGroup = ValueGroup(NO_KEY, Observable.just(
                Value(10F, 0, "e"),
                Value(1F, 0, "d"),
                Value(3F, 0, "a")
        ))
        val secondValueGroup = ValueGroup(NO_KEY, Observable.just(
                Value(10F, 0, "a"),
                Value(5F, 0, "d"),
                Value(1F, 0, "c"),
                Value(3F, 0, "e")
        ))

        val collector1Mock = mockk<DataCollector>()
        val collector2Mock = mockk<DataCollector>()


        every { collector1Mock.collectData(any(), any()) } returns Single.just(firstValueGroup)
        every { collector2Mock.collectData(any(), any()) } returns Single.just(secondValueGroup)

        val collector = DivisorCollector()
        //collector.collector1 = collector1Mock
        //collector.collector2 = collector2Mock

        collector.collectData(mockk(), NO_KEY)
                .test()
                .assertError(IllegalArgumentException::class.java)
    }

    companion object {

        private const val NO_KEY = -1
    }


}
