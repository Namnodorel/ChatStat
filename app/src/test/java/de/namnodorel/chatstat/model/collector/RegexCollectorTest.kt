/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.collector

import org.joda.time.LocalDateTime
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource

import java.util.Arrays
import java.util.Collections
import java.util.HashSet
import java.util.stream.Stream

import de.namnodorel.chatstat.model.data.Message
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import io.reactivex.rxjava3.core.Observable
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS

@TestInstance(PER_CLASS)
class RegexCollectorTest {

    @ParameterizedTest
    @MethodSource("provideMessagesRegexAndExpectedResult")
    fun `counts occurrences of a regex within messages`(messages: Array<Message>, regex: String, expectedResult: ValueGroup) {

        val key = 1

        val participants = HashSet<String>()
        participants.add("Bob")
        participants.add("Alice")

        val messageObservable = Observable.fromArray(*messages)

        val collector = RegexCollector()
        collector.regexes = listOf(regex)
        collector.target = participants

        collector.collectData(messageObservable, key)
                .test()
                .assertResult(expectedResult)
    }

    private fun provideMessagesRegexAndExpectedResult() = Stream.of(
            Arguments.of(
                    arrayOf(
                            Message(NO_DATE, "Bob", "Hi Alice!"),
                            Message(NO_DATE, "Alice", "Hello Bob! How are you doing?"),
                            Message(NO_DATE, "Bob", "I'm doing great. And you?"),
                            Message(NO_DATE, "Alice", "Fine, thank you.")
                    ),
                    "(?i)(hello|hi|hey)",
                    ValueGroup(1, Observable.just(
                            Value(2f, 4, "(?i)(hello|hi|hey)"))
                    )
            ),
            Arguments.of(
                    arrayOf(
                            Message(NO_DATE, "Bob", "Hi Alice! Hi!"),
                            Message(NO_DATE, "Alice", "Hello Bob! How are you doing?"),
                            Message(NO_DATE, "Bob", "Hey Alice!"),
                            Message(NO_DATE, "Alice", "Why are you repeating yourself?")
                    ),
                    "(?i)(hello|hi|hey)",
                    ValueGroup(1, Observable.just(
                            Value(4f, 4, "(?i)(hello|hi|hey)")
                    ))
            ),
            Arguments.of(
                    arrayOf(
                            Message(NO_DATE, "Bob", "Hi Alice! Hi!"),
                            Message(NO_DATE, "Alice", "Hello Bob! How are you doing?"),
                            Message(NO_DATE, "Bob", "Hi Alice!"),
                            Message(NO_DATE, "Alice", "Why are you repeating yourself?")
                    ),
                    "(?i)(hello|hi|hey)",
                    ValueGroup(1, Observable.just(
                            Value(4f, 4, "(?i)(hello|hi|hey)")
                    ))
            ),
            Arguments.of(
                    arrayOf(Message(
                            NO_DATE, "Bob", "")
                    ),
                    "doesntmatter",
                    ValueGroup(1, Observable.empty())
            ),
            Arguments.of(
                    arrayOf<Message>(),
                    "doesntmatter",
                    ValueGroup(1, Observable.empty())
            )
    )


    @Test
    fun `counts occurrences of multiple regexes individually within messages`() {

        val expectedResult = ValueGroup(1, Observable.just(
                Value(4f, 4, "(?i)(hello|hi|hey)"),
                Value(3f, 4, "(?i)(bob|alice)")
        ))

        val key = 1

        val participants = HashSet<String>()
        participants.add("Bob")
        participants.add("Alice")

        val messageObservable = Observable.fromArray(
                Message(NO_DATE, "Bob", "Hi Alice! Hi!"),
                Message(NO_DATE, "Alice", "Hi Bob! How are you doing?"),
                Message(NO_DATE, "Bob", "Hi Alice!"),
                Message(NO_DATE, "Alice", "Why are you repeating yourself?")
        )

        val collector = RegexCollector()
        collector.regexes = listOf("(?i)(hello|hi|hey)", "(?i)(bob|alice)")
        collector.target = participants

        collector.collectData(messageObservable, key)
                .test()
                .assertResult(expectedResult)
    }

    companion object {

        private val NO_DATE = LocalDateTime.now()
    }

}
