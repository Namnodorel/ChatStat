/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.pretransformer

import org.joda.time.LocalDateTime
import org.junit.jupiter.api.Test

import de.namnodorel.chatstat.model.data.Message
import io.reactivex.rxjava3.core.Observable

class TextCleanupPreTransformerTest {

    @Test
    fun `removes non-alphanumeric characters from message`() {

        val input = Observable.just(Message(NO_DATE, NO_SENDER, "This text contains! Non! Numeric... Symbols!¯\\_(ツ)_/¯"))

        val expectedResult = Message(NO_DATE, NO_SENDER, "This text contains  Non  Numeric    Symbols          ")

        val tcpt = TextCleanupPreTransformer()
        tcpt.removeNonLettersEnabled = true
        tcpt.makeLowerCaseEnabled = false

        val result = tcpt.apply(input)

        result
                .test()
                .assertResult(expectedResult)
    }

    @Test
    fun `makes mixed case message all lowercase`() {
        val input = Observable.just(Message(NO_DATE, NO_SENDER, "mIxEd CaSe MESSAGE"))

        val expectedResult = Message(NO_DATE, NO_SENDER, "mixed case message")

        val tcpt = TextCleanupPreTransformer()
        tcpt.makeLowerCaseEnabled = true
        tcpt.removeNonLettersEnabled = false

        val result = tcpt.apply(input)

        result
                .test()
                .assertResult(expectedResult)
    }

    companion object {

        private val NO_DATE = LocalDateTime.now()
        private const val NO_SENDER = ""
    }
}
