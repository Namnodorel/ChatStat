/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.transformer

import org.junit.jupiter.api.Test

import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import io.reactivex.rxjava3.core.Observable

class CountStackTransformerTest {

    @Test
    fun `counts amount of values`() {

        val key = 0

        val input = ValueGroup(key, Observable.just(
                Value(5f, 3),
                Value(10f, 2),
                Value(50f, 25)
        ))

        val expectedResult = Value(3f, 30)

        val cst = CountStackTransformer()

        val (_, content) = cst.transformDetail(input)

        content
                .test()
                .assertResult(expectedResult)
    }

}
