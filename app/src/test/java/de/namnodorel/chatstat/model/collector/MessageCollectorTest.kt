/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.collector

import org.joda.time.LocalDateTime
import org.junit.jupiter.api.Test

import java.util.HashSet

import de.namnodorel.chatstat.model.data.Message
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import io.reactivex.rxjava3.core.Observable


class MessageCollectorTest {

    @Test
    fun `can count messages by one participant`() {

        val participants = HashSet<String>()
        participants.add("Joe")

        val key = 0

        val messages = Observable.fromArray(
                Message(NO_DATE, "Joe", NO_MESSAGE),
                Message(NO_DATE, "Joe", NO_MESSAGE),
                Message(NO_DATE, "Joe", NO_MESSAGE),
                Message(NO_DATE, "Joe", NO_MESSAGE),
                Message(NO_DATE, "Joe", NO_MESSAGE),
                Message(NO_DATE, "Joe", NO_MESSAGE)
        )

        val expectedResult = ValueGroup(key, Observable.just(Value(6f, 6)))

        val messageCollector = MessageCollector()
        messageCollector.target = participants

        messageCollector.collectData(messages, key)
                .test()
                .assertResult(expectedResult)
    }

    @Test
    fun `can count messages by multiple participants individually`() {
        val participants = HashSet<String>()
        participants.add("Joe")
        participants.add("Monika")

        val key = 0

        val messages = Observable.fromArray(
                Message(NO_DATE, "Joe", NO_MESSAGE),
                Message(NO_DATE, "Monika", NO_MESSAGE),
                Message(NO_DATE, "Monika", NO_MESSAGE),
                Message(NO_DATE, "Joe", NO_MESSAGE),
                Message(NO_DATE, "Sam", NO_MESSAGE),
                Message(NO_DATE, "Joe", NO_MESSAGE)
        )

        val expectedResult = ValueGroup(key, Observable.just(Value(5f, 5)))

        val messageCollector = MessageCollector()
        messageCollector.target = participants

        messageCollector.collectData(messages, key)
                .test()
                .assertResult(expectedResult)
    }

    companion object {

        private val NO_DATE = LocalDateTime.now()
        private const val NO_MESSAGE = ""
    }
}
