/*
 *     ChatStat - An Android application to generate charts for chats
 *     Copyright (C) 2017-2019  Felix Günther (Namnodorel)
 *
 *     ChatStat is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     ChatStat is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.namnodorel.chatstat.model.collector

import org.joda.time.LocalDateTime
import org.junit.jupiter.api.Test

import java.util.HashSet

import de.namnodorel.chatstat.model.data.Message
import de.namnodorel.chatstat.model.data.Value
import de.namnodorel.chatstat.model.data.ValueGroup
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single

class WordPopularityCollectorTest {

    @Test
    fun `counts amounts of word usages`() {

        val key = 1

        val expectedResult = ValueGroup(key, Observable.fromArray(
                Value(3f, 4, "you"),
                Value(2f, 4, "doing"),
                Value(2f, 4, "Hello"),
                Value(1f, 4, "Alice"),
                Value(1f, 4, "And"),
                Value(1f, 4, "are"),
                Value(1f, 4, "Bob"),
                Value(1f, 4, "Fine"),
                Value(1f, 4, "great"),
                Value(1f, 4, "How"),
                Value(1f, 4, "I"),
                Value(1f, 4, "m"),
                Value(1f, 4, "thank")
        ))

        val participants = HashSet<String>()
        participants.add("Alice")
        participants.add("Bob")

        val messageObservable = Observable.fromArray(
                Message(NO_DATE, "Bob", "Hello Alice!"),
                Message(NO_DATE, "Alice", "Hello Bob! How are you doing?"),
                Message(NO_DATE, "Bob", "I'm doing great. And you?"),
                Message(NO_DATE, "Alice", "Fine, thank you.")
        )

        val collector = WordPopularityCollector()
        collector.target = participants

        val result = collector.collectData(messageObservable, key)

        result
                .test()
                .assertResult(expectedResult)
    }

    companion object {

        private val NO_DATE = LocalDateTime.now()
    }

}
