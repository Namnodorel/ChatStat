Release notes and latest release download
[here](https://gitlab.com/Namnodorel/ChatStat/releases)

# ChatStat
Welcome to ChatStat! This is an Android app that can analyze your
WhatsApp chats and visualize them however you want. Features include:

- Support for both private and group chats
- ChatStat can measure:
  - Message count
  - Word count
  - Time to respond (in minutes)
  - Word popularity
  - Emoji popularity
  - Popularity of a custom RegEx
- Options to process the data:
  - Calculate averages
  - Calculate percentages
  - Only show highest/lowest values
  - Find streaks in a row
  - Limit the processed timespan
  - Make messages in the middle of the night belong to the day you want
  - More variations of the above and utility options
  - Combine these options however you like!
- Available charts:
  - Bar Chart
  - Line Chart
  - Pie Chart
  - Scatter Chart
  - Radar/Spider Chart
  - Single statistic with a short text
- Customize how your charts look:
  - Data colors
  - Background grids
  - Axis lines
  - Data labels
  - More, often depending on the chart
- Dark/Light themes available
- Works completely offline
- Works on Android 5.0 (Lollipop) and higher
- No advertising
- No tracking
- 100% open source

_Note: I am not affiliated with WhatsApp in any way._

# Bugs/Feature Requests

- first, search if your bug/feature **already has an issue**
- **if not**, create a new one
  [here](https://gitlab.com/Namnodorel/ChatStat/issues) and give a
  description that is as detailed as possible.

# Planned Features
_These are mostly ideas for features to come. There is no way to tell
when (or if) they will be implemented._ 

This list is not exhaustive. For everything see the
[project issues](https://gitlab.com/Namnodorel/ChatStat/issues).

- Support for other messengers _(Most unfortunately do not provide an
  easy way to export a chat)_
- Charts:
  - Word Clouds _(See #1)_
  - Heat Map/Mosaic Chart _(See #6)_
  - Candlestick Chart _(See #19)_
- Ability to display messages a data point is made from _(See #7)_
- Filter trivial words from messages _(See #2)_
- Documentation in-app _(See #85)_
- Data controls _(See #96)_

# License
    ChatStat - An Android application to generate charts for chats
    Copyright (C) 2017-2019  Felix Günther (Namnodorel)

    ChatStat is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ChatStat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ChatStat.  If not, see <https://www.gnu.org/licenses/>.

If you would like to use ChatStats code under another license, please
contact me.